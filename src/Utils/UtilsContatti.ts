import { UtilsSetting } from './UtilsSetting';
import { Contatto } from '../class/contatto'
import { Observable } from 'rxjs/Observable';
import { delay } from 'rxjs/operators';

export class UtilsContatti {
    static listaContatti:Contatto[];

    static setListaContatti(_listaContatti:Contatto[]) {
        this.listaContatti = _listaContatti;
    }

    static addListaContatti(contatto:Contatto) {
        this.listaContatti.push(contatto);
    }

    static removeListaContatti(index:number) {
        this.listaContatti.splice(index, 1);
    }

    static checkDati(dati:any):any[] {
        let result = [true, ""];

        if (dati.nomeCognome.trim() == "") {
            result = [false, "Inserire nome e Cognome"];
            return result;
        }
        if (dati.telefono.trim() == "") {
            result = [false, "Inserire Numero Telefono"];
            return result;
        }
        if (dati.email.trim() == "") {
            result = [false, "Inserire E-mail"];
            return result;
        }
        if (parseInt(dati.maxore) <= 0 && dati.idcompany == 0) {
            result = [false, "Inserire Ore maggiori di 0"];
            return result;
        }
        if (parseInt(dati.tariffaora) < 0 && dati.idcompany == 0) {
            result = [false, "Inserire Tariffa oraria maggiore o uguale di 0"];
            return result;
        }
        if (parseInt(dati.tariffagiornata) < 0 && dati.idcompany == 0) {
            result = [false, "Inserire Tariffa Giornata maggiore o uguale di 0"];
            return result;
        }
        if (parseInt(dati.tariffakm) < 0 && dati.idcompany == 0) {
            result = [false, "Inserire Tariffa KM maggiore o uguale di 0"];
            return result;
        }
        
        return result;
    }

    static checkChanges(dati:any, contatto:Contatto):any[] {
        let changes = false;
        let campo:string[] = [];
        if (contatto.getNomeCognome() != dati.nomeCognome) {
            changes = true;
            campo.push("Nome Cognome");
        }
        if (contatto.getTelefono() != dati.telefono) {
            changes = true;
            campo.push("Telefono");
        }
        if (contatto.getEmail() != dati.email) {
            changes = true;
            campo.push("Email");
        }
        if (contatto.getPIva() != dati.piva) {
            changes = true;
            campo.push("Piva");
        }
        if (contatto.getCF() != dati.cf) {
            changes = true;
            campo.push("CF");
        }
        if (contatto.getSedeLegale() != dati.sedelegale) {
            changes = true;
            campo.push("Sede legale");
        }
        if (contatto.getSedeOperativa() != dati.sedeoperativa) {
            changes = true;
            campo.push("Sede operativa");
        }
        if (contatto.getMaxOre() != dati.maxore && dati.idcompany == 0) {
            changes = true;
            campo.push("Ore Max");
        }
        if (contatto.getTariffaOre() != dati.tariffaora && dati.idcompany == 0) {
            changes = true;
            campo.push("Tariffa Ore");
        }
        if (contatto.getTariffaGiornata() != dati.tariffagiornata && dati.idcompany == 0) {
            changes = true;
            campo.push("Tariffa Giornata");
        }
        if (contatto.getTariffaKm() != dati.tariffakm && dati.idcompany == 0) {
            changes = true;
            campo.push("Tariffa KM");
        }

        return [changes, campo];
    }

    static getContattoPrincipale(id:number):Contatto[] {
        return this.listaContatti.filter((contatto) => {
                return contatto.getID() == id;
        });
    }

    static getListaAssociati(idCompany:number, page?: number, size?: number, includiPersone?: boolean) {
        var tmpList;
        let tmp = this.listaContatti.filter((contatto) => {
            return contatto.getIdCompany() == idCompany;
        });

        tmpList = tmp;
        if (includiPersone != undefined) {
            if (!includiPersone) {
                tmpList = tmp.filter((contatto) => {
                    return contatto.getType().toLowerCase() == "azienda" ;
                });
            }
        }
        if (page && size) {
            if (page != -1 && size != -1) {
                tmpList = tmpList.slice((page - 1) * size, ((page - 1) * size) + size);
            }
        }
    
        return tmpList;
    }

    static getListaAssociatiAsync(idCompany:number, page?: number, size?: number, timeout = 1000): Observable<Contatto[]> {
        return new Observable<Contatto[]>(observer => {
            observer.next(this.getListaAssociati(idCompany, page, size, UtilsSetting.includiPersone));
            observer.complete();
        }).pipe(delay(timeout));
    }
}