import { ToastController } from 'ionic-angular';
import { Contatto } from '../class/contatto'
import { Utente } from "../class/utente";

export class Utils {
    static debug:Boolean = true;
    static userLogged:Utente;
    static ivaValue:number;
    static emailToSend:string = "";
    static ipws:string = "";

    static writeLog(message:String) {
        if (this.debug) {
            console.log(message);
        }
    }

    static setUserLogged(_user:Utente) {
        this.userLogged = _user;
    }

    static presentToast(message:string, context:ToastController) {
        const toast = context.create({
          message: message,
          duration: 4000
        });
        
        toast.present();
    }
}