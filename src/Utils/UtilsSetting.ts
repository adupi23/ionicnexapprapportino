import { CampiSetting } from './../class/campiSetting';
export class UtilsSetting {
    static campi:CampiSetting[];
    static includiPersone:boolean;

    static setCampi(_campi:CampiSetting[]) {
        this.campi = _campi;
    }
}