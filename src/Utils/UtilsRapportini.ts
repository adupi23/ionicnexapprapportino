import { RigaRapportino } from './../class/rigaRapportino';
import { Utils } from './Utils';
import { Rapportino } from './../class/rapportino';
import { UtilsCommessa } from './UtilsCommessa';
import { UtilsContatti } from './UtilsContatti';

export class UtilsRapportini {
    static listaRapportini:Rapportino[];

    static setListaRapportini(_listaRapportini:Rapportino[]) {
        this.listaRapportini = _listaRapportini;
    }

    static addListaRapportini(contatto:Rapportino) {
        this.listaRapportini.push(contatto);
    }

    static removeListaRapportini(index:number) {
        this.listaRapportini.splice(index, 1);
    }

    static getDateString(date:string) {
        var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        let dateS = new Date(date).toLocaleDateString('it-IT', options).toString();
        let aDateS = dateS.split("/");
        return aDateS[2] + "-" + aDateS[1] + "-" + aDateS[0];
    }

    static getDateIt(date:string) {
        var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        return new Date(date).toLocaleDateString('it-IT', options).toString();
    }

    static getDataCommessa(riga:RigaRapportino) {
        return UtilsRapportini.getDateIt(riga.data);
    }

    static getDateTimeISOString(date:Date) {
        var tzoffset = new Date().getTimezoneOffset() * 60000;
        return new Date(date.getTime() - tzoffset).toISOString();
    }

    static sortByDate(listaRigaRapportino:RigaRapportino[]) {
        return listaRigaRapportino.sort((a, b) => {
            if (new Date(a.data) > new Date(b.data)) return 1;
            if (new Date(a.data) < new Date(b.data)) return -1;
            return 0;
            // return new Date(a.data) - new Date(b.data);
        });
    }

    static getCommessa(idCommessa:number) {
        return UtilsCommessa.listaCommesse.filter(c => {
            return c.getID() == idCommessa;
        })[0];
    }

    static getCliente(idCommessa:number) {
        let commessa =  UtilsCommessa.listaCommesse.filter(c => {
            return c.getID() == idCommessa;
        })[0];

        return UtilsContatti.listaContatti.filter(c => {
            return c.getID() == commessa.getIdCliente();
        })[0];
    }

    static getRapportino(idRapportino:number) {
        return UtilsRapportini.listaRapportini.filter(r => {
            return r.id == idRapportino;
        })[0];
    }
}