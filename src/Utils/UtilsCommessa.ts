import { Utils } from './Utils';
import { Commessa } from "../class/commessa";
import { Contatto } from "../class/contatto";

export class UtilsCommessa {
    static listaCommesse:Commessa[];

    static setListaCommesse(_listaCommesse:Commessa[]) {
        this.listaCommesse = _listaCommesse;
    }

    static addListaCommesse(commessa:Commessa) {
        this.listaCommesse.push(commessa);
    }

    static removeListaCommesse(index:number) {
        this.listaCommesse.splice(index, 1);
    }

    static checkCommessa(p:Contatto, a:Contatto, n:string, libero:string[]):any[] {

        if (p == null) {
            return [false, "Scegliere Cliente"];
        }
        if (a == null) {
            return [false, "Scegliere Da chi hai lavorato"];
        }
        if (n == '') {
            return [false, "Inserire numero Commessa"];
        }
        // libero.forEach(value => {
        //     if (value.trim() == '') {
        //     return [false, "Compila tutti i campi"];
        //     }
        // })

        return [true, ""];
    }

    static checkChanges(commessa:Commessa, p:Contatto, a:Contatto, n:string, libero:string[]) {
        var result:boolean = false;

        if (p != null) {
            if (p.getID() != commessa.getIdCliente()) {
                result = true;
                Utils.writeLog("Principale");
            }
        }
        if (a != null) {
            if (a.getID() != commessa.getIdLavorato()) {
                result = true;
                Utils.writeLog("Associato");
            }
        }
        if (n != commessa.getNumero()) {
            result = true;
            Utils.writeLog("Numero");
        }
        // if (libero.length != commessa.getLibero().length) {
        //     result = true;
        //     Utils.writeLog("Libero Length " + libero.length + " " + commessa.getLibero().length);
        // } else {
            for (var i = 0; i < libero.length; i++) {
                if (libero[i] != commessa.getLibero()[i]) {
                    result = true;
                    Utils.writeLog("Libero " + i + " value");
                    return result;
                }
            }
        // }

        return result;
    }
}