import { UtilsRapportini } from './../Utils/UtilsRapportini';
import { UtilsSetting } from './../Utils/UtilsSetting';
import { SettingPage } from './../pages/setting/setting';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { DatabaseProvider } from '../providers/database/database';
import { Utils } from '../Utils/Utils';
import { UtilsContatti } from './../Utils/UtilsContatti';
import { ContattiPage } from '../pages/contatti/contatti';
import { CommessePage } from './../pages/commesse/commesse';
import { UtilsCommessa } from '../Utils/UtilsCommessa';
import { RapportiniPage } from './../pages/rapportini/rapportini';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;
  users = [];

  list:any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              private databaseprovider: DatabaseProvider, private storage: Storage, public http:Http) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Contatti', component: ContattiPage },
      { title: 'Commesse', component: CommessePage },
      { title: 'Rapportini', component: RapportiniPage},
      { title: 'Impostazioni', component: SettingPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      
      this.databaseprovider.getDatabaseState().subscribe(rdy => {
        if (rdy) {
          Utils.writeLog('Database Pronto');
          this.databaseprovider.getContatti()
          .then(listaContatti => {
            UtilsContatti.setListaContatti(listaContatti);
          })
        }
      })

      this.databaseprovider.getDatabaseState().subscribe(rdy => {
        if (rdy) {
          this.databaseprovider.getCampiSettings()
          .then(campiSetting => {
            UtilsSetting.setCampi(campiSetting);
          })
        }
      })

      this.databaseprovider.getDatabaseState().subscribe(rdy => {
        if (rdy) {
          this.databaseprovider.getCommesse()
          .then(commesse => {
            UtilsCommessa.setListaCommesse(commesse);
          }) 
        }
      })

      this.databaseprovider.getDatabaseState().subscribe(rdy => {
        if (rdy) {
          this.databaseprovider.getRapportini()
          .then(rapportini => {
            UtilsRapportini.setListaRapportini(rapportini);
          }) 
        }
      })

      this.storage.get('includi_persone').then(value => {
        UtilsSetting.includiPersone = value;
        Utils.writeLog("Includi Persone: " + UtilsSetting.includiPersone);
      });
      
      this.getIva();

      this.getEmailToSend();

      this.getIPWS();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, {}, {animate: true, direction: 'forward'});
  }

  logout() {
    this.nav.setRoot(LoginPage, {}, {animate: true, direction: 'back'});
  }

  getIva() {
    Utils.ivaValue = 22.00;
  }

  getEmailToSend() {
    this.storage.get('email_to_send').then((val:string) => {
      if (val != '') {
        Utils.emailToSend = val;
      } else {
        Utils.emailToSend = '';
      }
    });
  }

  getIPWS() {
    this.storage.get('ipws').then((val:string) => {
      if (val != '') {
        Utils.ipws = val;
      } else {
        Utils.ipws = '';
      }
    });
  }
}
