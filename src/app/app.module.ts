import { DocumentiPage } from './../pages/documenti/documenti';
import { HttpClientModule } from '@angular/common/http';
import { PopoverPage } from './../pages/popover/popover';
import { DettagliRapportinoPage } from './../pages/dettagli-rapportino/dettagli-rapportino';
import { RapportiniPage } from './../pages/rapportini/rapportini';
import { DettagliComessaPage } from './../pages/dettagli-comessa/dettagli-comessa';
import { DettagliSettingPage } from './../pages/dettagli-setting/dettagli-setting';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';

import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';

import { DatabaseProvider } from '../providers/database/database';
import { SelectSearchableModule } from 'ionic-select-searchable';

import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { SQLite } from '@ionic-native/sqlite';
import { ContattiPage } from '../pages/contatti/contatti';
import { CommessePage } from '../pages/commesse/commesse';
import { ContattoDettagliPage } from '../pages/contatto-dettagli/contatto-dettagli';
import { SettingPage } from '../pages/setting/setting';
import { CallNumber } from '@ionic-native/call-number';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { RiepilogoPage } from '../pages/riepilogo/riepilogo';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ContattiPage,
    ContattoDettagliPage,
    CommessePage,
    SettingPage,
    DettagliSettingPage,
    DettagliComessaPage,
    RapportiniPage,
    DettagliRapportinoPage,
    PopoverPage,
    RiepilogoPage,
    DocumentiPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    SelectSearchableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ContattiPage,
    ContattoDettagliPage,
    CommessePage,
    SettingPage,
    DettagliSettingPage,
    DettagliComessaPage,
    RapportiniPage,
    DettagliRapportinoPage,
    PopoverPage,
    RiepilogoPage,
    DocumentiPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    SQLitePorter,
    DatabaseProvider,
    CallNumber,
    ScreenOrientation,
    Camera,
    File
  ]
})
export class AppModule {}
