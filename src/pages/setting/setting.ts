import { Storage } from '@ionic/storage';
import { DettagliSettingPage } from './../dettagli-setting/dettagli-setting';
import { DatabaseProvider } from './../../providers/database/database';
import { UtilsSetting } from './../../Utils/UtilsSetting';
import { CampiSetting } from './../../class/campiSetting';
import { Utils } from './../../Utils/Utils';
import { Component } from '@angular/core';
import { NavController, NavParams, Toggle } from 'ionic-angular';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  campi:CampiSetting[];
  persone:boolean;
  emailToSend:string;
  ipws:string;
  // name:string[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private databaseprovider: DatabaseProvider, private storage: Storage) {
      this.campi = UtilsSetting.campi.filter((campo) => {
        return campo.getIDCampo() == 0;
      });
      this.persone = UtilsSetting.includiPersone;
      this.emailToSend = Utils.emailToSend;
      this.ipws = Utils.ipws;

      Utils.writeLog("N campi: "+this.campi.length);
  }

  ionViewDidEnter(){

  }

  dettagli(campo:CampiSetting) {
    this.navCtrl.push(DettagliSettingPage, {
      setting: campo,
      modal: false
    });
  }

  changeIncludiPersone() {
    UtilsSetting.includiPersone = this.persone;
    this.storage.set('includi_persone', this.persone);

    Utils.writeLog("Includi Persone: " + UtilsSetting.includiPersone);
  }

  changeEmail() {
    Utils.emailToSend = this.emailToSend;
    this.storage.set('email_to_send', Utils.emailToSend);
  }

  changeIP() {
    Utils.ipws = this.ipws;
    this.storage.set('ipws', Utils.ipws);
  }

}
