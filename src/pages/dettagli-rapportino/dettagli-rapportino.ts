import { File } from '@ionic-native/file';
import { DocumentiPage } from './../documenti/documenti';
import { RiepilogoPage } from './../riepilogo/riepilogo';
import { PopoverPage } from './../popover/popover';
import { UtilsContatti } from './../../Utils/UtilsContatti';
import { RigaRapportino } from './../../class/rigaRapportino';
import { Commessa } from './../../class/commessa';
import { UtilsCommessa } from './../../Utils/UtilsCommessa';
import { UtilsRapportini } from './../../Utils/UtilsRapportini';
import { DatabaseProvider } from './../../providers/database/database';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, Navbar, ToastController, PopoverController } from 'ionic-angular';
import { Rapportino } from '../../class/rapportino';
import { Utils } from '../../Utils/Utils';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { UtilsSetting } from '../../Utils/UtilsSetting';
import { Documento } from '../../class/documento';

@Component({
  selector: 'page-dettagli-rapportino',
  templateUrl: 'dettagli-rapportino.html',
})

export class DettagliRapportinoPage {
  @ViewChild(Navbar) navbar: Navbar;

  //rapportino
  rapportinoSelected:Rapportino;
  commesseSelected = [];
  nuovoRapportino:boolean;
  nomeRapportino:string = "";

  //rigaRapportino
  listaRigaRapportino:RigaRapportino[] = [];
  // datiRiga:any;
  listaDatiRiga:any[] = [];

  //commesse
  showCommesse:boolean;
  listaCommesseUsed = [];
  listaCommesse = [];
  listaCommesseBool = [];
  startIndex = 0;
  lastIndex = 0;
  enabled = true;
  //testo per la ricerca
  showSearch:boolean = false;
  searchText:string = "";
  oldSearchText:string = "";
  searching:boolean = false;

  public unregisterBackButtonAction: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private databaseprovider: DatabaseProvider, public platform: Platform,
              private alertCtrl:AlertController, private toastCtrl:ToastController,
              public popoverCtrl: PopoverController, private file: File) {
    this.rapportinoSelected = navParams.get('rapportino');
    this.nuovoRapportino = navParams.get('nuovo');
    this.showCommesse = this.nuovoRapportino;

    this.nomeRapportino = this.rapportinoSelected.nome;

    Utils.writeLog("SHOW COMMESSE: " + this.showCommesse);

    this.listaCommesseUsed = UtilsCommessa.listaCommesse;
    this.loadCommesse();
    Utils.writeLog("Commesse: " + this.listaCommesse.length);

    if (!this.nuovoRapportino) {
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      this.databaseprovider.getRigaRapportino(this.rapportinoSelected.id).then((result) => {
        let tmp = result;
        this.listaRigaRapportino = UtilsRapportini.sortByDate(tmp);
        this.loadDati();
      })
    }

    platform.registerBackButtonAction(() => {
      Utils.writeLog("Back pressed");
      this.backButtonClick();
    });
  }

  backButtonClick() {
    if (this.nuovoRapportino) {
      if (this.nomeRapportino.trim() != "" && this.commesseSelected.length > 0) {
        this.presentSave();
      } else {
        this.presentDelete();
      }
    } else {
      this.navCtrl.pop();
    }
  }

  changeName() {
    if (!this.nuovoRapportino) {
      let index = UtilsRapportini.listaRapportini.indexOf(this.rapportinoSelected);
      let value = this.nomeRapportino;
      this.rapportinoSelected.nome = value;
      Utils.writeLog("name value: " + this.rapportinoSelected.nome + " " + this.rapportinoSelected.id);
      UtilsRapportini.listaRapportini[index].nome = value;
      this.databaseprovider.updateRapportino(this.rapportinoSelected).then(() => {
        Utils.writeLog("Update success");
      });
    }
  }

  presentSave() {
    let alert = this.alertCtrl.create({
      //title: 'Attenzione',
      title: 'Sono state apportate delle modifiche',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Salva Modifiche',
          role: 'destructive',
          handler: () => {
            this.save();
          }
        },
        {
          text: 'Continua modifiche',
          handler: () => {}
        },
        {
          text: 'Annulla Modifiche',
          handler: () => {
            if (this.nuovoRapportino) {
              this.deleteFromDB(this.rapportinoSelected.id);
            }
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  save() {
    if (this.nomeRapportino.trim() != "") {
      if (this.commesseSelected.length > 0) {
        this.rapportinoSelected.nome = this.nomeRapportino;
        if (this.nuovoRapportino) {
          UtilsRapportini.listaRapportini.unshift(this.rapportinoSelected);
        }
        this.databaseprovider.updateRapportino(this.rapportinoSelected).then(() => {
          //aggiungere righe rapportini tramite le commesse
          this.commesseSelected.forEach(commessa => {
            this.databaseprovider.addRigaRapportino(this.rapportinoSelected.id, commessa.getID()).then((result) => {
              Utils.writeLog("Riga Aggiunta");
              this.listaCommesse.forEach(commessa => {
                commessa.restoreCount();
              });
              this.listaRigaRapportino.push(result);
              let tmp = UtilsRapportini.sortByDate(this.listaRigaRapportino);
              this.listaRigaRapportino = tmp;
              this.listaRigaRapportino.forEach(riga => {
                Utils.writeLog("Riga: " + riga.id + " data: " + riga.data);
              });
              this.loadDati();
              this.nuovoRapportino = false;
            });
          });
          // this.loadDati();
          this.commesseSelected = [];
        });
        if (this.showCommesse) {
          this.showCommesse = false;
        } else {
          this.navCtrl.pop();
        }
      } else {
        if (this.showCommesse) {
          Utils.presentToast("Dati non compilati correttamente, seleziona almeno una commessa", this.toastCtrl);
        }
      }
    } else {
      if (this.showCommesse) {
        Utils.presentToast("Dati non compilati correttamente, scrivi un nome per il rapportino", this.toastCtrl);
      }
      Utils.writeLog("Dati non compilati correttamente");
    }
  }

  presentDelete() {
    let alert = this.alertCtrl.create({
      //title: 'Attenzione',
      title: 'Sei sicuro di voler uscire ? Questa rapportino non sarà salvata',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Rimani',
          role: 'destructive',
          handler: () => {}
        },
        {
          text: 'Esci',
          handler: () => {
            this.deleteFromDB(this.rapportinoSelected.id);
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  deleteFromDB(id:number) {
    this.databaseprovider.deleteRapportino(id).then(() => {
      // CommessePage.listChanges = true;
    });
  }

  cancel() {
    if (this.nuovoRapportino) {
      this.presentDelete();
    } else {
      this.showCommesse = false;
    }
    this.listaCommesse.forEach(commessa => {
      commessa.restoreCount();
    });
    this.commesseSelected = [];
  }

  //lista commesse
  doInfinite(): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (this.listaCommesse.length < this.listaCommesseUsed.length) {
          this.loadCommesse();
        }
        Utils.writeLog("Commesse caricate visibili: " + this.listaCommesse.length);
        // this.toTopButtonVisible = true;
        this.searching = false;
        resolve(this.listaCommesse);
      }, 700);
    })
  }

  loadCommesse() {
    if (this.listaCommesseUsed.length > 0) {
      this.lastIndex = this.listaCommesseUsed.length
      if (this.lastIndex > (20 + this.startIndex)) {
        this.lastIndex = 20 + this.startIndex;
      }
      for (var i = this.startIndex; i < this.lastIndex; i++) {
        this.listaCommesse.push(this.listaCommesseUsed[i]);
      }
    }
    this.startIndex = this.lastIndex;
    if (this.listaCommesse.length == this.listaCommesseUsed.length) {
      this.enabled = false;
    }
    this.searching = false;
  }

  ionViewDidLoad() {
    this.navbar.backButtonClick = () => {
      this.backButtonClick();
    }
    if (!this.nuovoRapportino) {
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    }
  }

  addCommesse() {
    this.showCommesse = true;
    // this.screenOrientation.unlock();
  }

  add(commessa:Commessa) {
    commessa.addCount();

    this.commesseSelected.push(commessa);
  }

  remove(commessa:Commessa) {
    if (commessa.getCount() > 0) {
      commessa.removeCount();
      let index = this.listaCommesse.indexOf(commessa);
      this.commesseSelected.splice(index, 1);
    }
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    // this.screenOrientation.unlock();
    this.listaCommesse.forEach(commessa => {
      commessa.restoreCount();
    });
    this.commesseSelected = [];
  }

  getNCommessa(idCommessa:number) {
    let commessa =  UtilsCommessa.listaCommesse.filter(c => {
        return c.getID() == idCommessa;
    })[0];
    return commessa.getNumero().toString();
  }

  getClienteCommessa(idCommessa:number) {
      let commessa =  UtilsCommessa.listaCommesse.filter(c => {
          return c.getID() == idCommessa;
      })[0];

      let cliente = UtilsContatti.listaContatti.filter(c => {
          return c.getID() == commessa.getIdCliente();
      })[0];
      return cliente.getNomeCognome().toString();
  }

  getDataCommessa(riga:RigaRapportino) {
    return UtilsRapportini.getDateIt(riga.data);
  }

  loadDati() {
    this.listaDatiRiga = [];
    this.listaRigaRapportino.forEach(rr => {
      let dati = {
        data: this.getDataCommessa(rr)
      }
      this.listaDatiRiga.push(dati);
      // Utils.writeLog("DATE: " + dati.data);
    });
  }

  openRigaOption(Event, riga:RigaRapportino) {
    let index = this.listaRigaRapportino.indexOf(riga);
    let popover = this.popoverCtrl.create(PopoverPage, { riga: riga, index: index });
    popover.onDidDismiss(data => {
      Utils.writeLog("Popover Dismissed");
      if (data) {
        this.presentDeleteRiga(riga, index);
      }
      // this.listaDatiRiga[index].data = this.getDataCommessa(riga);
      let tmp = UtilsRapportini.sortByDate(this.listaRigaRapportino);
      this.listaRigaRapportino = tmp;
      this.loadDati();
    })
    popover.present({
      ev: event
    });
  }

  creaFattura() {
    this.navCtrl.push(RiepilogoPage, {
      righe: this.listaRigaRapportino
    });
  }

  openDocs(riga:RigaRapportino) {
    this.navCtrl.push(DocumentiPage, {
      riga: riga
    });
  }

  presentDeleteRiga(riga:RigaRapportino, index:number) {
    let alert = this.alertCtrl.create({
      title: 'Sei sicuro di voler eliminare questa riga ? Eliminerai anche i documenti associati',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Elimina',
          role: 'destructive',
          handler: () => {
            this.deleteRiga(riga, index);
          }
        },
        {
          text: 'Annulla',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

  deleteRiga(riga:RigaRapportino, index:number) {
    this.listaRigaRapportino.splice(index, 1);
    this.databaseprovider.getDocs(riga.id).then(data => {
      let listaDoc:Documento[] = data;
      listaDoc.forEach(doc => {
        var nameImg = doc.path.substr(doc.path.lastIndexOf('/') + 1);
        var pathImg = doc.path.substr(0, doc.path.lastIndexOf('/') + 1);
        this.file.removeFile(pathImg, nameImg).then(success => {
          if (success) {
            this.databaseprovider.deleteDoc(doc.id).then(() => {
              Utils.writeLog("Delete doc " + doc.id);
            });
          }
        }, err => {
          Utils.writeLog("Error del: " + JSON.stringify(err));
        });
      })
    });
    this.databaseprovider.deleteRigaRapportino(riga.id).then(() => {
      Utils.writeLog("Riga " + riga.id + " eliminata");
    });
  }

}
