import { UtilsCommessa } from './../../Utils/UtilsCommessa';
import { CommessePage } from './../commesse/commesse';
import { DettagliSettingPage } from './../dettagli-setting/dettagli-setting';
import { UtilsSetting } from './../../Utils/UtilsSetting';
import { CampiSetting } from './../../class/campiSetting';
import { DatabaseProvider } from './../../providers/database/database';
import { UtilsContatti } from './../../Utils/UtilsContatti';
import { Contatto } from './../../class/contatto';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, Navbar, ModalController, AlertController } from 'ionic-angular';
import { Commessa } from '../../class/commessa';
import { Utils } from '../../Utils/Utils';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-dettagli-comessa',
  templateUrl: 'dettagli-comessa.html',
})
export class DettagliComessaPage {
  @ViewChild(Navbar) navbar: Navbar;
  @ViewChild(SelectSearchableComponent) searchCliente: SelectSearchableComponent;

  commessaSelected:Commessa;
  nuovaCommessa:boolean;
  contattiPrincipali:Contatto[];
  principaleSelected:Contatto;
  contattiAssociati:Contatto[];
  associatoSelected:Contatto;
  lavoratoEnabled:boolean = false;
  idToUse = 0;

  page = 2;
  totalPages;
  subscription: Subscription;

  //
  libero:CampiSetting[] = [];
  liberoValue:string[] = [];
  numero:string = '';
  //

  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, platform:Platform, 
              private databaseprovider: DatabaseProvider, public modalCtrl: ModalController,
              private alertCtrl:AlertController) {
    this.commessaSelected = navParams.get('commessa');
    this.nuovaCommessa = navParams.get('nuovo');

    this.contattiPrincipali = UtilsContatti.getListaAssociati(0);
    this.totalPages = Math.round(this.contattiPrincipali.length / 20);
    
    Utils.writeLog("Total pages: "+this.totalPages);

    // platform.ready().then(() => {
      if (!this.nuovaCommessa) {
        this.principaleSelected = UtilsContatti.listaContatti.filter(c => {
          return c.getID() == this.commessaSelected.getIdCliente();
        })[0];

        this.associatoSelected = UtilsContatti.listaContatti.filter(c => {
          return c.getID() == this.commessaSelected.getIdLavorato();
        })[0];
        this.numero = this.commessaSelected.getNumero();
      }
      this.libero = this.filterOption();
      for(var i = 0;i < this.libero.length; i++) {
        let v = this.commessaSelected.getLibero()[i] != undefined ? this.commessaSelected.getLibero()[i] : '';
        this.liberoValue.push(v);
        Utils.writeLog("Libero v: " + v);
      }

      Utils.writeLog("Campi tot array: " + UtilsSetting.campi.length);
      Utils.writeLog("Libero array: " + this.libero.length);
    // });

    platform.registerBackButtonAction(() => {
      Utils.writeLog("Back pressed");
      this.backButtonClick();
    });
  }

  backButtonClick() {
    if (this.principaleSelected == null && this.associatoSelected == null) {
      Utils.writeLog("Principale: " + this.principaleSelected + " associato: " + this.associatoSelected);
      this.presentDelete();
    } else {
      if (UtilsCommessa.checkChanges(this.commessaSelected, this.principaleSelected, this.associatoSelected, this.numero, this.liberoValue)) {
        this.presentSave();
      } else {
        Utils.writeLog("Back button -> Nuova commessa ? "+this.nuovaCommessa);
        if (this.nuovaCommessa) {
          this.presentDelete();
        } else {
          this.navCtrl.pop();
        }
      }
    }
  }

  //Utils cliente e da chi
  filterContatti(cPrincipali: Contatto[], text: string) {
    return cPrincipali.filter(contatto => {
      return contatto.getNomeCognome().toLowerCase().indexOf(text.toLowerCase()) > -1;
    });
  }

  searchContatti(event: { component: SelectSearchableComponent, text: string }, tipo:string) {
    if (tipo == "selectCliente") {
      this.idToUse = 0;
    } else {
      if (this.principaleSelected != null) {
        this.idToUse = this.principaleSelected.getID();
      }
    }
    
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    // Close any running subscription.
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    Utils.writeLog("Filtro: " + text);
    if (!text) {
      // Close any running subscription.
      if (this.subscription) {
        this.subscription.unsubscribe();
      }

      event.component.items = UtilsContatti.getListaAssociati(this.idToUse, 1, 20, UtilsSetting.includiPersone);
      
      // Enable and start infinite scroll from the beginning.
      this.page = 2;
      this.totalPages = Math.round(UtilsContatti.getListaAssociati(this.idToUse, -1, -1, UtilsSetting.includiPersone).length / 20);
      event.component.endSearch();
      event.component.enableInfiniteScroll();
      return;
    }

    // , 1, 20
    this.subscription = UtilsContatti.getListaAssociatiAsync(this.idToUse).subscribe(contatti => {
      // Subscription will be closed when unsubscribed manually.
      Utils.writeLog("Subscription status: " + this.subscription.closed)
      if (this.subscription.closed) {
        return;
      } else {
        event.component.endSearch();
        event.component.enableInfiniteScroll();
      }

      let contattiFilter = this.filterContatti(contatti, text);
      event.component.items = contattiFilter.slice(0, 20);
      this.page = 2;
      this.totalPages = Math.round(contattiFilter.length / 20);
      if (this.totalPages == 0) { 
        this.totalPages += 1; 
      } else if (Math.abs((contattiFilter.length - ((this.totalPages) * 20))) < 10 && Math.abs((contattiFilter.length - ((this.totalPages) * 20))) > 0) {
        this.totalPages += 1;
      }

      Utils.writeLog("Total pages: " + this.totalPages);

      Utils.writeLog("Contatti trovati: " + contattiFilter.length + " per: " + this.totalPages + " pagine");
      Utils.writeLog("Contatti visibili: " + event.component.items.length);
      event.component.endSearch();
    });
  }

  getMoreContatti(event: { component: SelectSearchableComponent, text: string }) {
    let text = (event.text || '').trim().toLowerCase();

    // There're no more contacts - disable infinite scroll.
    if (this.page > this.totalPages) {
      event.component.disableInfiniteScroll();
      return;
    }

    let size = 20;
    if (text && (this.page == this.totalPages)) {
      let list = UtilsContatti.getListaAssociati(this.idToUse);
      if (text) {
        list = this.filterContatti(list, text);
      }
      size = list.length - ((this.page - 1) * size);
    }
    Utils.writeLog("Elementi da aggiungere: " + size);

    UtilsContatti.getListaAssociatiAsync(this.idToUse, this.page, size).subscribe(contatti => {
      contatti = event.component.items.concat(contatti);

      // if (text) {
      //   contatti = this.filterContatti(contatti, text);
      // }

      event.component.items = contatti;
      event.component.endInfiniteScroll();
      Utils.writeLog("Pagina n: " + this.page);
      this.page++;
      Utils.writeLog("Contatti visibili: " + event.component.items.length);
    });
  }

  changeCliente(event: { component: SelectSearchableComponent, value: any }, tipo:string) {
    if (tipo == "selectCliente") {
      this.principaleSelected = event.value;
      this.contattiAssociati = UtilsContatti.getListaAssociati(this.principaleSelected.getID(), -1, -1, UtilsSetting.includiPersone);
      this.associatoSelected = null;
      this.lavoratoEnabled = true;
    } else {
      this.associatoSelected = event.value;
    }
  }
  //

  filterOption() {
    return UtilsSetting.campi.filter((campo) => {
      if (campo.getIDCampo() == 0) {
        if (campo.getAttivo().toString().toLowerCase() == "true") {
          return true;
        }
      }
      return false;
    });
  }

  scegliOpzione(l:CampiSetting) {
    let modal = this.modalCtrl.create(DettagliSettingPage, {
      setting: l,
      modal: true
    });
    modal.onDidDismiss(value => {
      this.liberoValue[this.libero.indexOf(l)] = value;
    });
    modal.present();
  }

  ionViewDidLoad() {
    Utils.writeLog("Commessa Selezionata: " + this.commessaSelected.getID() + " " + this.commessaSelected.getNumero() + " " + this.nuovaCommessa);
    this.navbar.backButtonClick = () => {
      this.backButtonClick();
    }
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  presentSave() {
    let alert = this.alertCtrl.create({
      //title: 'Attenzione',
      title: 'Sono state apportate delle modifiche',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Salva Modifiche',
          role: 'destructive',
          handler: () => {
            this.save();
          }
        },
        {
          text: 'Continua modifiche',
          handler: () => {}
        },
        {
          text: 'Annulla Modifiche',
          handler: () => {
            if (this.nuovaCommessa) {
              this.deleteFromDB(this.commessaSelected.getID());
            }
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  save() {
    if (UtilsCommessa.checkCommessa(this.principaleSelected, this.associatoSelected, this.numero, this.liberoValue)) {
      if (UtilsCommessa.checkChanges(this.commessaSelected, this.principaleSelected, this.associatoSelected, this.numero, this.liberoValue)) {
        this.commessaSelected.setIdCliente(this.principaleSelected.getID());
        this.commessaSelected.setIdLavorato(this.associatoSelected.getID());
        this.commessaSelected.setNumero(this.numero);
        this.liberoValue.forEach(l => {
          Utils.writeLog("libero v: " + (l == null ? 'vuoto' : l));
        });
        this.commessaSelected.setLibero(this.liberoValue);

        if (this.nuovaCommessa) {
          // UtilsCommessa.addListaCommesse(this.commessaSelected);
          UtilsCommessa.listaCommesse.unshift(this.commessaSelected);
        }
        this.databaseprovider.updateCommessa(this.commessaSelected).then(() => {
          CommessePage.listChanges = true;
        });
      }
      this.navCtrl.pop();
    } else {
      Utils.writeLog("Dati non compilati correttamente");
    }
  }

  presentDelete() {
    let alert = this.alertCtrl.create({
      //title: 'Attenzione',
      title: 'Sei sicuro di voler uscire ? Questa commessa non sarà salvata',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Rimani',
          role: 'destructive',
          handler: () => {}
        },
        {
          text: 'Esci',
          handler: () => {
            this.deleteFromDB(this.commessaSelected.getID());
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  deleteFromDB(id:number) {
    this.databaseprovider.deleteCommessa(id).then(() => {
      CommessePage.listChanges = true;
    });
  }
  
}
