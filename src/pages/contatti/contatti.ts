import { CallNumber } from '@ionic-native/call-number';
import { DatabaseProvider } from './../../providers/database/database';
import { ContattoDettagliPage } from './../contatto-dettagli/contatto-dettagli';
import { UtilsContatti } from './../../Utils/UtilsContatti';
import { Utils } from './../../Utils/Utils';
import { Contatto } from './../../class/contatto';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, ItemSliding, Content, AlertController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'page-contatti',
  templateUrl: 'contatti.html',
})
export class ContattiPage {
  @ViewChild(Content) content: Content;

  static listChanges:boolean = false;
  //lista dei contatti visibili
  listaContatti = [];
  //lista da utilizzare per riempire listaContatti
  listaContattiUsed = [];
  //Indici per il caricamento parziale dei dati
  startIndex = 0;
  lastIndex = 0;
  //diventa false quando non ci sono più dati da poter caricare
  enabled = true;
  //true dopo il caricamento della seconda parte di dati, il bottene serve a tornare in cima
  // toTopButtonVisible:boolean = false;

  //testo per la ricerca
  showSearch:boolean = false;
  searchText:string = "";
  oldSearchText:string = "";
  //true quando viene mostrato il caricamento dei dati filtrati
  searching:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform,
              private alertCtrl: AlertController, private databaseprovider: DatabaseProvider,
              private toastCtrl: ToastController, private callNumber: CallNumber) {
    //platform.ready().then((() => this.screenHeight = platform.height()));
    this.listaContattiUsed = UtilsContatti.listaContatti;
    this.loadContatti();
    Utils.writeLog("Totale contatti: " + UtilsContatti.listaContatti.length);
    Utils.writeLog("Contatti caricati: " + this.listaContatti.length);
  }

  nuovo() {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.databaseprovider.addContatto(0)
        .then(contatto => {
          if (contatto != null) {
            this.openContatto(contatto, true);
          }
        })
      }
    });
  }

  doInfinite(): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (this.listaContatti.length < this.listaContattiUsed.length) {
          this.loadContatti();
        }
        Utils.writeLog("Contatti caricati visibili: " + this.listaContatti.length);
        // this.toTopButtonVisible = true;
        this.searching = false;
        resolve(this.listaContatti);
      }, 700);
    })
  }

  loadContatti() {
    if (this.listaContattiUsed.length > 0) {
      this.lastIndex = this.listaContattiUsed.length
      if (this.lastIndex > (20 + this.startIndex)) {
        this.lastIndex = 20 + this.startIndex;
      }
      for (var i = this.startIndex; i < this.lastIndex; i++) {
        this.listaContatti.push(this.listaContattiUsed[i]);
      }
    }
    this.startIndex = this.lastIndex;
    if (this.listaContatti.length == this.listaContattiUsed.length) {
      this.enabled = false;
    }
    this.searching = false;
  }

  scrollTop() {
    this.content.scrollToTop();
  }

  openSearch() {
    if (!this.showSearch) {
      this.searchText = "";
      this.showSearch = true;
    } else {
      this.showSearch = false;
    }
  }

  openContatto(contatto:Contatto, nuovo:boolean) {
    this.navCtrl.push(ContattoDettagliPage, {
      contatto: contatto,
      nuovo: nuovo
    });
  }

  delete(item:ItemSliding, contatto:Contatto) {
    //item.close()
    let indexGlobal = UtilsContatti.listaContatti.indexOf(contatto);
    if (contatto.getIdCompany() > 0) {
      if(indexGlobal > -1){
        this.doDelete(indexGlobal, contatto);
      }
    } else {
      //Utils.writeLog(contatto.getNomeCognome() + " è una compagnia");
      if (UtilsContatti.getListaAssociati(contatto.getID()).length > 0) {
        //messaggio
        Utils.presentToast('Prima di eliminare questa azienda dovresti eliminare tutti i suoi contatti associati', this.toastCtrl);
      } else {
        //elimina
        this.doDelete(indexGlobal, contatto);
      }
    }
  }

  doDelete(index:number, contatto:Contatto) {
    let alert = this.alertCtrl.create({
      title: 'Attenzione',
      message: 'Sei sicuro di voler eliminare questo contatto?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Elimina',
          handler: () => {
            UtilsContatti.removeListaContatti(index);
            // this.listaContattiUsed.splice(indexUsed, 1);
            // this.listaContatti.splice(index, 1);

            this.searching = true;
            this.enabled = true;
            
            this.startIndex = 0;
            this.lastIndex = 0;
            this.listaContatti = [];
            this.listaContattiUsed = UtilsContatti.listaContatti;
            if (this.searchText != "") {
              this.listaContattiUsed = this.filterList(this.searchText);
            }
            this.loadContatti();
            Utils.writeLog(contatto.getNomeCognome() + " Eliminato");
            this.databaseprovider.deleteContatto(contatto.getID());
          }
        }
      ]
    });
    alert.present();
  }

  //search
  resetList() {
    Utils.writeLog("reset list");
    this.startIndex = 0;
    this.lastIndex = 0;
    this.enabled = true;
    this.listaContatti = [];
    this.listaContattiUsed = UtilsContatti.listaContatti;
    this.loadContatti();
  }

  onClearSearchbar(ev: any) {
    Utils.writeLog("Clear Searchbar");
    this.searching = true;
    this.resetList();
    this.showSearch = false;
  }

  onCancelSearchbar(ev: any) {
    Utils.writeLog("Cancel Searchbar");
    this.searching = true;
    this.resetList();
    this.showSearch = false;
  }

  changedInput(ev: any) {
    Utils.writeLog("text changed");
    this.searching = true;
    this.enabled = true;

    setTimeout(() => {
      this.searching = false;
      if (this.searchText != "") {
        let tmpList = this.filterList(this.searchText);
        Utils.writeLog("Contatti filtrati: " + tmpList.length);
        this.startIndex = 0;
        this.lastIndex = 0;
        this.listaContatti = [];
        this.listaContattiUsed = tmpList;
        this.loadContatti();
        Utils.writeLog("Contatti visibili dopo filtro: " + this.listaContatti.length);
      } 
      else {
        this.oldSearchText = "";
        this.resetList();
      }
    }, 300);
  }

  filterList(searchTerm:string){
    let listToUse = UtilsContatti.listaContatti;
    if (searchTerm.length > this.oldSearchText.length) {
      listToUse = this.listaContattiUsed;
    }
    this.oldSearchText = searchTerm;
    return listToUse.filter((contatto) => {
        return contatto.getNomeCognome().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  ionViewDidEnter() {
    if (ContattiPage.listChanges) {
      ContattiPage.listChanges = false;
      this.resetList();
    }
  }

  call(contatto: Contatto) {
    this.callNumber.callNumber(contatto.getTelefono(), true)
    .then(res => console.log('Vai a compositore', res))
    .catch(err => console.log('Error: ', err));
  }
}
