import { ContattiPage } from './../contatti/contatti';
import { DatabaseProvider } from './../../providers/database/database';
import { UtilsContatti } from './../../Utils/UtilsContatti';
import { Utils } from './../../Utils/Utils';
import { Contatto } from './../../class/contatto';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, Navbar, AlertController, ItemSliding, Toggle, Toast, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-contatto-dettagli',
  templateUrl: 'contatto-dettagli.html',
})
export class ContattoDettagliPage {
  @ViewChild(Navbar) navbar: Navbar;
  choose:string;

  contattoSelected:Contatto;
  nuovoContatto:boolean = false;
  listaContattiAssociati:Contatto[];
  contattoPrincipale:string;

  dati:any;
  datiOk:boolean = false;

  public unregisterBackButtonAction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, platform:Platform,
              private alertCtrl:AlertController, private databaseprovider: DatabaseProvider,
              private toastCtrl:ToastController) {
    this.contattoSelected = navParams.get('contatto');
    this.nuovoContatto = navParams.get('nuovo');
    Utils.writeLog("Costruttore -> nuovo contatto ? " + this.nuovoContatto);
    this.choose = 'dettagli';

    if (this.contattoSelected != null) {
      let cf:string;
      let sedelegale:string;
      if (this.contattoSelected.getType() == 'Persona') {
        cf = this.contattoSelected.getCF();
        sedelegale = '';
      } else {
        cf = '';
        sedelegale = this.contattoSelected.getSedeLegale();
      }
      this.dati = { 
        idcompany: this.contattoSelected.getIdCompany(),
        nomeCognome : this.contattoSelected.getNomeCognome(), 
        telefono : this.contattoSelected.getTelefono(),
        email : this.contattoSelected.getEmail(),
        piva : this.contattoSelected.getPIva(), 
        cf : cf,
        sedelegale : sedelegale,
        sedeoperativa : this.contattoSelected.getSedeOperativa(),
        maxore : this.contattoSelected.getMaxOre(),
        tariffaora : this.contattoSelected.getTariffaOre(),
        tariffagiornata : this.contattoSelected.getTariffaGiornata(),
        tariffakm : this.contattoSelected.getTariffaKm()
      };
      if (this.contattoSelected.getIdCompany() != 0) {
        this.contattoPrincipale = UtilsContatti.getContattoPrincipale(this.contattoSelected.getIdCompany())[0].getNomeCognome();
      }
    }

    platform.registerBackButtonAction(() => {
      Utils.writeLog("Back pressed");
      this.backButtonClick();
    });
  }

  ionViewDidLoad() {
    Utils.writeLog("Contatto Selezionato: " + this.contattoSelected.getID() + " " + this.contattoSelected.getNomeCognome() + " " + this.contattoSelected.getType());
    this.navbar.backButtonClick = () => {
      this.backButtonClick();
    }
  }

  ionViewDidEnter() {
    if (this.contattoSelected.getType() == 'Azienda') {
      this.listaContattiAssociati = [];
      this.listaContattiAssociati = UtilsContatti.getListaAssociati(this.contattoSelected.getID());
    }
  }

  ionViewDidLeave() {
    if (this.nuovoContatto) {
      Utils.writeLog("Leaving view " + this.nuovoContatto);
      this.nuovoContatto = false;
    }
  }

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  backButtonClick() {
    Utils.writeLog("Back button -> Modifiche ? " + UtilsContatti.checkChanges(this.dati, this.contattoSelected));
    if (UtilsContatti.checkChanges(this.dati, this.contattoSelected)[0]) {
      this.presentSave();
    } else {
      Utils.writeLog("Back button -> Nuovo contatto  ? "+this.nuovoContatto);
      if (this.nuovoContatto) {
        this.presentDelete();
      } else {
        this.navCtrl.pop();
      }
    }
  }

  save() {
    let checkDati = UtilsContatti.checkDati(this.dati);
    Utils.writeLog("Dati corretti ? "+checkDati[0]);
    if (checkDati[0]) {
      let checkChanges = UtilsContatti.checkChanges(this.dati, this.contattoSelected);
      Utils.writeLog("Update ? " + checkChanges[0]);
      checkChanges[1].forEach(cc => {
        Utils.writeLog("Campo mod: " + cc);
      });
      if (UtilsContatti.checkChanges(this.dati, this.contattoSelected)[0]) {
        this.contattoSelected.setNomeCognome(this.dati.nomeCognome);
        this.contattoSelected.setTelefono(this.dati.telefono);
        this.contattoSelected.setEmail(this.dati.email);
        this.contattoSelected.setPIva(this.dati.piva);
        this.contattoSelected.setCF(this.dati.cf);
        this.contattoSelected.setSedeLegale(this.dati.sedelegale);
        this.contattoSelected.setSedeOperativa(this.dati.sedeoperativa);
        this.contattoSelected.setMaxOre(parseInt(this.dati.maxore));
        this.contattoSelected.setTariffaOre(parseFloat(this.dati.tariffaora));
        this.contattoSelected.setTariffaGiornata(parseFloat(this.dati.tariffagiornata));
        this.contattoSelected.setTariffaKm(parseFloat(this.dati.tariffakm));

        Utils.writeLog(this.contattoSelected.writeInfo());

        if (this.nuovoContatto) {
          //UtilsContatti.addListaContatti(this.contattoSelected);
          UtilsContatti.listaContatti.unshift(this.contattoSelected);
        }
        this.databaseprovider.updateContatto(this.contattoSelected)
        .then((res) => { ContattiPage.listChanges = true; })
        .catch(err => { Utils.writeLog("E\\ + update contatto: " + err) });
      }
      this.navCtrl.pop();
    } else {
      Utils.presentToast("Dati non compilati correttamente, " + checkDati[1], this.toastCtrl);
      Utils.writeLog("Dati non compilati correttamente")
    }
  }

  presentSave() {
    let alert = this.alertCtrl.create({
      //title: 'Attenzione',
      title: 'Sono state apportate delle modifiche',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Salva Modifiche',
          role: 'destructive',
          handler: () => {
            this.save();
          }
        },
        {
          text: 'Continua modifiche',
          handler: () => {}
        },
        {
          text: 'Annulla Modifiche',
          handler: () => {
            if (this.nuovoContatto) {
              this.deleteFromDB(this.contattoSelected.getID());
            }
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  openContatto(contatto:Contatto, nuovo:boolean) {
    this.navCtrl.push(ContattoDettagliPage, {
      contatto: contatto,
      nuovo: nuovo
    })
  }

  nuovo() {
    this.nuovoContatto = true;
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.databaseprovider.addContatto(this.contattoSelected.getID())
        .then(contatto => {
          if (contatto != null) {
            this.openContatto(contatto, true);
          }
        })
      }
    });
  }

  changeType(item: Toggle) {
    if (item.checked) {
      this.contattoSelected.setType("Azienda");
    } else {
      this.contattoSelected.setType("Persona");
    }
  }

  presentDelete() {
    let alert = this.alertCtrl.create({
      //title: 'Attenzione',
      title: 'Sei sicuro di voler uscire ? Questo contatto non sarà salvato',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Rimani',
          role: 'destructive',
          handler: () => {}
        },
        {
          text: 'Esci',
          handler: () => {
            this.deleteFromDB(this.contattoSelected.getID());
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  delete(item:ItemSliding, contatto:Contatto) {
    //item.close()
    if (contatto.getIdCompany() > 0) {
      let indexGlobal = UtilsContatti.listaContatti.indexOf(contatto);
      let index = this.listaContattiAssociati.indexOf(contatto);

      if(indexGlobal > -1){
        let alert = this.alertCtrl.create({
          title: 'Attenzione',
          message: 'Sei sicuro di voler eliminare questo contatto?',
          buttons: [
            {
              text: 'No',
              role: 'cancel',
              handler: () => {}
            },
            {
              text: 'Elimina',
              handler: () => {
                UtilsContatti.removeListaContatti(indexGlobal);
                this.listaContattiAssociati.splice(index, 1);
                Utils.writeLog(contatto.getNomeCognome() + " Eliminato");
                this.deleteFromDB(contatto.getID());
              }
            }
          ]
        });
        alert.present();
      }
    } else {
      Utils.writeLog(contatto.getNomeCognome() + " è una compagnia");
    }
  }

  deleteFromDB(id:number) {
    this.databaseprovider.deleteContatto(id).then(() => {
      ContattiPage.listChanges = true;
    });
  }
}
