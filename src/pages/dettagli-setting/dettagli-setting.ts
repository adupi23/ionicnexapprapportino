import { DatabaseProvider } from './../../providers/database/database';
import { UtilsSetting } from './../../Utils/UtilsSetting';
import { CampiSetting } from './../../class/campiSetting';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Toggle } from 'ionic-angular';
import { Utils } from '../../Utils/Utils';

@Component({
  selector: 'page-dettagli-setting',
  templateUrl: 'dettagli-setting.html',
})
export class DettagliSettingPage {

  campoSelected:CampiSetting;
  nomeCampo:string;
  listOption:CampiSetting[];
  values:string[] = [];

  isModal:boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private databaseprovider: DatabaseProvider, private viewCtrl:ViewController) {
    this.campoSelected = navParams.get('setting');
    this.isModal = navParams.get('modal');
    this.nomeCampo = this.campoSelected.getName();

    Utils.writeLog("Modal: " + this.isModal);
    this.listOption = this.filterOptions();

    this.setValues();
  }

  changeName() {
    let index = UtilsSetting.campi.indexOf(this.campoSelected);
    let value = this.nomeCampo;
    this.campoSelected.setName(value);
    Utils.writeLog("name value: " + this.campoSelected.getName());
    UtilsSetting.campi[index].setName(value);
    this.databaseprovider.updateCampo(this.campoSelected).then(() => {
      Utils.writeLog("Update success");
    });
  }

  changeValue(ev:any, option:CampiSetting) {
    let index = UtilsSetting.campi.indexOf(option);
    let value = this.values[this.listOption.indexOf(option)];
    option.setName(value);
    Utils.writeLog("name value: " + option.getName());
    UtilsSetting.campi[index].setValue(value);
    this.databaseprovider.updateCampo(option).then(() => {
      Utils.writeLog("Update success");
      this.setValues();
    });
  }

  changeActive(item: Toggle, campo:CampiSetting) {
    let i = UtilsSetting.campi.indexOf(campo);
    campo.setAttivo(item.checked);
    UtilsSetting.campi[i].setAttivo(item.checked);
    Utils.writeLog("attivo: " + campo.getAttivo() + " id: " + campo.getID());
    this.databaseprovider.updateCampo(campo).then(() => {
      Utils.writeLog("Update success");
    });
  }

  addOption() {
    Utils.writeLog("Add Option func");
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.databaseprovider.addCampo(this.campoSelected.getID())
        .then(campo => {
          if (campo != null) {
            this.listOption = this.filterOptions();
            this.setValues();
          }
        });
      }
    });
  }

  removeOption(option:CampiSetting) {
    Utils.writeLog("Remove Option func");
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.databaseprovider.removeCampo(option)
        .then(() => {
            this.listOption = this.filterOptions();
            this.setValues();
        });
      }
    });
  }

  filterOptions() {
    return UtilsSetting.campi.filter(campo => {
      return campo.getIDCampo() == this.campoSelected.getID();
    });
  }

  setValues() {
    this.values = [];
    this.listOption.forEach(c => {
      this.values.push(c.getValue());
    });
  }

  choose(option:CampiSetting) {
    let value = this.values[this.listOption.indexOf(option)];
    this.viewCtrl.dismiss(value);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
