import { UtilsRapportini } from './../../Utils/UtilsRapportini';
import { UtilsContatti } from './../../Utils/UtilsContatti';
import { UtilsCommessa } from './../../Utils/UtilsCommessa';
import { RigaRapportino } from './../../class/rigaRapportino';
import { ViewChild, Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Nav, ActionSheetController } from 'ionic-angular';
import { Excel } from '../../class/excel';
import { Utils } from '../../Utils/Utils';
import { SettingPage } from '../setting/setting';
import { Headers, RequestOptions, Http } from '@angular/http';

@Component({
  selector: 'page-riepilogo',
  templateUrl: 'riepilogo.html',
})
export class RiepilogoPage {
  @ViewChild(Nav) nav: Nav;

  righeRapportino:RigaRapportino[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http,
              private alertCtrl:AlertController, public loadingCtrl: LoadingController,
              private actionSheetCtrl:ActionSheetController) {
    this.righeRapportino = navParams.get("righe");
  }

  getNCommessa(idCommessa:number) {
    let commessa =  UtilsCommessa.listaCommesse.filter(c => {
        return c.getID() == idCommessa;
    })[0];
    return commessa.getNumero().toString();
  }

  getCommessa(idCommessa) {
    return UtilsCommessa.listaCommesse.filter(c => {
      return c.getID() == idCommessa;
    })[0];
  }

  getClienteCommessa(idCommessa:number) {
      let commessa =  UtilsCommessa.listaCommesse.filter(c => {
          return c.getID() == idCommessa;
      })[0];

      let cliente = UtilsContatti.listaContatti.filter(c => {
          return c.getID() == commessa.getIdCliente();
      })[0];
      return cliente;
  }

  getDataCommessa(riga:RigaRapportino) {
    return UtilsRapportini.getDateIt(riga.data);
  }

  createExcel() {
    if (Utils.emailToSend.trim() != "") {
      let alert = this.alertCtrl.create({
        title: 'Vuoi creare un riepilogo di questo Rapportino ?',
        buttons: [
          {
            text: 'Si',
            role: 'destructive',
            handler: () => {
              this.excel();
            }
          },
          {
            text: 'Annulla',
            role: 'cancel',
            handler: () => {}
          }
        ]
      });
   
      alert.present();
    } else {
      let alert = this.alertCtrl.create({
        title: 'Non hai indicato la mail a cui spedire i riepiloghi, vai nelle impostazioni',
        buttons: [
          {
            text: 'VAI',
            handler: () => {
              // this.nav.setRoot(SettingPage, {}, {animate: true, direction: 'forward'});
              this.navCtrl.setRoot(SettingPage, {}, {animate: true, direction: 'forward'});
            }
          }
        ]
      });
      alert.present();
    }
  }

  excel(righe = this.righeRapportino) {
    let loading = this.presentLoad();
      
    let excel = new Excel(righe);
    loading.present();
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json; charset=UTF-8' );
    const requestOptions = new RequestOptions({ headers: headers });

    let data = {
      rapportino: excel.rapportino,
      riepilogo: excel.riepilogo,
      fattura: excel.fattura,
      nome: UtilsRapportini.getRapportino(this.righeRapportino[0].idRapportino).nome,
      email: Utils.emailToSend
    }
    let postData = JSON.stringify(data);
    Utils.writeLog("POST DATA: " + postData);

    // let api_ip = "http://" + Utils.ipws;
    Utils.writeLog("email: " + data.email);
    this.http.post("https://nexrapportino.nexapp.it/apiTest/excel/create.php", postData, requestOptions)
    .subscribe(data => {
          Utils.writeLog("Excel creato");
          loading.dismiss(true);
          Utils.writeLog("Data WS: " + JSON.stringify(data));
        }, error => {
          Utils.writeLog("ERRORE WS: " + JSON.stringify(error));
          loading.dismiss(false);
    });

    // Utils.writeLog("Excel Header = " + JSON.stringify(excel.nomeColonna));
    // Utils.writeLog("Excel rows = " + JSON.stringify(excel.valueCells));
  }

  presentLoad() {
    let loading = this.loadingCtrl.create({
      content: 'Attendi...'
    });

    loading.onDidDismiss((data) => {
      Utils.writeLog("Data dismiss: " + data);
      if (data) {
        this.presentConfirmAlert(data);
      } else {
        this.presentConfirmAlert(data);
      }
    });
    return loading;
  }

  presentConfirmAlert(data:boolean) {
    let text = "C'è stato un errore, riprova";
    if (data) {
      text = "Il riepilogo è stato creato correttamente, riceverai una mail";
    }
    let alert = this.alertCtrl.create({
      //title: 'Attenzione',
      title: text,
      buttons: ['OK']
    });

    alert.present();
  }

}