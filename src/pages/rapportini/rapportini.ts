import { File } from '@ionic-native/file';
import { Documento } from './../../class/documento';
import { UtilsRapportini } from './../../Utils/UtilsRapportini';
import { DatabaseProvider } from './../../providers/database/database';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, ItemSliding, AlertController, ToastController } from 'ionic-angular';
import { Utils } from '../../Utils/Utils';
import { Rapportino } from '../../class/rapportino';
import { DettagliRapportinoPage } from '../dettagli-rapportino/dettagli-rapportino';

@Component({
  selector: 'page-rapportini',
  templateUrl: 'rapportini.html',
})

export class RapportiniPage {
  @ViewChild(Content) content: Content;

  listaRapportini = [];
  listaRapportiniUsed = [];
  startIndex = 0;
  lastIndex = 0;
  enabled = true;

  showSearch:boolean = false;
  searchText:string = "";
  oldSearchText:string = "";
  searching:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              private databaseprovider: DatabaseProvider, private alertCtrl: AlertController,
              private toastCtrl: ToastController, private file: File) {
    this.listaRapportiniUsed = UtilsRapportini.listaRapportini;
    this.loadRapportini();
    Utils.writeLog("Totale rapportini: " + UtilsRapportini.listaRapportini.length);
    Utils.writeLog("Rapportini caricati: " + this.listaRapportini.length);
  }

  nuovo() {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.databaseprovider.addRapportino()
        .then(rapportino => {
          if (rapportino != null) {
            this.openRapportino(rapportino, true);
          }
        })
      }
    });
  }

  loadRapportini() {
    if (this.listaRapportiniUsed.length > 0) {
      this.lastIndex = this.listaRapportiniUsed.length
      if (this.lastIndex > (20 + this.startIndex)) {
        this.lastIndex = 20 + this.startIndex;
      }
      for (var i = this.startIndex; i < this.lastIndex; i++) {
        this.listaRapportini.push(this.listaRapportiniUsed[i]);
      }
    }
    this.startIndex = this.lastIndex;
    if (this.listaRapportini.length == this.listaRapportiniUsed.length) {
      this.enabled = false;
    }
    this.searching = false;
  }

  doInfinite(): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (this.listaRapportini.length < this.listaRapportiniUsed.length) {
          this.loadRapportini();
        }
        Utils.writeLog("Rapportini caricati visibili: " + this.listaRapportini.length);
        // this.toTopButtonVisible = true;
        this.searching = false;
        resolve(this.listaRapportini);
      }, 700);
    })
  }

  scrollTop() {
    this.content.scrollToTop();
  }

  openRapportino(rapportino:Rapportino, nuovo:boolean) { 
    let page = DettagliRapportinoPage;
    // if (nuovo) {
    //   page = CreaRapportinoPage
    // }
    this.navCtrl.push(page, {
      rapportino: rapportino,
      nuovo: nuovo
    });
  }

  openSearch() {
    if (!this.showSearch) {
      this.searchText = "";
      this.showSearch = true;
    } else {
      this.showSearch = false;
    }
  }

  delete(item:ItemSliding, rapportino:Rapportino) {
    //item.close()
    let indexGlobal = UtilsRapportini.listaRapportini.indexOf(rapportino);
    if(indexGlobal > -1){
      this.doDelete(indexGlobal, rapportino);
    }
  }

  doDelete(index:number, rapportino:Rapportino) {
    let alert = this.alertCtrl.create({
      title: 'Attenzione',
      message: 'Sei sicuro di voler eliminare questo rapportino?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Elimina',
          handler: () => {
            this.deleteRecursive(rapportino);
            UtilsRapportini.removeListaRapportini(index);

            this.searching = true;
            this.enabled = true;
            
            this.startIndex = 0;
            this.lastIndex = 0;
            this.listaRapportini = [];
            this.listaRapportiniUsed = UtilsRapportini.listaRapportini;
            if (this.searchText != "") {
              this.listaRapportiniUsed = this.filterList(this.searchText);
            }
            this.loadRapportini();
            Utils.writeLog(rapportino.nome + " Eliminato");
            this.databaseprovider.deleteRapportino(rapportino.id);
          }
        }
      ]
    });
    alert.present();
  }

  deleteRecursive(rapportino:Rapportino) {
    this.databaseprovider.getRigaRapportino(rapportino.id).then(result => {
      let listaRighe = result;
      listaRighe.forEach(riga => {
        this.databaseprovider.getDocs(riga.id).then(result => {
          let listaDocs:Documento[] = result;
          //elimina docs riga
          listaDocs.forEach(doc => {
            var nameImg = doc.path.substr(doc.path.lastIndexOf('/') + 1);
            var pathImg = doc.path.substr(0, doc.path.lastIndexOf('/') + 1);
            this.file.removeFile(pathImg, nameImg).then(success => {
              if (success) {
                this.databaseprovider.deleteDoc(doc.id).then(() => {
                  Utils.writeLog("Delete doc " + doc.id);
                });
              }
            }, err => {
              Utils.writeLog("Error del: " + JSON.stringify(err));
            });
          });
          //elimina riga
          this.databaseprovider.deleteRigaRapportino(riga.id).then(() => {
            Utils.writeLog("Riga " + riga.id + " eliminata");
          });
        });
      });
    });
  }

  //search
  resetList() {
    Utils.writeLog("reset list");
    this.startIndex = 0;
    this.lastIndex = 0;
    this.enabled = true;
    this.listaRapportini = [];
    this.listaRapportiniUsed = UtilsRapportini.listaRapportini;
    this.loadRapportini();
  }

  onClearSearchbar(ev: any) {
    Utils.writeLog("Clear Searchbar");
    this.searching = true;
    this.resetList();
    this.showSearch = false;
  }

  onCancelSearchbar(ev: any) {
    Utils.writeLog("Cancel Searchbar");
    this.searching = true;
    this.resetList();
    this.showSearch = false;
  }

  changedInput(ev: any) {
    Utils.writeLog("text changed");
    this.searching = true;
    this.enabled = true;

    setTimeout(() => {
      this.searching = false;
      if (this.searchText != "") {
        let tmpList = this.filterList(this.searchText);
        Utils.writeLog("Contatti filtrati: " + tmpList.length);
        this.startIndex = 0;
        this.lastIndex = 0;
        this.listaRapportini = [];
        this.listaRapportiniUsed = tmpList;
        this.loadRapportini();
        Utils.writeLog("Contatti visibili dopo filtro: " + this.listaRapportini.length);
      } 
      else {
        this.oldSearchText = "";
        this.resetList();
      }
    }, 300);
  }

  filterList(searchTerm:string){
    let listToUse = UtilsRapportini.listaRapportini;
    if (searchTerm.length > this.oldSearchText.length) {
      listToUse = this.listaRapportiniUsed;
    }
    this.oldSearchText = searchTerm;
    return listToUse.filter((rapportino) => {
        return rapportino.nome.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  ionViewDidEnter() {
    this.resetList();
  }

}
