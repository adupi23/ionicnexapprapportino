import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Utente } from './../../class/utente';
import { Utils } from './../../Utils/Utils';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm:FormGroup;
  Menu:MenuController;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              menu:MenuController, private formBuilder:FormBuilder, private databaseprovider: DatabaseProvider) {
    this.Menu = menu;

    this.loginForm = this.formBuilder.group({
      username: ['Nexapp', Validators.required],
      password: ['Nexpwd18!', Validators.required]
    });
    // this.username = "Nexapp";
    // this.password = "Nexpwd18!";
  }

  // db
  //
  ionViewDidLoad() {
    
  }

  ionViewDidEnter(){
    this.Menu.enable(false);
    //this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
  }

  login() {
    if (this.loginForm.valid) {
      this.databaseprovider.getUser(this.loginForm.controls['username'].value, this.loginForm.controls['password'].value)
      .then(users => {
      Utils.writeLog("User data: " + users[0].getId() +  " " + users[0].getUsername());
        if (users.length == 1) {
          Utils.setUserLogged(users[0]);
          this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
        //   //this.navCtrl.setRoot(ContattiPage, {}, {animate: true, direction: 'forward'});
        }
      })
      .catch(e => Utils.writeLog(e));
    }
  }

}
