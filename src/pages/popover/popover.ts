import { DatabaseProvider } from './../../providers/database/database';
import { UtilsRapportini } from './../../Utils/UtilsRapportini';
import { RigaRapportino } from './../../class/rigaRapportino';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Toast } from 'ionic-angular';
import { Utils } from '../../Utils/Utils';
import { DettagliRapportinoPage } from '../dettagli-rapportino/dettagli-rapportino';

@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {
  dati:any;
  rigaSelected:RigaRapportino;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              private databaseprovider: DatabaseProvider) {
    this.rigaSelected = this.navParams.get('riga');
    this.loadDati();
  }

  deleteRiga() {
    this.viewCtrl.dismiss(true);
  }

  loadDati() {
    this.dati = {
      data: UtilsRapportini.getDateTimeISOString(new Date(this.rigaSelected.data)),
      totOreViaggio: this.rigaSelected.totOreViaggio.toString(),
      totOreLavoro: this.rigaSelected.totOreLavoro.toString(),
      totKM: this.rigaSelected.totKM.toString(),
      pranzo_art15: this.rigaSelected.pranzo_art15.toString(),
      cena_art15: this.rigaSelected.cena_art15.toString(),
      hotel_art15: this.rigaSelected.hotel_art15.toString(),
      altro_art15: this.rigaSelected.altro_art15.toString(),
      pranzo: this.rigaSelected.pranzo.toString(),
      cena: this.rigaSelected.cena.toString(),
      hotel: this.rigaSelected.hotel.toString(),
      altro: this.rigaSelected.altro.toString()
    }
    // Utils.writeLog("DATE: " + this.dati.data);
  }

  changeValue(nomeCampo:string, rigaRapportino = this.rigaSelected) {
    let index = this.navParams.get('index');
    let value = "";

    switch(nomeCampo) {
      case "tot_ore_viaggio":
        value = this.dati.totOreViaggio;
        this.rigaSelected.totOreViaggio = parseInt(value);
        break;
      case "tot_ore_lavoro":
        value = this.dati.totOreLavoro;
        this.rigaSelected.totOreLavoro = parseInt(value);
        break;
      case "tot_km":
        value = this.dati.totKM;
        this.rigaSelected.totKM = parseInt(value);
        break;
      case "pranzo_art15":
        value = this.dati.pranzo_art15;
        this.rigaSelected.pranzo_art15 = parseInt(value);
        break;
      case "cena_art15":
        value = this.dati.cena_art15;
        this.rigaSelected.cena_art15 = parseInt(value);
        break;
      case "hotel_art15":
        value = this.dati.hotel_art15;
        this.rigaSelected.hotel_art15 = parseInt(value);
        break;
      case "altro_art15":
        value = this.dati.altro_art15;
        this.rigaSelected.altro_art15 = parseInt(value);
        break;
      case "pranzo":
        value = this.dati.pranzo;
        this.rigaSelected.pranzo = parseInt(value);
        break;
      case "cena":
        value = this.dati.cena;
        this.rigaSelected.cena = parseInt(value);
        break;
      case "hotel":
        value = this.dati.hotel;
        this.rigaSelected.hotel = parseInt(value);
        break;
      case "altro":
        value = this.dati.altro;
        this.rigaSelected.altro = parseInt(value);
        break;
      case "data":
        let data = this.dati.data;
        value = UtilsRapportini.getDateString(data);
        this.rigaSelected.data = new Date(value).toDateString();
        rigaRapportino.data = new Date(value).toDateString();
        // Utils.writeLog("Case data: " + value);
        break;
    }
    Utils.writeLog("Value " + nomeCampo + ": " + value + " index: " + index);
    this.databaseprovider.updateRigaRapportino(nomeCampo, value, rigaRapportino.id).then(() => {
      Utils.writeLog("Update success");
    });
  }

}
