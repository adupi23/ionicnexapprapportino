import { Utils } from './../../Utils/Utils';
import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,  menu:MenuController,
              private databaseprovider:DatabaseProvider) {
    menu.enable(true);
    menu.swipeEnable(false);
  }

  getUsers() {
    this.databaseprovider.getUser('', '')
    .then(users => {
      if (users.length > 0) {
        for (var i = 0; i < users.length; i++) {
          Utils.writeLog("User data: " + users[0].getId() +  " " + users[0].getUsername());
        }
      } else {
        Utils.writeLog("No Data");
      }
      
    })
    .catch(e => Utils.writeLog(e));
  }


}
