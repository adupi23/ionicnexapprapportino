import { Camera } from '@ionic-native/camera';
import { Utils } from './../../Utils/Utils';
import { Documento } from './../../class/documento';
import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { RigaRapportino } from '../../class/rigaRapportino';
import { File } from '@ionic-native/file';

declare var cordova: any;

@Component({
  selector: 'page-documenti',
  templateUrl: 'documenti.html',
})
export class DocumentiPage {
  idRiga:RigaRapportino;
  listaDocs:Documento[] = [];
  lastImage:string = null;
  listaTitoliDoc:string[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private databaseprovider:DatabaseProvider,
              private camera:Camera, public platform: Platform, private file: File, 
              private alertCtrl:AlertController) {
    this.idRiga = navParams.get('riga');
    this.databaseprovider.getDocs(this.idRiga.id).then(data => {
      this.listaDocs = data;
      Utils.writeLog("Count Docs: " + this.listaDocs.length);
      this.loadTitoli();
    });
  }

  takePhoto() {
    var options = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
   
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      // if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
      //   this.filePath.resolveNativePath(imagePath)
      //     .then(filePath => {
      //       let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
      //       let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
      //       this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      //     });
      // } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      // }
    }, (err) => {
      Utils.writeLog('Errore img: ' + JSON.stringify(err));
    });
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.databaseprovider.addDoc(this.idRiga.id, this.lastImage, cordova.file.dataDirectory).then(result => {
        this.listaDocs.push(result);
        this.loadTitoli();
      })
    }, err => {
      Utils.writeLog('Errore salvataggio: ' + JSON.stringify(err));
    });
  }

  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }

  changeTitolo(doc:Documento) {
    let index = this.listaDocs.indexOf(doc);
    let value = this.listaTitoliDoc[index];
    this.listaDocs[index].titolo = value;
    Utils.writeLog("name value: " + this.listaDocs[index].titolo + " " + this.listaDocs[index].id);
    this.databaseprovider.updateDoc(doc).then(() => {
      Utils.writeLog("Update success");
    });
  }

  presentDelete(doc:Documento) {
    let alert = this.alertCtrl.create({
      title: 'Sei sicuro di voler eliminare questo documento ?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Si',
          role: 'destructive',
          handler: () => {
            this.delete(doc);
          }
        },
        {
          text: 'no',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

  delete(doc:Documento) {
    var nameImg = doc.path.substr(doc.path.lastIndexOf('/') + 1);
    var pathImg = doc.path.substr(0, doc.path.lastIndexOf('/') + 1);
    // Utils.writeLog("Name: " + nameImg + ", path: " + pathImg + ", tot: " + doc.path);
    this.file.removeFile(pathImg, nameImg).then(success => {
      if (success) {
        this.databaseprovider.deleteDoc(doc.id).then(() => {
          Utils.writeLog("Delete doc " + doc.id);
          let index = this.listaDocs.indexOf(doc);
          this.listaDocs.splice(index, 1);
          this.loadTitoli();
        });
      }
    }, err => {
      Utils.writeLog("Error del: " + JSON.stringify(err));
    });
  }

  loadTitoli() {
    this.listaTitoliDoc = [];
    this.listaDocs.forEach(doc => {
      this.listaTitoliDoc.push(doc.titolo);
    });
  }

}
