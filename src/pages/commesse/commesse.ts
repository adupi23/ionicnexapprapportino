import { Utils } from './../../Utils/Utils';
import { DatabaseProvider } from './../../providers/database/database';
import { DettagliComessaPage } from './../dettagli-comessa/dettagli-comessa';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, ItemSliding, AlertController, ToastController } from 'ionic-angular';
import { Commessa } from '../../class/commessa';
import { UtilsCommessa } from '../../Utils/UtilsCommessa';

@Component({
  selector: 'page-commesse',
  templateUrl: 'commesse.html',
})
export class CommessePage {
  @ViewChild(Content) content: Content;

  static listChanges:boolean = false;
  listaCommesseUsed = [];
  listaCommesse = [];
  startIndex = 0;
  lastIndex = 0;
  enabled = true;

  //testo per la ricerca
  showSearch:boolean = false;
  searchText:string = "";
  oldSearchText:string = "";
  searching:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private databaseprovider: DatabaseProvider, private alertCtrl: AlertController,
              private toastCtrl: ToastController) {
    this.listaCommesseUsed = UtilsCommessa.listaCommesse;
    this.loadCommesse();
  }

  nuova() {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.databaseprovider.addCommessa()
        .then(commessa => {
          if (commessa != null) {
            this.openCommessa(commessa, true);
          }
        });
      }
    });
  }

  openCommessa(commessa:Commessa, nuova:boolean) {
    this.navCtrl.push(DettagliComessaPage, {
      commessa: commessa,
      nuovo: nuova
    });
  }

  doInfinite(): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (this.listaCommesse.length < this.listaCommesseUsed.length) {
          this.loadCommesse();
        }
        Utils.writeLog("Commesse caricate visibili: " + this.listaCommesse.length);
        // this.toTopButtonVisible = true;
        this.searching = false;
        resolve(this.listaCommesse);
      }, 700);
    })
  }

  loadCommesse() {
    if (this.listaCommesseUsed.length > 0) {
      this.lastIndex = this.listaCommesseUsed.length
      if (this.lastIndex > (20 + this.startIndex)) {
        this.lastIndex = 20 + this.startIndex;
      }
      for (var i = this.startIndex; i < this.lastIndex; i++) {
        this.listaCommesse.push(this.listaCommesseUsed[i]);
      }
    }
    this.startIndex = this.lastIndex;
    if (this.listaCommesse.length == this.listaCommesseUsed.length) {
      this.enabled = false;
    }
    this.searching = false;
  }

  scrollTop() {
    this.content.scrollToTop();
  }

  openSearch() {
    if (!this.showSearch) {
      this.searchText = "";
      this.showSearch = true;
    } else {
      this.showSearch = false;
    }
  }

  delete(item:ItemSliding, commessa:Commessa) {
    //item.close()
    let indexGlobal = UtilsCommessa.listaCommesse.indexOf(commessa);

    this.doDelete(indexGlobal, commessa);
  }

  doDelete(index:number, commessa:Commessa) {
    let alert = this.alertCtrl.create({
      title: 'Attenzione',
      message: 'Sei sicuro di voler eliminare questa commessa?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Elimina',
          handler: () => {
            UtilsCommessa.removeListaCommesse(index);

            this.searching = true;
            this.enabled = true;
            
            this.startIndex = 0;
            this.lastIndex = 0;
            this.listaCommesse = [];
            this.listaCommesseUsed = UtilsCommessa.listaCommesse;
            if (this.searchText != "") {
              this.listaCommesseUsed = this.filterList(this.searchText);
            }
            this.loadCommesse();
            Utils.writeLog(commessa.getNumero() + " Eliminato");
            this.databaseprovider.deleteCommessa(commessa.getID());
          }
        }
      ]
    });
    alert.present();
  }

  filterList(searchTerm:string){
    let listToUse = UtilsCommessa.listaCommesse
    if (searchTerm.length > this.oldSearchText.length) {
      listToUse = this.listaCommesseUsed;
    }
    this.oldSearchText = searchTerm;
    return listToUse.filter((commessa) => {
        return commessa.getNumero().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  //search
  resetList() {
    Utils.writeLog("reset list");
    this.startIndex = 0;
    this.lastIndex = 0;
    this.enabled = true;
    this.listaCommesse = [];
    this.listaCommesseUsed = UtilsCommessa.listaCommesse;
    this.loadCommesse();
  }

  onClearSearchbar(ev: any) {
    Utils.writeLog("Clear Searchbar");
    this.searching = true;
    this.resetList();
    this.showSearch = false;
  }

  onCancelSearchbar(ev: any) {
    Utils.writeLog("Cancel Searchbar");
    this.searching = true;
    this.resetList();
    this.showSearch = false;
  }

  changedInput(ev: any) {
    Utils.writeLog("text changed");
    this.searching = true;
    this.enabled = true;

    setTimeout(() => {
      this.searching = false;
      if (this.searchText != "") {
        let tmpList = this.filterList(this.searchText);
        Utils.writeLog("Commesse filtrate: " + tmpList.length);
        this.startIndex = 0;
        this.lastIndex = 0;
        this.listaCommesse = [];
        this.listaCommesseUsed = UtilsCommessa.listaCommesse;
        this.loadCommesse();
        Utils.writeLog("Commesse visibili dopo filtro: " + this.listaCommesse.length);
      } 
      else {
        this.oldSearchText = "";
        this.resetList();
      }
    }, 300);
  }

  ionViewDidEnter() {
    if (CommessePage.listChanges) {
      CommessePage.listChanges = false;
      this.resetList();
    }
  }

  presentToast(message:string, position:string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: position
    });
  
    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
