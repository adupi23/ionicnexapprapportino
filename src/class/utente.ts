export class Utente {
    private id:number;
    private username:String;

    constructor(_id:number, _username:String) {
        this.id = _id;
        this.username = _username;
    }

    public getId() {
        return this.id;
    }

    public getUsername() {
        return this.username;
    }

    public setUsername(_username:String) {
        this.username = _username;
    }
}