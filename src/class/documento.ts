export class Documento {
    public id:number;
    public idRiga:number;
    public titolo:string;
    public path:string;
    
    constructor(_id:number, _idRiga:number, _titolo:string, _path:string) {
        this.id = _id;
        this.idRiga = _idRiga;
        this.titolo = _titolo;
        this.path = _path;
    }
}