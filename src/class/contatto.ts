export class Contatto {
    public id:number;
    private idCompany:number;
    private type:string;
    public nomeCognome:string;
    private telefono:string;
    private email:string;
    private pIva:string;
    private cf:string;
    private sedeLegale:string;
    private sedeOperativa:string;
    private maxOre:number;
    private tariffaOre:number;
    private tariffaGiornata:number;
    private tariffaKm:number;

    constructor(_id:number, _idCompany:number, _type:string, _nomeCognome:string, _telefono:string, _email:string, 
                _p_iva:string, _cf:string, _sede_legale:string, _sede_operativa:string, _maxOre:number, _tariffaOre:number,
                _tariffa_giornata:number, _tariffaKm:number) {
        this.id = _id;
        this.idCompany = _idCompany;
        this.type = _type;
        this.nomeCognome = _nomeCognome;
        this.telefono = _telefono;
        this.email = _email;
        this.pIva = _p_iva;
        this.cf = _cf;
        this.sedeLegale = _sede_legale;
        this.sedeOperativa = _sede_operativa;
        this.maxOre = _maxOre;
        this.tariffaOre = _tariffaOre;
        this.tariffaGiornata = _tariffa_giornata;
        this.tariffaKm = _tariffaKm
    }

    public getID() { return this.id; }
    public getIdCompany() { return this.idCompany; }

    public getType() { return this.type; }
    public setType(_type:string) { this.type = _type; }

    public getNomeCognome() { return this.nomeCognome; }
    public setNomeCognome(_nomeCognome:string) { this.nomeCognome = _nomeCognome; }

    public getTelefono() { return this.telefono; }
    public setTelefono(_telefono:string) { this.telefono = _telefono; }

    public getEmail() { return this.email; }
    public setEmail(_email:string) { this.email = _email; }

    public getPIva() { return this.pIva; }
    public setPIva(_p_iva:string) { this.pIva = _p_iva; }

    public getCF() { return this.cf; }
    public setCF(_cf:string) { this.cf = _cf; }

    public getSedeLegale() { return this.sedeLegale; }
    public setSedeLegale(_sede_legale:string) { this.sedeLegale = _sede_legale; }

    public getSedeOperativa() { return this.sedeOperativa; }
    public setSedeOperativa(_sede_operativa:string) { this.sedeOperativa = _sede_operativa; }

    public getMaxOre() { return this.maxOre; }
    public setMaxOre(_maxOre:number) { this.maxOre = _maxOre; }

    public getTariffaOre() { return this.tariffaOre; }
    public setTariffaOre(_tariffaOre:number) { this.tariffaOre = _tariffaOre; }

    public getTariffaGiornata() { return this.tariffaGiornata; }
    public setTariffaGiornata(_tariffa_giornata:number) { this.tariffaGiornata = _tariffa_giornata; }

    public getTariffaKm() { return this.tariffaKm; }
    public setTariffaKm(_tariffaKm:number) { this.tariffaKm = _tariffaKm; }

    public writeInfo() {
        return "ID: " + this.id + ", IDCompany: " + this.idCompany + ", Tipo: " + this.type + ", Nome e Cognome: "
            + this.nomeCognome + ", Telefono: " + this.telefono + ", Email: " + this.email + ", PIVA: " + this.pIva
            + ", CF: " + this.cf + ", Sede Legale: " + this.sedeLegale + ", Sede Operativa: " + this.sedeOperativa
            + ", Max Ore: " + this.maxOre + ", Tariffa Ore: " + this.tariffaOre + ", Tariffa KM: " + this.tariffaKm;
    }
}