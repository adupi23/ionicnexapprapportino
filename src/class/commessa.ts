export class Commessa {
    private id:number;
    private numero:string;
    private idCliente:number;
    private idLavorato:number;
    private libero:string[];
    private count:number;

    constructor(_id:number, _numero:string, _idCliente:number, _idLavorato:number, _libero:string[]) {
        this.id = _id;
        this.numero = _numero;
        this.idCliente = _idCliente;
        this.idLavorato = _idLavorato;
        this.libero = _libero;
        this.count = 0;
    }

    getID() { return this.id; }
    
    getNumero() { return this.numero; }
    setNumero(_numero:string) { this.numero = _numero; }

    getIdCliente() { return this.idCliente; }
    setIdCliente(_idCliente:number) { this.idCliente = _idCliente; }

    getIdLavorato() { return this.idLavorato; }
    setIdLavorato(_idLavorato:number) { this.idLavorato = _idLavorato; }

    getLibero() { return this.libero; }
    getOneLibero(index:number) { return this.libero[index]; }
    setLibero(_libero:string[]) { this.libero = _libero; }
    setOneLibero(index:number, _valueLibero:string) { this.libero[index] = _valueLibero; }

    getCount() { return this.count; }
    addCount() { this.count++; }
    removeCount() { this.count--; }
    restoreCount() { this.count = 0; }
}