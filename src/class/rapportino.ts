export class Rapportino {
    public id:number;
    public nome:string;

    constructor(_id:number, _nome:string) {
        this.id = _id
        this.nome = _nome;
    }
}