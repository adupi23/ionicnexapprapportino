export class Riepilogo {
    public idCommessa:number;
    public idCliente:number;
    public idRigaRapportino:number;
    public data:string;
    public totOreViaggioLavoro:number;
    public totOreCorrette:number;
    public rimborsi:number;
    public rimborsi_art15;

    constructor(_idCommessa:number, _idCliente:number, _idRigaRapportino:number, _data:string, _totOre:number, 
                _totOreC:number, _rimborsi:number, _rimborsi_art15:number) {
        this.idCommessa = _idCommessa;
        this.idCliente = _idCliente;
        this.idRigaRapportino = _idRigaRapportino;
        this.data = _data;
        this.totOreViaggioLavoro = _totOre;
        this.totOreCorrette = _totOreC;
        this.rimborsi = _rimborsi;
        this.rimborsi_art15 = _rimborsi_art15;
    }
}