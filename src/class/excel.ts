import { UtilsRapportini } from './../Utils/UtilsRapportini';
import { RigaRapportino } from './rigaRapportino';
import { UtilsSetting } from "../Utils/UtilsSetting";
import { Utils } from "../Utils/Utils";
import { Riepilogo } from './riepilogo';

export class Excel {
    // public nomeColonna:any[] = [];
    public rapportino;
    // public valueCells = [];
    public riepilogo;
    public fattura;
    public rr:RigaRapportino[];
    valueRiepilogo:Riepilogo[] = [];

    countLibero = 0;

    constructor(_righe:RigaRapportino[]) {
        this.rr = _righe;
        this.excelRapportino();
        this.excelRiepilogo();
        this.excelFattura();
        // let rrExcel = this.groupByCliente(_righe);
    }

    excelRapportino() {
        let temp = ["COMMESSA", "CLIENTE", "DATA"];
        let tempLibero = [];
        UtilsSetting.campi.forEach(c => {
        if (c.getIDCampo() == 0 && c.getAttivo().toString().toLowerCase() == "true") {
            this.countLibero++;
            tempLibero.push(c.getName().toUpperCase());
        }
        });
        let temp2 = ["TOT ORE VIAGGIO", "TOT ORE LAVORO", "TOT ORE VIAGGIO-LAVORO", "TOT RIMBORSO", "TOT KM", "TOT RIMBORSO KM", 
                    "PRANZO (A15)", "CENA (A15)", "TOT VITTO (A15)", "HOTEL (A15)", "ALTRO (A15)",
                    "PRANZO", "CENA", "TOT VITTO", "HOTEL", "ALTRO"];

        let nomeColonna = temp.concat(tempLibero, temp2);

        let valueCells = [];
        for(var i = 0; i < this.rr.length; i++) {
            let riga = this.rr[i];
            let commessa = UtilsRapportini.getCommessa(riga.idCommessa);
            let cliente = UtilsRapportini.getCliente(riga.idCommessa)
            
            let tmpR = [commessa.getNumero(), cliente.getNomeCognome(), UtilsRapportini.getDateIt(riga.data)];
            // let tmpRLibero = commessa.getLibero();
            let tmpRLibero = [];
            for (var k = 0; k < this.countLibero; k++) {
                tmpRLibero.push(commessa.getOneLibero(k));
            }
            Utils.writeLog("CONTA LIBERI: " + tmpRLibero.length);
            
            let tmpR2:string[] = [riga.totOreViaggio.toString(), riga.totOreLavoro.toString(), riga.getOreViaggioLavoro().toString(), 
                                riga.getRimborsoOre(cliente).toString(), riga.totKM.toString(), 
                                riga.getTotRimborsoKm(cliente.getTariffaKm()).toString(), riga.pranzo_art15.toString(), 
                                riga.cena_art15.toString(), riga.getVittoArt15().toString(), riga.hotel_art15.toString(), 
                                riga.altro_art15.toString(), riga.pranzo.toString(), riga.cena.toString(), riga.getVitto().toString(),
                                riga.hotel.toString(), riga.altro.toString()];
            valueCells.push(tmpR.concat(tmpRLibero, tmpR2));
        }
        // Utils.writeLog("Lista Header = " + this.nomeColonna.length);
        this.rapportino = { header: nomeColonna, body:valueCells };
    }

    excelRiepilogo() {
        let nomeColonna = ["COMMESSA", "CLIENTE", "DATA", "TOT ORE VIAGGIO-LAVORO", "TOT ORE CORRETTE", "RIMBORSI", 
                    "RIMBORSI (Art 15)"];

        for(var i = 0; i < this.rr.length; i++) {
            let riga = this.rr[i];
            let commessa = UtilsRapportini.getCommessa(riga.idCommessa);
            let cliente = UtilsRapportini.getCliente(riga.idCommessa)
            
            let oreCorrette = riga.getOreViaggioLavoro();
            if (riga.getOreViaggioLavoro() > cliente.getMaxOre()) {
                oreCorrette = cliente.getMaxOre();
            }
            let rimborsi = riga.getTotRimborsoKm(cliente.getTariffaKm()) + riga.getRimborsi();
            let rimborsi_art15 = riga.getRimborsiArt15();

            this.valueRiepilogo.push(new Riepilogo(commessa.getID(), cliente.getID(), riga.id, UtilsRapportini.getDateIt(riga.data),
                        riga.getOreViaggioLavoro(), oreCorrette, rimborsi, rimborsi_art15));
        }
        this.riepilogo = { header: nomeColonna, body:this.getArray(this.valueRiepilogo) };
    }

    excelFattura() {
        let nomeColonna = ["COMMESSA", "CLIENTE", "SERVIZI SOGG.IVA", "RIMBORSI", "IVA", "RIMBORSI NON IMP.IVA RT. 15"];

        let righe = this.orderByCommessa(Array.from(this.valueRiepilogo));
        let lastCommessa = -1;
        let valueFattura = [];
        for(var i = 0; i < righe.length; i++) {
            let riga = righe[i];
            let commessa = UtilsRapportini.getCommessa(riga.idCommessa);
            let cliente = UtilsRapportini.getCliente(riga.idCommessa);
            let rigaRapportino = this.getRiga(riga.idRigaRapportino);
            // console.log("RIGA: " + riga.idCommessa + ", value riep: " + this.valueRiepilogo[0].idCommessa + ", last: " + lastCommessa);
            if (riga === this.valueRiepilogo[0] || riga.idCommessa != lastCommessa) {
                valueFattura.push([ commessa.getNumero(), cliente.getNomeCognome(), 
                                    rigaRapportino.getRimborsoOre(cliente).toString(),
                                    rigaRapportino.getRimborsi().toString(), Utils.ivaValue.toString(), 
                                    rigaRapportino.getRimborsiArt15() ]);
                lastCommessa = riga.idCommessa;
            } else {
                let last = valueFattura[valueFattura.length -1];
                last[2] = (parseFloat(last[2]) + rigaRapportino.getRimborsoOre(cliente)).toString();
                last[3] = (parseFloat(last[3]) + rigaRapportino.getRimborsi()).toString();
                last[5] = (parseFloat(last[5]) + rigaRapportino.getRimborsiArt15()).toString();
            }
        }
        this.fattura = { header: nomeColonna, body:valueFattura };
    }

    getArray(riepilogo:Riepilogo[]) {
        let result = [];
        riepilogo.forEach(r => {
            result.push([UtilsRapportini.getCommessa(r.idCommessa).getNumero(), UtilsRapportini.getCliente(r.idCliente).getNomeCognome(),
                        r.data, r.totOreViaggioLavoro.toString(), r.totOreCorrette.toString(), r.rimborsi.toString(), 
                        r.rimborsi_art15.toString()]);
        });
        return result;
    }

    orderByCommessa(rr:Riepilogo[]):Riepilogo[] {
        return rr.sort((a, b) => {
            if (a[0] < b[0]) return -1;
            else if (a[0] > b[0]) return 1;
            else return 0;
        });
    }

    getRiga(idRiga:number):RigaRapportino {
        return this.rr.filter(r => {
            return r.id == idRiga;
        })[0];
    }
}