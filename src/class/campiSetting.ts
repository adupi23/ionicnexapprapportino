export class CampiSetting {

    private id:number;
    private idCampo:number;
    private attivo:boolean;
    private name:string;
    private value:string;

    constructor(_id:number, _idCampo:number, _attivo:boolean, _name:string, _value:string) {
        this.id = _id;
        this.idCampo = _idCampo;
        this.attivo = _attivo;
        this.name = _name;
        this.value = _value;
    }

    getID() { return this.id; }
    getIDCampo() { return this.idCampo; }

    getAttivo():boolean { return this.attivo as boolean; }
    setAttivo(_attivo:boolean) { this.attivo = _attivo; }

    getName() { return this.name }
    setName(_name:string) { this.name = _name; }

    getValue() { return this.value; }
    setValue(_value:string) { this.value = _value; }

}