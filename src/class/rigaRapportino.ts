import { UtilsContatti } from './../Utils/UtilsContatti';
import { UtilsCommessa } from './../Utils/UtilsCommessa';
import { Contatto } from './contatto';
export class RigaRapportino {
    public id:number;
    public idRapportino:number;
    public idCommessa:number;
    public data:string;
    public totOreViaggio:number;
    public totOreLavoro:number;
    public totKM:number;
    public pranzo_art15:number;
    public cena_art15:number;
    public hotel_art15:number;
    public altro_art15:number;
    public pranzo:number;
    public cena:number;
    public hotel:number;
    public altro:number;

    constructor(_id:number, _idRapportino:number, _idCommessa:number, _data:string, _totOViaggio:number, _totOLavoro:number,
                _totKM:number, _pranzo_art15:number, _cena_art15:number, _hotel_art15:number, _altro_art15:number, _pranzo:number,  _cena:number, _hotel:number, _altro:number) {
        this.id = _id;
        this.idRapportino = _idRapportino;
        this.idCommessa = _idCommessa;
        this.data = _data;
        this.totOreViaggio = _totOViaggio;
        this.totOreLavoro = _totOLavoro;
        this.totKM = _totKM;
        this.pranzo_art15 = _pranzo_art15;
        this.cena_art15 = _cena_art15;
        this.hotel_art15 = _hotel_art15;
        this.altro_art15 = _altro_art15;
        this.pranzo = _pranzo;
        this.cena = _cena;
        this.hotel = _hotel;
        this.altro = _altro;
    }

    getOreViaggioLavoro() { return this.totOreViaggio + this.totOreLavoro; }

    getTotRimborsoKm(tariffa:number) { return this.totKM * tariffa; }

    getVittoArt15() { return this.pranzo_art15 + this.cena_art15; }
    getVitto() { return this.pranzo + this.cena; }

    getRimborsiArt15() { return this.pranzo_art15 + this.cena_art15 + this.hotel_art15 + this.altro_art15; }
    getRimborsi() { return this.pranzo + this.cena + this.hotel + this.altro; }

    getRimborsoOre(cliente:Contatto) { 
        let ore = this.getOreViaggioLavoro();
        if (ore > cliente.getMaxOre()) {
            return cliente.getTariffaGiornata();
        } else {
            return ore * cliente.getTariffaOre();
        }
    }
}