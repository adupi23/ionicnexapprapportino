import { Documento } from './../../class/documento';
import { UtilsRapportini } from './../../Utils/UtilsRapportini';
import { RigaRapportino } from './../../class/rigaRapportino';
import { Commessa } from './../../class/commessa';
import { UtilsSetting } from './../../Utils/UtilsSetting';
import { CampiSetting } from './../../class/campiSetting';
import { Contatto } from '../../class/contatto';
import { Injectable } from '@angular/core';

import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Utils } from '../../Utils/Utils';
import { Utente } from '../../class/utente';
import { Rapportino } from '../../class/rapportino';

@Injectable()
export class DatabaseProvider {
  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;

  constructor(public sqlitePorter: SQLitePorter, private storage: Storage, 
              private sqlite: SQLite, private platform: Platform, private http: Http) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'nexRapportino.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        this.database = db;
        this.storage.get('database_created').then(val => {
          if (val) {
            this.databaseReady.next(true);
          } else {
            this.fillDatabase('assets/dump.sql');
          }
        });
      });
    });
  }

  fillDatabase(fileName:string) {
    this.http.get(fileName)
      .map(res => res.text())
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(data => {
            this.databaseReady.next(true);
            this.storage.set('database_created', true);
            this.storage.set('includi_persone', false);
            this.storage.set('email_to_send', '');
          })
          .catch(e => console.error(e));
      });
  }

  getUser(username:String, password:String):Promise<Utente[]> {
    var query = 'SELECT * FROM utente';
    var params = [];
    if (username != '' && password != '') {
      query += ' utente WHERE username = ? AND password = ?';
      params = [username, password];
    }
    return this.database.executeSql(query, params).then((data) => {
      let users:Utente[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let user = new Utente(data.rows.item(i).id, data.rows.item(i).username);
          users.push(user);
        }
      }
      return users;
    }, err => {
      Utils.writeLog(err);
      return [];
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable();
  }

  getContatti():Promise<Contatto[]> {
    var query = 'SELECT * FROM contatto ORDER BY id DESC';
    var params = [];
    return this.database.executeSql(query, params).then((data) => {
      var contatti:Contatto[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let contatto = new Contatto(data.rows.item(i).id, data.rows.item(i).id_company, 
                                      data.rows.item(i).type, data.rows.item(i).cognome_nome, 
                                      data.rows.item(i).telefono, data.rows.item(i).email, 
                                      data.rows.item(i).p_iva, data.rows.item(i).cf, 
                                      data.rows.item(i).sede_legale, data.rows.item(i).sede_operativa,
                                      data.rows.item(i).max_ore, data.rows.item(i).tariffa_ore,
                                      data.rows.item(i).tariffa_giornata, data.rows.item(i).tariffa_km);
          contatti.push(contatto);
        }
      }
      return contatti;
    }, err => {
      Utils.writeLog(err);
      return [];
    });
  }

  getContattiAssociati(idCompany:number):Promise<Contatto[]> {
    var query = 'SELECT * FROM contatto WHERE id_company = ?';
    var params = [idCompany];
    return this.database.executeSql(query, params).then((data) => {
      var contatti:Contatto[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let contatto = new Contatto(data.rows.item(i).id, data.rows.item(i).id_company, 
                                      data.rows.item(i).type, data.rows.item(i).cognome_nome, 
                                      data.rows.item(i).telefono, data.rows.item(i).email, 
                                      data.rows.item(i).p_iva, data.rows.item(i).cf, 
                                      data.rows.item(i).sede_legale, data.rows.item(i).sede_operativa,
                                      data.rows.item(i).max_ore, data.rows.item(i).tariffa_ore,
                                      data.rows.item(i).tariffa_giornata, data.rows.item(i).tariffa_km);
          contatti.push(contatto);
        }
      }
      return contatti;
    }, err => {
      Utils.writeLog(err);
      return [];
    });
  }

  addContatto(idCompany:number):Promise<Contatto> {
    var query = 'INSERT INTO contatto(id_company,type,cognome_nome,telefono,email,p_iva,cf,sede_legale,sede_operativa,username_creazione,username_modifica)' +
                              'VALUES (?,?,?,?,?,?,?,?,?,?,?)';
    var params = [idCompany, 'Azienda', '', '', '', '','','','', Utils.userLogged.getUsername(), Utils.userLogged.getUsername()];
    return this.database.executeSql(query, params).then((result) => {
      let contatto = new Contatto(result.insertId, idCompany, 'Azienda', '', '', '', '', '', '', '', 0, 0, 0, 0);
      return contatto;
    }, err => {
      Utils.writeLog(err);
      return null;
    });
  }

  deleteContatto(idContatto:number) {
    var query = 'DELETE FROM contatto WHERE id = ?';
    var params = [idContatto];
    return this.database.executeSql(query, params).then(() => {}, 
      err => {
        Utils.writeLog(err);
        return null;
      });
  }

  updateContatto(contatto:Contatto) {
    var query = 'UPDATE contatto SET type = ?, cognome_nome = ?,telefono = ?,email = ?,p_iva = ?,cf = ?,sede_legale = ?,' +
                                    'sede_operativa = ?, max_ore = ?, tariffa_ore = ?, tariffa_giornata = ?, tariffa_km = ?, ' +
                                    'username_modifica = ?, data_modifica = CURRENT_TIMESTAMP ' + 
                                    'WHERE id = ?';
    var params = [contatto.getType(), contatto.getNomeCognome(), contatto.getTelefono(), contatto.getEmail(), contatto.getPIva(),
                  contatto.getCF(), contatto.getSedeLegale(), contatto.getSedeOperativa(), contatto.getMaxOre(), contatto.getTariffaOre(),
                  contatto.getTariffaGiornata(), contatto.getTariffaKm(), Utils.userLogged.getUsername(), contatto.getID()];
    return this.database.executeSql(query, params).then(() => {}, 
      err => {
        Utils.writeLog(err.toString());
        return null;
      });
  }

  getCampiSettings():Promise<CampiSetting[]> {
    var query = 'SELECT * FROM settingCampo';
    var params = [];
    return this.database.executeSql(query, params).then((data) => {
      var campiLibero = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let campoLibero = new CampiSetting(data.rows.item(i).id, data.rows.item(i).id_campo, data.rows.item(i).attivo,
                                             data.rows.item(i).name, data.rows.item(i).value);
          campiLibero.push(campoLibero);
        }
      }
      return campiLibero;
    }, err => {
      Utils.writeLog(err);
      return [];
    });
  }

  addCampo(idCampo:number):Promise<CampiSetting> {
    var query = 'INSERT INTO settingCampo(id_campo, name, value) VALUES(?, ?, ?)';
    var params = [idCampo, '', 'default'];
    return this.database.executeSql(query, params).then((result) => {
      let campo = new CampiSetting(result.insertId, idCampo, true, '', 'default');
      UtilsSetting.campi.push(campo);
      return campo;
    }, err => {
      Utils.writeLog(err);
      return null;
    });
  }

  updateCampo(campo:CampiSetting) {
    var query = 'UPDATE settingCampo SET id_campo = ?, attivo = ?, name = ?, value = ? WHERE id = ?';
    var params = [campo.getIDCampo(), campo.getAttivo(), campo.getName(), campo.getValue(), campo.getID()];
    return this.database.executeSql(query, params).then(() => {}, 
      err => {
        Utils.writeLog(err);
        return null;
      });
  }

  removeCampo(option:CampiSetting) {
    var query = 'DELETE FROM settingCampo WHERE id = ?';
    var params = [option.getID()];
    return this.database.executeSql(query, params).then((result) => {
      let index = UtilsSetting.campi.indexOf(option);
      UtilsSetting.campi.splice(index, 1);
    }, err => {
      Utils.writeLog(err);
      return null;
    });
  }

  getCommesse():Promise<Commessa[]> {
    var query = 'SELECT * FROM commessa';
    var params = [];
    return this.database.executeSql(query, params).then((data) => {
      var listaCommesse:Commessa[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let commessa = new Commessa(data.rows.item(i).id, data.rows.item(i).numero, 
                                      data.rows.item(i).id_cliente, data.rows.item(i).id_lavorato, 
                                      [data.rows.item(i).libero1.toString(), data.rows.item(i).libero2.toString(), 
                                       data.rows.item(i).libero3.toString(), data.rows.item(i).libero4.toString()]);
          listaCommesse.push(commessa);
        }
      }
      return listaCommesse;
    }, err => {
      Utils.writeLog(err);
      return [];
    });
  }

  addCommessa():Promise<Commessa> {
    var query = 'INSERT INTO commessa(username_creazione, username_modifica) VALUES (?, ?)';
    var params = [Utils.userLogged.getUsername(), Utils.userLogged.getUsername()];
    return this.database.executeSql(query, params).then((result) => {
      let l:string[] = [];
      let commessa = new Commessa(result.insertId, '', 0, 0, l);
      return commessa;
    }, err => {
      Utils.writeLog(err);
      return null;
    });
  }

  updateCommessa(commessa:Commessa) {
    var query = 'UPDATE commessa SET numero = ?, id_cliente = ?, id_lavorato = ?, username_modifica = ?, data_modifica = CURRENT_TIMESTAMP, libero1 = ?, libero2 = ?, libero3 = ?, libero4 = ?' +
                'WHERE id = ?';
    var params = [commessa.getNumero(), commessa.getIdCliente(), commessa.getIdLavorato(), Utils.userLogged.getUsername()];
    for (var i = 0; i < 4; i++) {
      let v = commessa.getLibero()[i] != undefined ? commessa.getLibero()[i] : '';
      params.push(v);
    }
    params.push(commessa.getID());
    return this.database.executeSql(query, params).then((result) => {
      Utils.writeLog(result.toString());
    }, 
    err => {
      Utils.writeLog(err);
      return null;
    });
  }

  deleteCommessa(idCommesa:number) {
    var query = 'DELETE FROM commessa WHERE id = ?';
    var params = [idCommesa];
    return this.database.executeSql(query, params).then(() => {}, 
      err => {
        Utils.writeLog(err);
        return null;
      });
  }

  getRapportini():Promise<Rapportino[]> {
    var query = 'SELECT * FROM rapportino';
    var params = [];
    return this.database.executeSql(query, params).then((data) => {
      var listaRapportini:Rapportino[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let rapportino = new Rapportino(data.rows.item(i).id, data.rows.item(i).nomeRapportino);
          listaRapportini.push(rapportino);
        }
      }
      return listaRapportini;
    }, err => {
      Utils.writeLog(err);
      return [];
    });
  }

  addRapportino() {
    var query = 'INSERT INTO rapportino(username_creazione, username_modifica) VALUES (?, ?)';
    var params = [Utils.userLogged.getUsername(), Utils.userLogged.getUsername()];
    return this.database.executeSql(query, params).then((result) => {
      let rapportino = new Rapportino(result.insertId, '');
      return rapportino;
    }, err => {
      Utils.writeLog(err);
      return null;
    });
  }

  updateRapportino(rapportino:Rapportino) {
    var query = 'UPDATE rapportino SET nomeRapportino = ?, username_modifica = ?, data_modifica = CURRENT_TIMESTAMP WHERE id = ?';
    var params = [rapportino.nome, Utils.userLogged.getUsername(), rapportino.id];
    Utils.writeLog("nomeRapportino: " + rapportino.nome + " " + rapportino.id);
    return this.database.executeSql(query, params).then((result) => {
      Utils.writeLog(result.toString());
    }, 
    err => {
      Utils.writeLog(err);
      return null;
    });
  }

  deleteRapportino(idRapportino:number) {
    var query = 'PRAGMA foreign_keys = on;';
    var query2 = 'DELETE FROM rapportino WHERE id = ?;';
    var params = [idRapportino];
    return this.database.executeSql(query, []).then(() => {
      return this.database.executeSql(query2, params).then(() => {},
        err => {
          Utils.writeLog(err);
          return null;
        });
    }, 
      err => {
        Utils.writeLog(err);
        return null;
    });
  }

  addRigaRapportino(idRapportino:number, idCommessa:number) {
    var query = "INSERT INTO rigaRapportino(id_rapportino, id_commessa) VALUES(?, ?);";
    var params = [idRapportino, idCommessa];
    return this.database.executeSql(query, params).then((result) => {
      let rigaRapportino = new RigaRapportino(result.insertId, idRapportino, idCommessa, new Date().toDateString(),
                                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      return rigaRapportino;
    }, err => {
      Utils.writeLog(err);
      return null;
    });
  }

  getRigaRapportino(idRapportino):Promise<RigaRapportino[]> {
    var query = 'SELECT * FROM RigaRapportino WHERE id_rapportino = ?';
    var params = [idRapportino];
    return this.database.executeSql(query, params).then((data) => {
      var listaRigaRapportini:RigaRapportino[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let rigaRapportino = new RigaRapportino(data.rows.item(i).id, data.rows.item(i).id_rapportino, 
                                                  data.rows.item(i).id_commessa, new Date(data.rows.item(i).data).toDateString(), 
                                                  data.rows.item(i).tot_ore_viaggio, data.rows.item(i).tot_ore_lavoro, 
                                                  data.rows.item(i).tot_km, data.rows.item(i).pranzo_art15, data.rows.item(i).cena_art15, 
                                                  data.rows.item(i).hotel_art15, data.rows.item(i).altro_art15, data.rows.item(i).pranzo, 
                                                  data.rows.item(i).cena, data.rows.item(i).hotel, data.rows.item(i).altro);
          listaRigaRapportini.push(rigaRapportino);
        }
      }
      return listaRigaRapportini;
    }, err => {
      Utils.writeLog(err);
      return [];
    });
  }

  deleteRigaRapportino(idRiga:number) {
    var query = 'DELETE FROM rigaRapportino WHERE id = ?;';
    var params = [idRiga];
    return this.database.executeSql(query, params).then(() => {}, 
      err => {
        Utils.writeLog(err);
        return null;
    });
  }

  updateRigaRapportino(nomeCampo:string, value:string, idRigaRapportino) {
    let valueInt;
    if (nomeCampo.toLowerCase() != "data") {
      Utils.writeLog("Nome Campo: " + nomeCampo);
      if (value.trim() != "") {
        valueInt = parseFloat(value); 
      } else {
        valueInt = 0;
      }
      Utils.writeLog("Value: " + valueInt);
    } else {
      valueInt = value;
    }
    var query = 'UPDATE rigaRapportino SET ' + nomeCampo + ' = ? WHERE id = ?';
    var params = [valueInt, idRigaRapportino];
    return this.database.executeSql(query, params).then((result) => {
      Utils.writeLog(result.toString());
    }, 
    err => {
      Utils.writeLog(err);
      return null;
    });
  }

  getDocs(idRigaRapportino:number) {
    var query = 'SELECT * FROM documenti WHERE id_riga_rapportino = ?';
    var params = [idRigaRapportino];
    return this.database.executeSql(query, params).then((data) => {
      var listaDocs:Documento[] = [];
      Utils.writeLog("RIGHE DOC: " + data.rows.length);
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          let doc = new Documento(data.rows.item(i).id, data.rows.item(i).id_riga_rapportino, 
                                  data.rows.item(i).titolo, data.rows.item(i).path);
          listaDocs.push(doc);
        }
      }
      return listaDocs;
    }, err => {
      Utils.writeLog(JSON.stringify(err));
      return [];
    });
  }

  addDoc(idRigaRapportino:number, titolo:string, path:string) {
    var query = "INSERT INTO documenti(id_riga_rapportino, titolo, path, username_creazione) VALUES(?, ?, ?, ?);";
    var params = [idRigaRapportino, titolo.replace('.jpg', ''), path + titolo, Utils.userLogged.getUsername()];
    return this.database.executeSql(query, params).then((result) => {
      return new Documento(result.insertId, idRigaRapportino, titolo.replace('.jpg', ''), path + titolo);
    }, err => {
      Utils.writeLog(JSON.stringify(err));
      return null;
    });
  }

  updateDoc(doc:Documento) {
    var query = 'UPDATE documenti SET titolo = ? WHERE id = ?';
    var params = [doc.titolo, doc.id];
    Utils.writeLog("nome DOC db: " + doc.titolo + " " + doc.id);
    return this.database.executeSql(query, params).then((result) => {
      Utils.writeLog(JSON.stringify(result));
    }, 
    err => {
      Utils.writeLog(JSON.stringify(err));
      return null;
    });
  }

  deleteDoc(idDoc:number) {
    var query = 'DELETE FROM documenti WHERE id = ?;';
    var params = [idDoc];
    return this.database.executeSql(query, params).then(() => {}, 
      err => {
        Utils.writeLog(err);
        return null;
    });
  }
}
