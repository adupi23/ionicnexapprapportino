CREATE TABLE IF NOT EXISTS utente(id INTEGER PRIMARY KEY, username TEXT, password TEXT);
INSERT INTO utente(username, password) VALUES('Nexapp', 'Nexpwd18!');

CREATE TABLE IF NOT EXISTS contatto(id INTEGER PRIMARY KEY, id_company INTEGER NOT NULL, type TEXT NOT NULL, cognome_nome TEXT NOT NULL, telefono TEXT, email TEXT, p_iva TEXT, cf TEXT, sede_legale TEXT, sede_operativa TEXT, max_ore INTEGER DEFAULT 0, tariffa_ore DOUBLE DEFAULT 1, tariffa_giornata DOUBLE DEFAULT 1, tariffa_km DOUBLE DEFAULT 1, username_creazione TEXT NOT NULL, data_creazione TIMESTAMP DEFAULT CURRENT_TIMESTAMP, username_modifica TEXT NOT NULL, data_modifica TIMESTAMP DEFAULT CURRENT_TIMESTAMP); 
-- INSERT INTO `contatto` (`id_company`,`type`,`cognome_nome`,`telefono`,`email`,`p_iva`,`cf`,`sede_legale`,`sede_operativa`,`username_creazione`,`username_modifica`) VALUES (28,"","Dotson Eric","1-588-235-9314","Vestibulum.ut.eros@ametdiameu.edu","704-7134 Nisl. St.","","Santa Cesarea Terme","PUG","Nexapp","Nexapp");

UPDATE contatto SET id_company = 0, type = 'Azienda';

CREATE TABLE IF NOT EXISTS commessa(id INTEGER PRIMARY KEY, numero TEXT NOT NULL DEFAULT '', id_cliente INTEGER NOT NULL DEFAULT 0, id_lavorato INTEGER NOT NULL DEFAULT 0, libero1 TEXT NOT NULL DEFAULT '', libero2 TEXT NOT NULL DEFAULT '', libero3 TEXT NOT NULL DEFAULT '', libero4 TEXT NOT NULL DEFAULT '', username_creazione TEXT NOT NULL, data_creazione TIMESTAMP DEFAULT CURRENT_TIMESTAMP, username_modifica TEXT NOT NULL, data_modifica TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE IF NOT EXISTS settingCampo(id INTEGER PRIMARY KEY, id_campo INTEGER DEFAULT 0, attivo BOOLEAN DEFAULT true, name TEXT NOT NULL, value TEXT NOT NULL DEFAULT '');
INSERT INTO settingCampo(name) VALUES('libero1'), ('libero2'), ('libero3'), ('libero4'); 

CREATE TABLE IF NOT EXISTS rapportino(id INTEGER PRIMARY KEY, nomeRapportino TEXT NOT NULL DEFAULT '', username_creazione TEXT NOT NULL, data_creazione TIMESTAMP DEFAULT CURRENT_TIMESTAMP, username_modifica TEXT NOT NULL, data_modifica TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
CREATE TABLE IF NOT EXISTS rigaRapportino(id INTEGER PRIMARY KEY, id_rapportino INTEGER NOT NULL, id_commessa INTEGER NOT NULL, data TEXT NOT NULL DEFAULT CURRENT_DATE, tot_ore_viaggio DOUBLE DEFAULT 0, tot_ore_lavoro DOUBLE DEFAULT 0, tot_km DOUBLE DEFAULT 0, pranzo_art15 DOUBLE DEFAULT 0, cena_art15 DOUBLE DEFAULT 0, hotel_art15 DOUBLE DEFAULT 0, altro_art15 DOUBLE DEFAULT 0, pranzo DOUBLE DEFAULT 0, cena DOUBLE DEFAULT 0, hotel DOUBLE DEFAULT 0, altro DOUBLE DEFAULT 0, FOREIGN KEY (id_rapportino) REFERENCES rapportino(id) ON DELETE CASCADE);
PRAGMA foreign_keys=on;
CREATE TABLE IF NOT EXISTS documenti(id INTEGER PRIMARY KEY, id_riga_rapportino INTEGER NOT NULL, titolo TEXT NOT NULL DEFAULT '', path TEXT NOT NULL DEFAULT '', username_creazione TEXT NOT NULL, data_creazione TIMESTAMP DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY (id_riga_rapportino) REFERENCES rigaRapportino(id));

