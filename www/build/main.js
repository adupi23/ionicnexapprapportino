webpackJsonp([0],{

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utils; });
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.writeLog = function (message) {
        if (this.debug) {
            console.log(message);
        }
    };
    Utils.setUserLogged = function (_user) {
        this.userLogged = _user;
    };
    Utils.presentToast = function (message, context) {
        var toast = context.create({
            message: message,
            duration: 4000
        });
        toast.present();
    };
    Utils.debug = true;
    Utils.emailToSend = "";
    Utils.ipws = "";
    return Utils;
}());

//# sourceMappingURL=Utils.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_storage__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dettagli_setting_dettagli_setting__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SettingPage = /** @class */ (function () {
    // name:string[] = [];
    function SettingPage(navCtrl, navParams, databaseprovider, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.storage = storage;
        this.campi = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.filter(function (campo) {
            return campo.getIDCampo() == 0;
        });
        this.persone = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone;
        this.emailToSend = __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].emailToSend;
        this.ipws = __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].ipws;
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("N campi: " + this.campi.length);
    }
    SettingPage.prototype.ionViewDidEnter = function () {
    };
    SettingPage.prototype.dettagli = function (campo) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__dettagli_setting_dettagli_setting__["a" /* DettagliSettingPage */], {
            setting: campo,
            modal: false
        });
    };
    SettingPage.prototype.changeIncludiPersone = function () {
        __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone = this.persone;
        this.storage.set('includi_persone', this.persone);
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Includi Persone: " + __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone);
    };
    SettingPage.prototype.changeEmail = function () {
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].emailToSend = this.emailToSend;
        this.storage.set('email_to_send', __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].emailToSend);
    };
    SettingPage.prototype.changeIP = function () {
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].ipws = this.ipws;
        this.storage.set('ipws', __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].ipws);
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["Component"])({
            selector: 'page-setting',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/setting/setting.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Impostazioni</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>\n      Campi Liberi Commessa\n    </ion-list-header>\n    <div *ngFor="let c of campi">\n      <div *ngIf="c?.getIDCampo() == 0">\n      <ion-item #item (click)="dettagli(c)">\n        <!-- <ion-item> -->\n          <ion-label>Libero {{c.getID()}}: {{ c.getName() }}</ion-label> \n          <!-- <ion-input type="text" placeholder="Nome campo" [(ngModel)]="name[campi.indexOf(c)]" (ionChange)="changeName($event, c)"></ion-input> -->\n          <!-- <ion-toggle checked="{{ c?.getAttivo() }}" (ionChange)="changeActive($event, c)"></ion-toggle> -->\n          <ion-icon padding-left name="arrow-forward"  item-end></ion-icon> <!-- (click)="dettagli(c)" -->\n        <!-- </ion-item> -->\n        <!-- <ion-item-options side="right">\n          <button ion-button (click)="dettagli(c)">\n            <ion-icon name="options"></ion-icon>Default\n          </button>\n        </ion-item-options> -->\n      </ion-item>\n      </div>\n    </div>\n    <ion-list-header>\n      Opzioni Commessa\n    </ion-list-header>\n    <ion-item>\n      <ion-label>Includere le persone nella commessa ?</ion-label>\n      <ion-checkbox (ionChange)="changeIncludiPersone()" [(ngModel)]="persone"></ion-checkbox>\n    </ion-item>\n    <ion-list-header>\n      Opzioni Rapportino\n    </ion-list-header>\n    <ion-item>\n      <ion-label floating>Email a cui inviare i rapportini</ion-label>\n      <ion-input (ionChange)="changeEmail()" [(ngModel)]="emailToSend" type="email"></ion-input>\n    </ion-item>\n    <!-- <ion-list-header>\n      Sviluppo\n    </ion-list-header>\n    <ion-item>\n      <ion-label floating>IP WS</ion-label>\n      <ion-input (ionChange)="changeIP()" [(ngModel)]="ipws"></ion-input>\n    </ion-item> -->\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/setting/setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_0__ionic_storage__["b" /* Storage */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DettagliSettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DettagliSettingPage = /** @class */ (function () {
    function DettagliSettingPage(navCtrl, navParams, databaseprovider, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.viewCtrl = viewCtrl;
        this.values = [];
        this.campoSelected = navParams.get('setting');
        this.isModal = navParams.get('modal');
        this.nomeCampo = this.campoSelected.getName();
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Modal: " + this.isModal);
        this.listOption = this.filterOptions();
        this.setValues();
    }
    DettagliSettingPage.prototype.changeName = function () {
        var index = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.indexOf(this.campoSelected);
        var value = this.nomeCampo;
        this.campoSelected.setName(value);
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("name value: " + this.campoSelected.getName());
        __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi[index].setName(value);
        this.databaseprovider.updateCampo(this.campoSelected).then(function () {
            __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Update success");
        });
    };
    DettagliSettingPage.prototype.changeValue = function (ev, option) {
        var _this = this;
        var index = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.indexOf(option);
        var value = this.values[this.listOption.indexOf(option)];
        option.setName(value);
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("name value: " + option.getName());
        __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi[index].setValue(value);
        this.databaseprovider.updateCampo(option).then(function () {
            __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Update success");
            _this.setValues();
        });
    };
    DettagliSettingPage.prototype.changeActive = function (item, campo) {
        var i = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.indexOf(campo);
        campo.setAttivo(item.checked);
        __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi[i].setAttivo(item.checked);
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("attivo: " + campo.getAttivo() + " id: " + campo.getID());
        this.databaseprovider.updateCampo(campo).then(function () {
            __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Update success");
        });
    };
    DettagliSettingPage.prototype.addOption = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Add Option func");
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addCampo(_this.campoSelected.getID())
                    .then(function (campo) {
                    if (campo != null) {
                        _this.listOption = _this.filterOptions();
                        _this.setValues();
                    }
                });
            }
        });
    };
    DettagliSettingPage.prototype.removeOption = function (option) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Remove Option func");
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.removeCampo(option)
                    .then(function () {
                    _this.listOption = _this.filterOptions();
                    _this.setValues();
                });
            }
        });
    };
    DettagliSettingPage.prototype.filterOptions = function () {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.filter(function (campo) {
            return campo.getIDCampo() == _this.campoSelected.getID();
        });
    };
    DettagliSettingPage.prototype.setValues = function () {
        var _this = this;
        this.values = [];
        this.listOption.forEach(function (c) {
            _this.values.push(c.getValue());
        });
    };
    DettagliSettingPage.prototype.choose = function (option) {
        var value = this.values[this.listOption.indexOf(option)];
        this.viewCtrl.dismiss(value);
    };
    DettagliSettingPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DettagliSettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-dettagli-setting',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/dettagli-setting/dettagli-setting.html"*/'<ion-header>\n  <div *ngIf="!isModal; then navBar; else toolBar"></div>\n  <ng-template #navBar>\n    <ion-navbar>\n      <ion-title>{{ campoSelected.getName() }}</ion-title>\n    </ion-navbar>\n  </ng-template>\n  <ng-template #toolBar>\n    <ion-toolbar>\n      <ion-title>{{ campoSelected.getName() }}</ion-title>\n      <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n          <span ion-text color="primary" showWhen="ios">Cancel</span>\n          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ng-template> \n</ion-header>\n\n<ion-content>\n  <ion-grid no-padding>\n    <div *ngIf="!isModal">\n      <ion-row fixed class="background">\n        <ion-col text-center col-4>\n          <ion-label>{{ campoSelected.getAttivo().toString() == \'true\' ? \'ATTIVO\' : \'DISATTIVO\' }}</ion-label>\n        </ion-col>\n        <ion-col no-padding col-4></ion-col>\n        <ion-col ion-item text-center col-4 class="background">\n          <ion-toggle item-end checked="{{ campoSelected?.getAttivo() }}" (ionChange)="changeActive($event, campoSelected)"></ion-toggle>\n        </ion-col>\n      </ion-row>\n    </div>\n    <ion-row fixed class="background-light" padding-bottom>\n      <ion-col col-1></ion-col>\n      <ion-col col-10 ion-item class="background-light">\n        <ion-label floating>Nome Campo</ion-label>\n        <ion-input type="text" [(ngModel)]="nomeCampo" (ionChange)="changeName()"></ion-input>\n      </ion-col>\n      <ion-col col-1></ion-col>\n    </ion-row>\n    <div *ngIf="campoSelected?.getAttivo().toString() == \'true\'">\n      <h3 padding-left>Valori di default</h3>\n      <ion-row *ngFor="let o of listOption">\n        <ion-col ion-item no-padding text-center col-2 *ngIf="isModal">\n          <ion-icon padding-left item-start (click)="choose(o)" name="checkmark"></ion-icon>\n        </ion-col>\n        \n        <div *ngIf="isModal; then col8 else col10"></div>\n        <ng-template #col8>\n          <ion-col ion-item no-padding col-8>\n            <ion-label floating>Opzione {{ listOption.indexOf(o) + 1 }}</ion-label>\n            <ion-input type="text" [(ngModel)]="values[listOption.indexOf(o)]" (ionChange)="changeValue($event, o)"></ion-input>\n          </ion-col>\n        </ng-template>\n        <ng-template #col10>\n          <ion-col padding-left ion-item no-padding col-10>\n            <ion-label floating>Opzione {{ listOption.indexOf(o) + 1 }}</ion-label>\n            <ion-input type="text" [(ngModel)]="values[listOption.indexOf(o)]" (ionChange)="changeValue($event, o)"></ion-input>\n          </ion-col>\n        </ng-template>\n        \n        <ion-col ion-item no-padding text-center col-2>\n          <ion-icon padding-right item-end name="close" color="danger" (click)="removeOption(o)"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-grid>\n  <div *ngIf="campoSelected?.getAttivo().toString() == \'true\'">\n    <!-- <ion-item no-lines>\n      <ion-icon padding-right item-end name="add" color="secondary" (click)="addOption()"></ion-icon>\n    </ion-item> -->\n    <ion-row>\n      <ion-col text-center padding-bottom>\n        <button ion-button block (click)="addOption()">Aggiungi Valore</button>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/dettagli-setting/dettagli-setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["ViewController"]])
    ], DettagliSettingPage);
    return DettagliSettingPage;
}());

//# sourceMappingURL=dettagli-setting.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommessePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dettagli_comessa_dettagli_comessa__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CommessePage = /** @class */ (function () {
    function CommessePage(navCtrl, navParams, databaseprovider, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.listaCommesseUsed = [];
        this.listaCommesse = [];
        this.startIndex = 0;
        this.lastIndex = 0;
        this.enabled = true;
        //testo per la ricerca
        this.showSearch = false;
        this.searchText = "";
        this.oldSearchText = "";
        this.searching = false;
        this.listaCommesseUsed = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse;
        this.loadCommesse();
    }
    CommessePage_1 = CommessePage;
    CommessePage.prototype.nuova = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addCommessa()
                    .then(function (commessa) {
                    if (commessa != null) {
                        _this.openCommessa(commessa, true);
                    }
                });
            }
        });
    };
    CommessePage.prototype.openCommessa = function (commessa, nuova) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__dettagli_comessa_dettagli_comessa__["a" /* DettagliComessaPage */], {
            commessa: commessa,
            nuovo: nuova
        });
    };
    CommessePage.prototype.doInfinite = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                if (_this.listaCommesse.length < _this.listaCommesseUsed.length) {
                    _this.loadCommesse();
                }
                __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("Commesse caricate visibili: " + _this.listaCommesse.length);
                // this.toTopButtonVisible = true;
                _this.searching = false;
                resolve(_this.listaCommesse);
            }, 700);
        });
    };
    CommessePage.prototype.loadCommesse = function () {
        if (this.listaCommesseUsed.length > 0) {
            this.lastIndex = this.listaCommesseUsed.length;
            if (this.lastIndex > (20 + this.startIndex)) {
                this.lastIndex = 20 + this.startIndex;
            }
            for (var i = this.startIndex; i < this.lastIndex; i++) {
                this.listaCommesse.push(this.listaCommesseUsed[i]);
            }
        }
        this.startIndex = this.lastIndex;
        if (this.listaCommesse.length == this.listaCommesseUsed.length) {
            this.enabled = false;
        }
        this.searching = false;
    };
    CommessePage.prototype.scrollTop = function () {
        this.content.scrollToTop();
    };
    CommessePage.prototype.openSearch = function () {
        if (!this.showSearch) {
            this.searchText = "";
            this.showSearch = true;
        }
        else {
            this.showSearch = false;
        }
    };
    CommessePage.prototype.delete = function (item, commessa) {
        //item.close()
        var indexGlobal = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.indexOf(commessa);
        this.doDelete(indexGlobal, commessa);
    };
    CommessePage.prototype.doDelete = function (index, commessa) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Attenzione',
            message: 'Sei sicuro di voler eliminare questa commessa?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () { }
                },
                {
                    text: 'Elimina',
                    handler: function () {
                        __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].removeListaCommesse(index);
                        _this.searching = true;
                        _this.enabled = true;
                        _this.startIndex = 0;
                        _this.lastIndex = 0;
                        _this.listaCommesse = [];
                        _this.listaCommesseUsed = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse;
                        if (_this.searchText != "") {
                            _this.listaCommesseUsed = _this.filterList(_this.searchText);
                        }
                        _this.loadCommesse();
                        __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog(commessa.getNumero() + " Eliminato");
                        _this.databaseprovider.deleteCommessa(commessa.getID());
                    }
                }
            ]
        });
        alert.present();
    };
    CommessePage.prototype.filterList = function (searchTerm) {
        var listToUse = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse;
        if (searchTerm.length > this.oldSearchText.length) {
            listToUse = this.listaCommesseUsed;
        }
        this.oldSearchText = searchTerm;
        return listToUse.filter(function (commessa) {
            return commessa.getNumero().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    //search
    CommessePage.prototype.resetList = function () {
        __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("reset list");
        this.startIndex = 0;
        this.lastIndex = 0;
        this.enabled = true;
        this.listaCommesse = [];
        this.listaCommesseUsed = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse;
        this.loadCommesse();
    };
    CommessePage.prototype.onClearSearchbar = function (ev) {
        __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("Clear Searchbar");
        this.searching = true;
        this.resetList();
        this.showSearch = false;
    };
    CommessePage.prototype.onCancelSearchbar = function (ev) {
        __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("Cancel Searchbar");
        this.searching = true;
        this.resetList();
        this.showSearch = false;
    };
    CommessePage.prototype.changedInput = function (ev) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("text changed");
        this.searching = true;
        this.enabled = true;
        setTimeout(function () {
            _this.searching = false;
            if (_this.searchText != "") {
                var tmpList = _this.filterList(_this.searchText);
                __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("Commesse filtrate: " + tmpList.length);
                _this.startIndex = 0;
                _this.lastIndex = 0;
                _this.listaCommesse = [];
                _this.listaCommesseUsed = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse;
                _this.loadCommesse();
                __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("Commesse visibili dopo filtro: " + _this.listaCommesse.length);
            }
            else {
                _this.oldSearchText = "";
                _this.resetList();
            }
        }, 300);
    };
    CommessePage.prototype.ionViewDidEnter = function () {
        if (CommessePage_1.listChanges) {
            CommessePage_1.listChanges = false;
            this.resetList();
        }
    };
    CommessePage.prototype.presentToast = function (message, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: position
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    CommessePage.listChanges = false;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Content"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Content"])
    ], CommessePage.prototype, "content", void 0);
    CommessePage = CommessePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
            selector: 'page-commesse',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/commesse/commesse.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Commesse</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="openSearch()">\n        <ion-icon name="search"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf="showSearch">\n    <ion-searchbar [(ngModel)]="searchText" animated="true" showCancelButton \n          debounce="500" placeholder="Cerca" \n          (ionInput)="changedInput($event)" (ionCancel)="onCancelSearchbar($event)" \n          (ionClear)="onClearSearchbar($event)">\n    </ion-searchbar>\n  </div>\n  <div *ngIf="searching" class="spinner-container">\n    <ion-spinner></ion-spinner>\n  </div>\n  <button ion-item (click)="nuova()">\n    <ion-icon name="add-circle" item-start></ion-icon>\n      Nuovo\n    <div class="item-note" item-end></div>\n  </button>\n  <div *ngIf="listaCommesse?.length > 0; then commesse; else nocommesse"></div>\n  <ng-template #commesse>\n    <ion-list>\n      <ion-item-sliding #item *ngFor="let commessa of listaCommesse">\n        <button ion-item (click)="openCommessa(commessa, false)">\n          <ion-icon name="pricetag" item-start></ion-icon>\n          {{ commessa.getNumero() }}\n        </button>\n        <ion-item-options icon-start side="left" (ionSwipe)="delete($event, commessa)">\n          <button ion-button color="danger" (click)="delete(item, commessa)" expandable>\n            <ion-icon name="trash"></ion-icon>Elimina\n          </button>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfinite())" [enabled]="enabled">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ng-template>\n  <!-- <div *ngIf="toTopButtonVisible; then show"></div> -->\n  <!-- <ng-template #show> -->\n  <ion-fab bottom right>\n    <button ion-fab color="primary" (click)="scrollTop()">\n      <ion-icon name="arrow-dropup"></ion-icon>\n    </button>\n  </ion-fab>\n  <!-- </ng-template> -->\n  <ng-template #nocommesse>\n    <p>Non sono presenti commesse</p>\n  </ng-template>\n\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/commesse/commesse.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["ToastController"]])
    ], CommessePage);
    return CommessePage;
    var CommessePage_1;
}());

//# sourceMappingURL=commesse.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, menu, databaseprovider) {
        this.navCtrl = navCtrl;
        this.databaseprovider = databaseprovider;
        menu.enable(true);
        menu.swipeEnable(false);
    }
    HomePage.prototype.getUsers = function () {
        this.databaseprovider.getUser('', '')
            .then(function (users) {
            if (users.length > 0) {
                for (var i = 0; i < users.length; i++) {
                    __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("User data: " + users[0].getId() + " " + users[0].getUsername());
                }
            }
            else {
                __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog("No Data");
            }
        })
            .catch(function (e) { return __WEBPACK_IMPORTED_MODULE_0__Utils_Utils__["a" /* Utils */].writeLog(e); });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h3>Ionic Menu Starter</h3>\n\n  <button ion-button secondary (click)="getUsers()">Vedi utenti</button>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1__providers_database_database__["a" /* DatabaseProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContattiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_call_number__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contatto_dettagli_contatto_dettagli__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ContattiPage = /** @class */ (function () {
    function ContattiPage(navCtrl, navParams, platform, alertCtrl, databaseprovider, toastCtrl, callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.databaseprovider = databaseprovider;
        this.toastCtrl = toastCtrl;
        this.callNumber = callNumber;
        //lista dei contatti visibili
        this.listaContatti = [];
        //lista da utilizzare per riempire listaContatti
        this.listaContattiUsed = [];
        //Indici per il caricamento parziale dei dati
        this.startIndex = 0;
        this.lastIndex = 0;
        //diventa false quando non ci sono più dati da poter caricare
        this.enabled = true;
        //true dopo il caricamento della seconda parte di dati, il bottene serve a tornare in cima
        // toTopButtonVisible:boolean = false;
        //testo per la ricerca
        this.showSearch = false;
        this.searchText = "";
        this.oldSearchText = "";
        //true quando viene mostrato il caricamento dei dati filtrati
        this.searching = false;
        //platform.ready().then((() => this.screenHeight = platform.height()));
        this.listaContattiUsed = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti;
        this.loadContatti();
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Totale contatti: " + __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.length);
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Contatti caricati: " + this.listaContatti.length);
    }
    ContattiPage_1 = ContattiPage;
    ContattiPage.prototype.nuovo = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addContatto(0)
                    .then(function (contatto) {
                    if (contatto != null) {
                        _this.openContatto(contatto, true);
                    }
                });
            }
        });
    };
    ContattiPage.prototype.doInfinite = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                if (_this.listaContatti.length < _this.listaContattiUsed.length) {
                    _this.loadContatti();
                }
                __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Contatti caricati visibili: " + _this.listaContatti.length);
                // this.toTopButtonVisible = true;
                _this.searching = false;
                resolve(_this.listaContatti);
            }, 700);
        });
    };
    ContattiPage.prototype.loadContatti = function () {
        if (this.listaContattiUsed.length > 0) {
            this.lastIndex = this.listaContattiUsed.length;
            if (this.lastIndex > (20 + this.startIndex)) {
                this.lastIndex = 20 + this.startIndex;
            }
            for (var i = this.startIndex; i < this.lastIndex; i++) {
                this.listaContatti.push(this.listaContattiUsed[i]);
            }
        }
        this.startIndex = this.lastIndex;
        if (this.listaContatti.length == this.listaContattiUsed.length) {
            this.enabled = false;
        }
        this.searching = false;
    };
    ContattiPage.prototype.scrollTop = function () {
        this.content.scrollToTop();
    };
    ContattiPage.prototype.openSearch = function () {
        if (!this.showSearch) {
            this.searchText = "";
            this.showSearch = true;
        }
        else {
            this.showSearch = false;
        }
    };
    ContattiPage.prototype.openContatto = function (contatto, nuovo) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__contatto_dettagli_contatto_dettagli__["a" /* ContattoDettagliPage */], {
            contatto: contatto,
            nuovo: nuovo
        });
    };
    ContattiPage.prototype.delete = function (item, contatto) {
        //item.close()
        var indexGlobal = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.indexOf(contatto);
        if (contatto.getIdCompany() > 0) {
            if (indexGlobal > -1) {
                this.doDelete(indexGlobal, contatto);
            }
        }
        else {
            //Utils.writeLog(contatto.getNomeCognome() + " è una compagnia");
            if (__WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociati(contatto.getID()).length > 0) {
                //messaggio
                __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].presentToast('Prima di eliminare questa azienda dovresti eliminare tutti i suoi contatti associati', this.toastCtrl);
            }
            else {
                //elimina
                this.doDelete(indexGlobal, contatto);
            }
        }
    };
    ContattiPage.prototype.doDelete = function (index, contatto) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Attenzione',
            message: 'Sei sicuro di voler eliminare questo contatto?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () { }
                },
                {
                    text: 'Elimina',
                    handler: function () {
                        __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].removeListaContatti(index);
                        // this.listaContattiUsed.splice(indexUsed, 1);
                        // this.listaContatti.splice(index, 1);
                        _this.searching = true;
                        _this.enabled = true;
                        _this.startIndex = 0;
                        _this.lastIndex = 0;
                        _this.listaContatti = [];
                        _this.listaContattiUsed = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti;
                        if (_this.searchText != "") {
                            _this.listaContattiUsed = _this.filterList(_this.searchText);
                        }
                        _this.loadContatti();
                        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog(contatto.getNomeCognome() + " Eliminato");
                        _this.databaseprovider.deleteContatto(contatto.getID());
                    }
                }
            ]
        });
        alert.present();
    };
    //search
    ContattiPage.prototype.resetList = function () {
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("reset list");
        this.startIndex = 0;
        this.lastIndex = 0;
        this.enabled = true;
        this.listaContatti = [];
        this.listaContattiUsed = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti;
        this.loadContatti();
    };
    ContattiPage.prototype.onClearSearchbar = function (ev) {
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Clear Searchbar");
        this.searching = true;
        this.resetList();
        this.showSearch = false;
    };
    ContattiPage.prototype.onCancelSearchbar = function (ev) {
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Cancel Searchbar");
        this.searching = true;
        this.resetList();
        this.showSearch = false;
    };
    ContattiPage.prototype.changedInput = function (ev) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("text changed");
        this.searching = true;
        this.enabled = true;
        setTimeout(function () {
            _this.searching = false;
            if (_this.searchText != "") {
                var tmpList = _this.filterList(_this.searchText);
                __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Contatti filtrati: " + tmpList.length);
                _this.startIndex = 0;
                _this.lastIndex = 0;
                _this.listaContatti = [];
                _this.listaContattiUsed = tmpList;
                _this.loadContatti();
                __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Contatti visibili dopo filtro: " + _this.listaContatti.length);
            }
            else {
                _this.oldSearchText = "";
                _this.resetList();
            }
        }, 300);
    };
    ContattiPage.prototype.filterList = function (searchTerm) {
        var listToUse = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti;
        if (searchTerm.length > this.oldSearchText.length) {
            listToUse = this.listaContattiUsed;
        }
        this.oldSearchText = searchTerm;
        return listToUse.filter(function (contatto) {
            return contatto.getNomeCognome().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    ContattiPage.prototype.ionViewDidEnter = function () {
        if (ContattiPage_1.listChanges) {
            ContattiPage_1.listChanges = false;
            this.resetList();
        }
    };
    ContattiPage.prototype.call = function (contatto) {
        this.callNumber.callNumber(contatto.getTelefono(), true)
            .then(function (res) { return console.log('Vai a compositore', res); })
            .catch(function (err) { return console.log('Error: ', err); });
    };
    ContattiPage.listChanges = false;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["Content"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["Content"])
    ], ContattiPage.prototype, "content", void 0);
    ContattiPage = ContattiPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["Component"])({
            selector: 'page-contatti',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/contatti/contatti.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Contatti</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="openSearch()">\n        <ion-icon name="search"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="showSearch">\n    <ion-searchbar [(ngModel)]="searchText" animated="true" showCancelButton \n          debounce="500" placeholder="Cerca" \n          (ionInput)="changedInput($event)" (ionCancel)="onCancelSearchbar($event)" \n          (ionClear)="onClearSearchbar($event)">\n    </ion-searchbar>\n  </div>\n  <div *ngIf="searching" class="spinner-container">\n      <ion-spinner></ion-spinner>\n  </div>\n  <button ion-item (click)="nuovo()" >\n    <ion-icon name="add-circle" item-start></ion-icon>\n      Nuovo\n    <div class="item-note" item-end></div>\n  </button>\n  <div *ngIf="listaContatti?.length > 0; then contatti; else nocontatti"></div>\n  <ng-template #contatti>\n    <ion-list>\n      <ion-item-sliding #item *ngFor="let contatto of listaContatti">\n        <button ion-item (click)="openContatto(contatto, false)">\n          <div *ngIf="contatto?.getType() == \'Azienda\'; then azienda else persona"></div>\n          <ng-template #persona><ion-icon ios="ios-contact-outline" md="md-contact" item-start></ion-icon></ng-template>\n          <ng-template #azienda><ion-icon name="contacts" color="{{contatto?.getIdCompany() == 0 ? \'secondary\' : \'\'}}" item-start></ion-icon></ng-template>\n          {{contatto.getNomeCognome()}}\n        </button>\n        <ion-item-options icon-start side="left" (ionSwipe)="delete($event, contatto)">\n          <button ion-button color="danger" (click)="delete(item, contatto)" expandable>\n            <ion-icon name="trash"></ion-icon>Elimina\n          </button>\n        </ion-item-options>\n        <ion-item-options icon-start side="right">\n          <a ion-button href="mailto:{{ contatto.getEmail() }}" color="secondary">\n            <ion-icon name="mail"></ion-icon>E-mail\n          </a>\n          <button ion-button color="blue" (click)="call(contatto)">\n            <ion-icon ios="ios-call-outline" md="md-call"></ion-icon>Chiama\n          </button>\n          <!-- <a ion-button href="tel:{{ contatto.getTelefono() }}" color="blue">\n              <ion-icon ios="ios-call-outline" md="md-call"></ion-icon>Chiama\n          </a> -->\n        </ion-item-options>\n      </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfinite())" [enabled]="enabled">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ng-template>\n  <!-- <div *ngIf="toTopButtonVisible; then show"></div> -->\n  <!-- <ng-template #show> -->\n  <ion-fab bottom right>\n    <button ion-fab color="primary" (click)="scrollTop()">\n      <ion-icon name="arrow-dropup"></ion-icon>\n    </button>\n  </ion-fab>\n  <!-- </ng-template> -->\n  <ng-template #nocontatti>\n    <p>Non sono presenti contatti</p>\n  </ng-template>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/contatti/contatti.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_0__ionic_native_call_number__["a" /* CallNumber */]])
    ], ContattiPage);
    return ContattiPage;
    var ContattiPage_1;
}());

//# sourceMappingURL=contatti.js.map

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 183;

/***/ }),

/***/ 184:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocumentiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_camera__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DocumentiPage = /** @class */ (function () {
    function DocumentiPage(navCtrl, navParams, databaseprovider, camera, platform, file, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.camera = camera;
        this.platform = platform;
        this.file = file;
        this.alertCtrl = alertCtrl;
        this.listaDocs = [];
        this.lastImage = null;
        this.listaTitoliDoc = [];
        this.idRiga = navParams.get('riga');
        this.databaseprovider.getDocs(this.idRiga.id).then(function (data) {
            _this.listaDocs = data;
            __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog("Count Docs: " + _this.listaDocs.length);
            _this.loadTitoli();
        });
    }
    DocumentiPage.prototype.takePhoto = function () {
        var _this = this;
        var options = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.CAMERA,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            // if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            //   this.filePath.resolveNativePath(imagePath)
            //     .then(filePath => {
            //       let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            //       let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            //       this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            //     });
            // } else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            // }
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog('Errore img: ' + JSON.stringify(err));
        });
    };
    DocumentiPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.databaseprovider.addDoc(_this.idRiga.id, _this.lastImage, cordova.file.dataDirectory).then(function (result) {
                _this.listaDocs.push(result);
                _this.loadTitoli();
            });
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog('Errore salvataggio: ' + JSON.stringify(err));
        });
    };
    DocumentiPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    DocumentiPage.prototype.changeTitolo = function (doc) {
        var index = this.listaDocs.indexOf(doc);
        var value = this.listaTitoliDoc[index];
        this.listaDocs[index].titolo = value;
        __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog("name value: " + this.listaDocs[index].titolo + " " + this.listaDocs[index].id);
        this.databaseprovider.updateDoc(doc).then(function () {
            __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog("Update success");
        });
    };
    DocumentiPage.prototype.presentDelete = function (doc) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Sei sicuro di voler eliminare questo documento ?',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Si',
                    role: 'destructive',
                    handler: function () {
                        _this.delete(doc);
                    }
                },
                {
                    text: 'no',
                    handler: function () { }
                }
            ]
        });
        alert.present();
    };
    DocumentiPage.prototype.delete = function (doc) {
        var _this = this;
        var nameImg = doc.path.substr(doc.path.lastIndexOf('/') + 1);
        var pathImg = doc.path.substr(0, doc.path.lastIndexOf('/') + 1);
        // Utils.writeLog("Name: " + nameImg + ", path: " + pathImg + ", tot: " + doc.path);
        this.file.removeFile(pathImg, nameImg).then(function (success) {
            if (success) {
                _this.databaseprovider.deleteDoc(doc.id).then(function () {
                    __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog("Delete doc " + doc.id);
                    var index = _this.listaDocs.indexOf(doc);
                    _this.listaDocs.splice(index, 1);
                    _this.loadTitoli();
                });
            }
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog("Error del: " + JSON.stringify(err));
        });
    };
    DocumentiPage.prototype.loadTitoli = function () {
        var _this = this;
        this.listaTitoliDoc = [];
        this.listaDocs.forEach(function (doc) {
            _this.listaTitoliDoc.push(doc.titolo);
        });
    };
    DocumentiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
            selector: 'page-documenti',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/documenti/documenti.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Documenti</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-end solid (click)="takePhoto()">\n        Aggiungi\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-row fixed>\n    <ion-col col-12 col-md-6 col-lg-4 *ngFor="let doc of listaDocs">\n      <ion-card text-wrap>\n        <ion-item text-wrap>\n          <button ion-button item-left solid>\n            Mostra\n          </button>\n          <button ion-button icon-only item-right clear (click)="presentDelete(doc)">\n            <ion-icon name="close" color="danger"></ion-icon>\n          </button>\n        </ion-item>\n        <ion-card-content>\n          <!-- ID: {{ doc.id }} -->\n          Documento n° {{ listaDocs.indexOf(doc) + 1 }}: <ion-input \n                                                          [(ngModel)]="listaTitoliDoc[listaDocs.indexOf(doc)]"\n                                                          type="text" (ionChange)="changeTitolo(doc)"></ion-input>\n          <!-- Path: {{ doc.path }} -->\n        </ion-card-content> \n      </ion-card>\n    </ion-col>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/documenti/documenti.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["AlertController"]])
    ], DocumentiPage);
    return DocumentiPage;
}());

//# sourceMappingURL=documenti.js.map

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__class_documento__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__class_rigaRapportino__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__class_commessa__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__class_campiSetting__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__class_contatto__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_sqlite_porter__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_map__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_storage__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__class_utente__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__class_rapportino__ = __webpack_require__(462);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(sqlitePorter, storage, sqlite, platform, http) {
        var _this = this;
        this.sqlitePorter = sqlitePorter;
        this.storage = storage;
        this.sqlite = sqlite;
        this.platform = platform;
        this.http = http;
        this.databaseReady = new __WEBPACK_IMPORTED_MODULE_12_rxjs__["a" /* BehaviorSubject */](false);
        this.platform.ready().then(function () {
            _this.sqlite.create({
                name: 'nexRapportino.db',
                location: 'default'
            }).then(function (db) {
                _this.database = db;
                _this.storage.get('database_created').then(function (val) {
                    if (val) {
                        _this.databaseReady.next(true);
                    }
                    else {
                        _this.fillDatabase('assets/dump.sql');
                    }
                });
            });
        });
    }
    DatabaseProvider.prototype.fillDatabase = function (fileName) {
        var _this = this;
        this.http.get(fileName)
            .map(function (res) { return res.text(); })
            .subscribe(function (sql) {
            _this.sqlitePorter.importSqlToDb(_this.database, sql)
                .then(function (data) {
                _this.databaseReady.next(true);
                _this.storage.set('database_created', true);
                _this.storage.set('includi_persone', false);
                _this.storage.set('email_to_send', '');
            })
                .catch(function (e) { return console.error(e); });
        });
    };
    DatabaseProvider.prototype.getUser = function (username, password) {
        var query = 'SELECT * FROM utente';
        var params = [];
        if (username != '' && password != '') {
            query += ' utente WHERE username = ? AND password = ?';
            params = [username, password];
        }
        return this.database.executeSql(query, params).then(function (data) {
            var users = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var user = new __WEBPACK_IMPORTED_MODULE_15__class_utente__["a" /* Utente */](data.rows.item(i).id, data.rows.item(i).username);
                    users.push(user);
                }
            }
            return users;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return [];
        });
    };
    DatabaseProvider.prototype.getDatabaseState = function () {
        return this.databaseReady.asObservable();
    };
    DatabaseProvider.prototype.getContatti = function () {
        var query = 'SELECT * FROM contatto ORDER BY id DESC';
        var params = [];
        return this.database.executeSql(query, params).then(function (data) {
            var contatti = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var contatto = new __WEBPACK_IMPORTED_MODULE_5__class_contatto__["a" /* Contatto */](data.rows.item(i).id, data.rows.item(i).id_company, data.rows.item(i).type, data.rows.item(i).cognome_nome, data.rows.item(i).telefono, data.rows.item(i).email, data.rows.item(i).p_iva, data.rows.item(i).cf, data.rows.item(i).sede_legale, data.rows.item(i).sede_operativa, data.rows.item(i).max_ore, data.rows.item(i).tariffa_ore, data.rows.item(i).tariffa_giornata, data.rows.item(i).tariffa_km);
                    contatti.push(contatto);
                }
            }
            return contatti;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return [];
        });
    };
    DatabaseProvider.prototype.getContattiAssociati = function (idCompany) {
        var query = 'SELECT * FROM contatto WHERE id_company = ?';
        var params = [idCompany];
        return this.database.executeSql(query, params).then(function (data) {
            var contatti = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var contatto = new __WEBPACK_IMPORTED_MODULE_5__class_contatto__["a" /* Contatto */](data.rows.item(i).id, data.rows.item(i).id_company, data.rows.item(i).type, data.rows.item(i).cognome_nome, data.rows.item(i).telefono, data.rows.item(i).email, data.rows.item(i).p_iva, data.rows.item(i).cf, data.rows.item(i).sede_legale, data.rows.item(i).sede_operativa, data.rows.item(i).max_ore, data.rows.item(i).tariffa_ore, data.rows.item(i).tariffa_giornata, data.rows.item(i).tariffa_km);
                    contatti.push(contatto);
                }
            }
            return contatti;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return [];
        });
    };
    DatabaseProvider.prototype.addContatto = function (idCompany) {
        var query = 'INSERT INTO contatto(id_company,type,cognome_nome,telefono,email,p_iva,cf,sede_legale,sede_operativa,username_creazione,username_modifica)' +
            'VALUES (?,?,?,?,?,?,?,?,?,?,?)';
        var params = [idCompany, 'Azienda', '', '', '', '', '', '', '', __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername(), __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername()];
        return this.database.executeSql(query, params).then(function (result) {
            var contatto = new __WEBPACK_IMPORTED_MODULE_5__class_contatto__["a" /* Contatto */](result.insertId, idCompany, 'Azienda', '', '', '', '', '', '', '', 0, 0, 0, 0);
            return contatto;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.deleteContatto = function (idContatto) {
        var query = 'DELETE FROM contatto WHERE id = ?';
        var params = [idContatto];
        return this.database.executeSql(query, params).then(function () { }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.updateContatto = function (contatto) {
        var query = 'UPDATE contatto SET type = ?, cognome_nome = ?,telefono = ?,email = ?,p_iva = ?,cf = ?,sede_legale = ?,' +
            'sede_operativa = ?, max_ore = ?, tariffa_ore = ?, tariffa_giornata = ?, tariffa_km = ?, ' +
            'username_modifica = ?, data_modifica = CURRENT_TIMESTAMP ' +
            'WHERE id = ?';
        var params = [contatto.getType(), contatto.getNomeCognome(), contatto.getTelefono(), contatto.getEmail(), contatto.getPIva(),
            contatto.getCF(), contatto.getSedeLegale(), contatto.getSedeOperativa(), contatto.getMaxOre(), contatto.getTariffaOre(),
            contatto.getTariffaGiornata(), contatto.getTariffaKm(), __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername(), contatto.getID()];
        return this.database.executeSql(query, params).then(function () { }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err.toString());
            return null;
        });
    };
    DatabaseProvider.prototype.getCampiSettings = function () {
        var query = 'SELECT * FROM settingCampo';
        var params = [];
        return this.database.executeSql(query, params).then(function (data) {
            var campiLibero = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var campoLibero = new __WEBPACK_IMPORTED_MODULE_4__class_campiSetting__["a" /* CampiSetting */](data.rows.item(i).id, data.rows.item(i).id_campo, data.rows.item(i).attivo, data.rows.item(i).name, data.rows.item(i).value);
                    campiLibero.push(campoLibero);
                }
            }
            return campiLibero;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return [];
        });
    };
    DatabaseProvider.prototype.addCampo = function (idCampo) {
        var query = 'INSERT INTO settingCampo(id_campo, name, value) VALUES(?, ?, ?)';
        var params = [idCampo, '', 'default'];
        return this.database.executeSql(query, params).then(function (result) {
            var campo = new __WEBPACK_IMPORTED_MODULE_4__class_campiSetting__["a" /* CampiSetting */](result.insertId, idCampo, true, '', 'default');
            __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.push(campo);
            return campo;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.updateCampo = function (campo) {
        var query = 'UPDATE settingCampo SET id_campo = ?, attivo = ?, name = ?, value = ? WHERE id = ?';
        var params = [campo.getIDCampo(), campo.getAttivo(), campo.getName(), campo.getValue(), campo.getID()];
        return this.database.executeSql(query, params).then(function () { }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.removeCampo = function (option) {
        var query = 'DELETE FROM settingCampo WHERE id = ?';
        var params = [option.getID()];
        return this.database.executeSql(query, params).then(function (result) {
            var index = __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.indexOf(option);
            __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.splice(index, 1);
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.getCommesse = function () {
        var query = 'SELECT * FROM commessa';
        var params = [];
        return this.database.executeSql(query, params).then(function (data) {
            var listaCommesse = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var commessa = new __WEBPACK_IMPORTED_MODULE_2__class_commessa__["a" /* Commessa */](data.rows.item(i).id, data.rows.item(i).numero, data.rows.item(i).id_cliente, data.rows.item(i).id_lavorato, [data.rows.item(i).libero1.toString(), data.rows.item(i).libero2.toString(),
                        data.rows.item(i).libero3.toString(), data.rows.item(i).libero4.toString()]);
                    listaCommesse.push(commessa);
                }
            }
            return listaCommesse;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return [];
        });
    };
    DatabaseProvider.prototype.addCommessa = function () {
        var query = 'INSERT INTO commessa(username_creazione, username_modifica) VALUES (?, ?)';
        var params = [__WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername(), __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername()];
        return this.database.executeSql(query, params).then(function (result) {
            var l = [];
            var commessa = new __WEBPACK_IMPORTED_MODULE_2__class_commessa__["a" /* Commessa */](result.insertId, '', 0, 0, l);
            return commessa;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.updateCommessa = function (commessa) {
        var query = 'UPDATE commessa SET numero = ?, id_cliente = ?, id_lavorato = ?, username_modifica = ?, data_modifica = CURRENT_TIMESTAMP, libero1 = ?, libero2 = ?, libero3 = ?, libero4 = ?' +
            'WHERE id = ?';
        var params = [commessa.getNumero(), commessa.getIdCliente(), commessa.getIdLavorato(), __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername()];
        for (var i = 0; i < 4; i++) {
            var v = commessa.getLibero()[i] != undefined ? commessa.getLibero()[i] : '';
            params.push(v);
        }
        params.push(commessa.getID());
        return this.database.executeSql(query, params).then(function (result) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(result.toString());
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.deleteCommessa = function (idCommesa) {
        var query = 'DELETE FROM commessa WHERE id = ?';
        var params = [idCommesa];
        return this.database.executeSql(query, params).then(function () { }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.getRapportini = function () {
        var query = 'SELECT * FROM rapportino';
        var params = [];
        return this.database.executeSql(query, params).then(function (data) {
            var listaRapportini = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var rapportino = new __WEBPACK_IMPORTED_MODULE_16__class_rapportino__["a" /* Rapportino */](data.rows.item(i).id, data.rows.item(i).nomeRapportino);
                    listaRapportini.push(rapportino);
                }
            }
            return listaRapportini;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return [];
        });
    };
    DatabaseProvider.prototype.addRapportino = function () {
        var query = 'INSERT INTO rapportino(username_creazione, username_modifica) VALUES (?, ?)';
        var params = [__WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername(), __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername()];
        return this.database.executeSql(query, params).then(function (result) {
            var rapportino = new __WEBPACK_IMPORTED_MODULE_16__class_rapportino__["a" /* Rapportino */](result.insertId, '');
            return rapportino;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.updateRapportino = function (rapportino) {
        var query = 'UPDATE rapportino SET nomeRapportino = ?, username_modifica = ?, data_modifica = CURRENT_TIMESTAMP WHERE id = ?';
        var params = [rapportino.nome, __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername(), rapportino.id];
        __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog("nomeRapportino: " + rapportino.nome + " " + rapportino.id);
        return this.database.executeSql(query, params).then(function (result) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(result.toString());
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.deleteRapportino = function (idRapportino) {
        var _this = this;
        var query = 'PRAGMA foreign_keys = on;';
        var query2 = 'DELETE FROM rapportino WHERE id = ?;';
        var params = [idRapportino];
        return this.database.executeSql(query, []).then(function () {
            return _this.database.executeSql(query2, params).then(function () { }, function (err) {
                __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
                return null;
            });
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.addRigaRapportino = function (idRapportino, idCommessa) {
        var query = "INSERT INTO rigaRapportino(id_rapportino, id_commessa) VALUES(?, ?);";
        var params = [idRapportino, idCommessa];
        return this.database.executeSql(query, params).then(function (result) {
            var rigaRapportino = new __WEBPACK_IMPORTED_MODULE_1__class_rigaRapportino__["a" /* RigaRapportino */](result.insertId, idRapportino, idCommessa, new Date().toDateString(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            return rigaRapportino;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.getRigaRapportino = function (idRapportino) {
        var query = 'SELECT * FROM RigaRapportino WHERE id_rapportino = ?';
        var params = [idRapportino];
        return this.database.executeSql(query, params).then(function (data) {
            var listaRigaRapportini = [];
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var rigaRapportino = new __WEBPACK_IMPORTED_MODULE_1__class_rigaRapportino__["a" /* RigaRapportino */](data.rows.item(i).id, data.rows.item(i).id_rapportino, data.rows.item(i).id_commessa, new Date(data.rows.item(i).data).toDateString(), data.rows.item(i).tot_ore_viaggio, data.rows.item(i).tot_ore_lavoro, data.rows.item(i).tot_km, data.rows.item(i).pranzo_art15, data.rows.item(i).cena_art15, data.rows.item(i).hotel_art15, data.rows.item(i).altro_art15, data.rows.item(i).pranzo, data.rows.item(i).cena, data.rows.item(i).hotel, data.rows.item(i).altro);
                    listaRigaRapportini.push(rigaRapportino);
                }
            }
            return listaRigaRapportini;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return [];
        });
    };
    DatabaseProvider.prototype.deleteRigaRapportino = function (idRiga) {
        var query = 'DELETE FROM rigaRapportino WHERE id = ?;';
        var params = [idRiga];
        return this.database.executeSql(query, params).then(function () { }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.updateRigaRapportino = function (nomeCampo, value, idRigaRapportino) {
        var valueInt;
        if (nomeCampo.toLowerCase() != "data") {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog("Nome Campo: " + nomeCampo);
            if (value.trim() != "") {
                valueInt = parseFloat(value);
            }
            else {
                valueInt = 0;
            }
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog("Value: " + valueInt);
        }
        else {
            valueInt = value;
        }
        var query = 'UPDATE rigaRapportino SET ' + nomeCampo + ' = ? WHERE id = ?';
        var params = [valueInt, idRigaRapportino];
        return this.database.executeSql(query, params).then(function (result) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(result.toString());
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider.prototype.getDocs = function (idRigaRapportino) {
        var query = 'SELECT * FROM documenti WHERE id_riga_rapportino = ?';
        var params = [idRigaRapportino];
        return this.database.executeSql(query, params).then(function (data) {
            var listaDocs = [];
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog("RIGHE DOC: " + data.rows.length);
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    var doc = new __WEBPACK_IMPORTED_MODULE_0__class_documento__["a" /* Documento */](data.rows.item(i).id, data.rows.item(i).id_riga_rapportino, data.rows.item(i).titolo, data.rows.item(i).path);
                    listaDocs.push(doc);
                }
            }
            return listaDocs;
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(JSON.stringify(err));
            return [];
        });
    };
    DatabaseProvider.prototype.addDoc = function (idRigaRapportino, titolo, path) {
        var query = "INSERT INTO documenti(id_riga_rapportino, titolo, path, username_creazione) VALUES(?, ?, ?, ?);";
        var params = [idRigaRapportino, titolo.replace('.jpg', ''), path + titolo, __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].userLogged.getUsername()];
        return this.database.executeSql(query, params).then(function (result) {
            return new __WEBPACK_IMPORTED_MODULE_0__class_documento__["a" /* Documento */](result.insertId, idRigaRapportino, titolo.replace('.jpg', ''), path + titolo);
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(JSON.stringify(err));
            return null;
        });
    };
    DatabaseProvider.prototype.updateDoc = function (doc) {
        var query = 'UPDATE documenti SET titolo = ? WHERE id = ?';
        var params = [doc.titolo, doc.id];
        __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog("nome DOC db: " + doc.titolo + " " + doc.id);
        return this.database.executeSql(query, params).then(function (result) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(JSON.stringify(result));
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(JSON.stringify(err));
            return null;
        });
    };
    DatabaseProvider.prototype.deleteDoc = function (idDoc) {
        var query = 'DELETE FROM documenti WHERE id = ?;';
        var params = [idDoc];
        return this.database.executeSql(query, params).then(function () { }, function (err) {
            __WEBPACK_IMPORTED_MODULE_14__Utils_Utils__["a" /* Utils */].writeLog(err);
            return null;
        });
    };
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__ionic_native_sqlite_porter__["a" /* SQLitePorter */], __WEBPACK_IMPORTED_MODULE_13__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_10__angular_http__["b" /* Http */]])
    ], DatabaseProvider);
    return DatabaseProvider;
}());

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 227:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 227;

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PopoverPage = /** @class */ (function () {
    function PopoverPage(navCtrl, navParams, viewCtrl, databaseprovider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.databaseprovider = databaseprovider;
        this.rigaSelected = this.navParams.get('riga');
        this.loadDati();
    }
    PopoverPage.prototype.deleteRiga = function () {
        this.viewCtrl.dismiss(true);
    };
    PopoverPage.prototype.loadDati = function () {
        this.dati = {
            data: __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getDateTimeISOString(new Date(this.rigaSelected.data)),
            totOreViaggio: this.rigaSelected.totOreViaggio.toString(),
            totOreLavoro: this.rigaSelected.totOreLavoro.toString(),
            totKM: this.rigaSelected.totKM.toString(),
            pranzo_art15: this.rigaSelected.pranzo_art15.toString(),
            cena_art15: this.rigaSelected.cena_art15.toString(),
            hotel_art15: this.rigaSelected.hotel_art15.toString(),
            altro_art15: this.rigaSelected.altro_art15.toString(),
            pranzo: this.rigaSelected.pranzo.toString(),
            cena: this.rigaSelected.cena.toString(),
            hotel: this.rigaSelected.hotel.toString(),
            altro: this.rigaSelected.altro.toString()
        };
        // Utils.writeLog("DATE: " + this.dati.data);
    };
    PopoverPage.prototype.changeValue = function (nomeCampo, rigaRapportino) {
        if (rigaRapportino === void 0) { rigaRapportino = this.rigaSelected; }
        var index = this.navParams.get('index');
        var value = "";
        switch (nomeCampo) {
            case "tot_ore_viaggio":
                value = this.dati.totOreViaggio;
                this.rigaSelected.totOreViaggio = parseInt(value);
                break;
            case "tot_ore_lavoro":
                value = this.dati.totOreLavoro;
                this.rigaSelected.totOreLavoro = parseInt(value);
                break;
            case "tot_km":
                value = this.dati.totKM;
                this.rigaSelected.totKM = parseInt(value);
                break;
            case "pranzo_art15":
                value = this.dati.pranzo_art15;
                this.rigaSelected.pranzo_art15 = parseInt(value);
                break;
            case "cena_art15":
                value = this.dati.cena_art15;
                this.rigaSelected.cena_art15 = parseInt(value);
                break;
            case "hotel_art15":
                value = this.dati.hotel_art15;
                this.rigaSelected.hotel_art15 = parseInt(value);
                break;
            case "altro_art15":
                value = this.dati.altro_art15;
                this.rigaSelected.altro_art15 = parseInt(value);
                break;
            case "pranzo":
                value = this.dati.pranzo;
                this.rigaSelected.pranzo = parseInt(value);
                break;
            case "cena":
                value = this.dati.cena;
                this.rigaSelected.cena = parseInt(value);
                break;
            case "hotel":
                value = this.dati.hotel;
                this.rigaSelected.hotel = parseInt(value);
                break;
            case "altro":
                value = this.dati.altro;
                this.rigaSelected.altro = parseInt(value);
                break;
            case "data":
                var data = this.dati.data;
                value = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getDateString(data);
                this.rigaSelected.data = new Date(value).toDateString();
                rigaRapportino.data = new Date(value).toDateString();
                // Utils.writeLog("Case data: " + value);
                break;
        }
        __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Value " + nomeCampo + ": " + value + " index: " + index);
        this.databaseprovider.updateRigaRapportino(nomeCampo, value, rigaRapportino.id).then(function () {
            __WEBPACK_IMPORTED_MODULE_4__Utils_Utils__["a" /* Utils */].writeLog("Update success");
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-popover',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/popover/popover.html"*/'<ion-list>\n  <ion-list-header>\n    <button ion-button item-right color="danger" (click)="deleteRiga()">Elimina</button>\n  </ion-list-header>\n  <ion-item>\n      <ion-label floating>Data: </ion-label>\n      <ion-datetime (ionChange)="changeValue(\'data\')" displayFormat="DD/MM/YYYY" [(ngModel)]="dati.data"></ion-datetime>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Ore Viaggio: </ion-label>\n    <ion-input  (ionChange)="changeValue(\'tot_ore_viaggio\')" \n                [(ngModel)]="dati.totOreViaggio" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Ore Lavoro: </ion-label>\n    <ion-input  (ionChange)="changeValue(\'tot_ore_lavoro\')" \n                [(ngModel)]="dati.totOreLavoro" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>KM percorsi: </ion-label>\n    <ion-input  (ionChange)="changeValue(\'tot_km\')" \n                [(ngModel)]="dati.totKM" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Pranzo Art.15 (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'pranzo_art15\')" \n                [(ngModel)]="dati.pranzo_art15" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Cena Art.15 (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'cena_art15\')" \n                [(ngModel)]="dati.cena_art15" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Hotel Art.15 (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'hotel_art15\')" \n                [(ngModel)]="dati.hotel_art15" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Altro Art.15 (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'altro_art15\')" \n                [(ngModel)]="dati.altro_art15" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Pranzo (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'pranzo\')" \n                [(ngModel)]="dati.pranzo" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Cena (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'cena\')" \n                [(ngModel)]="dati.cena" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Hotel (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'hotel\')" \n                [(ngModel)]="dati.hotel" \n                type="number">\n    </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Altro (€): </ion-label>\n    <ion-input  (ionChange)="changeValue(\'altro\')" \n                [(ngModel)]="dati.altro" \n                type="number">\n    </ion-input>\n  </ion-item>\n</ion-list>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/popover/popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_0__providers_database_database__["a" /* DatabaseProvider */]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=popover.js.map

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DettagliRapportinoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_file__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__documenti_documenti__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__riepilogo_riepilogo__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__popover_popover__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Utils_UtilsContatti__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var DettagliRapportinoPage = /** @class */ (function () {
    function DettagliRapportinoPage(navCtrl, navParams, databaseprovider, platform, alertCtrl, toastCtrl, popoverCtrl, file) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.popoverCtrl = popoverCtrl;
        this.file = file;
        this.commesseSelected = [];
        this.nomeRapportino = "";
        //rigaRapportino
        this.listaRigaRapportino = [];
        // datiRiga:any;
        this.listaDatiRiga = [];
        this.listaCommesseUsed = [];
        this.listaCommesse = [];
        this.listaCommesseBool = [];
        this.startIndex = 0;
        this.lastIndex = 0;
        this.enabled = true;
        //testo per la ricerca
        this.showSearch = false;
        this.searchText = "";
        this.oldSearchText = "";
        this.searching = false;
        this.rapportinoSelected = navParams.get('rapportino');
        this.nuovoRapportino = navParams.get('nuovo');
        this.showCommesse = this.nuovoRapportino;
        this.nomeRapportino = this.rapportinoSelected.nome;
        __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("SHOW COMMESSE: " + this.showCommesse);
        this.listaCommesseUsed = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse;
        this.loadCommesse();
        __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Commesse: " + this.listaCommesse.length);
        if (!this.nuovoRapportino) {
            // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
            this.databaseprovider.getRigaRapportino(this.rapportinoSelected.id).then(function (result) {
                var tmp = result;
                _this.listaRigaRapportino = __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__["a" /* UtilsRapportini */].sortByDate(tmp);
                _this.loadDati();
            });
        }
        platform.registerBackButtonAction(function () {
            __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Back pressed");
            _this.backButtonClick();
        });
    }
    DettagliRapportinoPage.prototype.backButtonClick = function () {
        if (this.nuovoRapportino) {
            if (this.nomeRapportino.trim() != "" && this.commesseSelected.length > 0) {
                this.presentSave();
            }
            else {
                this.presentDelete();
            }
        }
        else {
            this.navCtrl.pop();
        }
    };
    DettagliRapportinoPage.prototype.changeName = function () {
        if (!this.nuovoRapportino) {
            var index = __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini.indexOf(this.rapportinoSelected);
            var value = this.nomeRapportino;
            this.rapportinoSelected.nome = value;
            __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("name value: " + this.rapportinoSelected.nome + " " + this.rapportinoSelected.id);
            __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini[index].nome = value;
            this.databaseprovider.updateRapportino(this.rapportinoSelected).then(function () {
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Update success");
            });
        }
    };
    DettagliRapportinoPage.prototype.presentSave = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            //title: 'Attenzione',
            title: 'Sono state apportate delle modifiche',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Salva Modifiche',
                    role: 'destructive',
                    handler: function () {
                        _this.save();
                    }
                },
                {
                    text: 'Continua modifiche',
                    handler: function () { }
                },
                {
                    text: 'Annulla Modifiche',
                    handler: function () {
                        if (_this.nuovoRapportino) {
                            _this.deleteFromDB(_this.rapportinoSelected.id);
                        }
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    DettagliRapportinoPage.prototype.save = function () {
        var _this = this;
        if (this.nomeRapportino.trim() != "") {
            if (this.commesseSelected.length > 0) {
                this.rapportinoSelected.nome = this.nomeRapportino;
                if (this.nuovoRapportino) {
                    __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini.unshift(this.rapportinoSelected);
                }
                this.databaseprovider.updateRapportino(this.rapportinoSelected).then(function () {
                    //aggiungere righe rapportini tramite le commesse
                    _this.commesseSelected.forEach(function (commessa) {
                        _this.databaseprovider.addRigaRapportino(_this.rapportinoSelected.id, commessa.getID()).then(function (result) {
                            __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Riga Aggiunta");
                            _this.listaCommesse.forEach(function (commessa) {
                                commessa.restoreCount();
                            });
                            _this.listaRigaRapportino.push(result);
                            var tmp = __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__["a" /* UtilsRapportini */].sortByDate(_this.listaRigaRapportino);
                            _this.listaRigaRapportino = tmp;
                            _this.listaRigaRapportino.forEach(function (riga) {
                                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Riga: " + riga.id + " data: " + riga.data);
                            });
                            _this.loadDati();
                            _this.nuovoRapportino = false;
                        });
                    });
                    // this.loadDati();
                    _this.commesseSelected = [];
                });
                if (this.showCommesse) {
                    this.showCommesse = false;
                }
                else {
                    this.navCtrl.pop();
                }
            }
            else {
                if (this.showCommesse) {
                    __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].presentToast("Dati non compilati correttamente, seleziona almeno una commessa", this.toastCtrl);
                }
            }
        }
        else {
            if (this.showCommesse) {
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].presentToast("Dati non compilati correttamente, scrivi un nome per il rapportino", this.toastCtrl);
            }
            __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Dati non compilati correttamente");
        }
    };
    DettagliRapportinoPage.prototype.presentDelete = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            //title: 'Attenzione',
            title: 'Sei sicuro di voler uscire ? Questa rapportino non sarà salvata',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Rimani',
                    role: 'destructive',
                    handler: function () { }
                },
                {
                    text: 'Esci',
                    handler: function () {
                        _this.deleteFromDB(_this.rapportinoSelected.id);
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    DettagliRapportinoPage.prototype.deleteFromDB = function (id) {
        this.databaseprovider.deleteRapportino(id).then(function () {
            // CommessePage.listChanges = true;
        });
    };
    DettagliRapportinoPage.prototype.cancel = function () {
        if (this.nuovoRapportino) {
            this.presentDelete();
        }
        else {
            this.showCommesse = false;
        }
        this.listaCommesse.forEach(function (commessa) {
            commessa.restoreCount();
        });
        this.commesseSelected = [];
    };
    //lista commesse
    DettagliRapportinoPage.prototype.doInfinite = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                if (_this.listaCommesse.length < _this.listaCommesseUsed.length) {
                    _this.loadCommesse();
                }
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Commesse caricate visibili: " + _this.listaCommesse.length);
                // this.toTopButtonVisible = true;
                _this.searching = false;
                resolve(_this.listaCommesse);
            }, 700);
        });
    };
    DettagliRapportinoPage.prototype.loadCommesse = function () {
        if (this.listaCommesseUsed.length > 0) {
            this.lastIndex = this.listaCommesseUsed.length;
            if (this.lastIndex > (20 + this.startIndex)) {
                this.lastIndex = 20 + this.startIndex;
            }
            for (var i = this.startIndex; i < this.lastIndex; i++) {
                this.listaCommesse.push(this.listaCommesseUsed[i]);
            }
        }
        this.startIndex = this.lastIndex;
        if (this.listaCommesse.length == this.listaCommesseUsed.length) {
            this.enabled = false;
        }
        this.searching = false;
    };
    DettagliRapportinoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.navbar.backButtonClick = function () {
            _this.backButtonClick();
        };
        if (!this.nuovoRapportino) {
            // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
        }
    };
    DettagliRapportinoPage.prototype.addCommesse = function () {
        this.showCommesse = true;
        // this.screenOrientation.unlock();
    };
    DettagliRapportinoPage.prototype.add = function (commessa) {
        commessa.addCount();
        this.commesseSelected.push(commessa);
    };
    DettagliRapportinoPage.prototype.remove = function (commessa) {
        if (commessa.getCount() > 0) {
            commessa.removeCount();
            var index = this.listaCommesse.indexOf(commessa);
            this.commesseSelected.splice(index, 1);
        }
    };
    DettagliRapportinoPage.prototype.ionViewWillLeave = function () {
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
        // this.screenOrientation.unlock();
        this.listaCommesse.forEach(function (commessa) {
            commessa.restoreCount();
        });
        this.commesseSelected = [];
    };
    DettagliRapportinoPage.prototype.getNCommessa = function (idCommessa) {
        var commessa = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.filter(function (c) {
            return c.getID() == idCommessa;
        })[0];
        return commessa.getNumero().toString();
    };
    DettagliRapportinoPage.prototype.getClienteCommessa = function (idCommessa) {
        var commessa = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.filter(function (c) {
            return c.getID() == idCommessa;
        })[0];
        var cliente = __WEBPACK_IMPORTED_MODULE_4__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.filter(function (c) {
            return c.getID() == commessa.getIdCliente();
        })[0];
        return cliente.getNomeCognome().toString();
    };
    DettagliRapportinoPage.prototype.getDataCommessa = function (riga) {
        return __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getDateIt(riga.data);
    };
    DettagliRapportinoPage.prototype.loadDati = function () {
        var _this = this;
        this.listaDatiRiga = [];
        this.listaRigaRapportino.forEach(function (rr) {
            var dati = {
                data: _this.getDataCommessa(rr)
            };
            _this.listaDatiRiga.push(dati);
            // Utils.writeLog("DATE: " + dati.data);
        });
    };
    DettagliRapportinoPage.prototype.openRigaOption = function (Event, riga) {
        var _this = this;
        var index = this.listaRigaRapportino.indexOf(riga);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__popover_popover__["a" /* PopoverPage */], { riga: riga, index: index });
        popover.onDidDismiss(function (data) {
            __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Popover Dismissed");
            if (data) {
                _this.presentDeleteRiga(riga, index);
            }
            // this.listaDatiRiga[index].data = this.getDataCommessa(riga);
            var tmp = __WEBPACK_IMPORTED_MODULE_6__Utils_UtilsRapportini__["a" /* UtilsRapportini */].sortByDate(_this.listaRigaRapportino);
            _this.listaRigaRapportino = tmp;
            _this.loadDati();
        });
        popover.present({
            ev: event
        });
    };
    DettagliRapportinoPage.prototype.creaFattura = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__riepilogo_riepilogo__["a" /* RiepilogoPage */], {
            righe: this.listaRigaRapportino
        });
    };
    DettagliRapportinoPage.prototype.openDocs = function (riga) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__documenti_documenti__["a" /* DocumentiPage */], {
            riga: riga
        });
    };
    DettagliRapportinoPage.prototype.presentDeleteRiga = function (riga, index) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Sei sicuro di voler eliminare questa riga ? Eliminerai anche i documenti associati',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Elimina',
                    role: 'destructive',
                    handler: function () {
                        _this.deleteRiga(riga, index);
                    }
                },
                {
                    text: 'Annulla',
                    handler: function () { }
                }
            ]
        });
        alert.present();
    };
    DettagliRapportinoPage.prototype.deleteRiga = function (riga, index) {
        var _this = this;
        this.listaRigaRapportino.splice(index, 1);
        this.databaseprovider.getDocs(riga.id).then(function (data) {
            var listaDoc = data;
            listaDoc.forEach(function (doc) {
                var nameImg = doc.path.substr(doc.path.lastIndexOf('/') + 1);
                var pathImg = doc.path.substr(0, doc.path.lastIndexOf('/') + 1);
                _this.file.removeFile(pathImg, nameImg).then(function (success) {
                    if (success) {
                        _this.databaseprovider.deleteDoc(doc.id).then(function () {
                            __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Delete doc " + doc.id);
                        });
                    }
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Error del: " + JSON.stringify(err));
                });
            });
        });
        this.databaseprovider.deleteRigaRapportino(riga.id).then(function () {
            __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Riga " + riga.id + " eliminata");
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["Navbar"])
    ], DettagliRapportinoPage.prototype, "navbar", void 0);
    DettagliRapportinoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["Component"])({
            selector: 'page-dettagli-rapportino',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/dettagli-rapportino/dettagli-rapportino.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ nomeRapportino }}</ion-title>\n    <ion-buttons end>\n      <div *ngIf="listaRigaRapportino?.length > 0">\n        <button ion-button icon-end solid (click)="creaFattura()" color="secondary">\n          Riepilogo\n          <ion-icon name="create"></ion-icon>\n        </button>\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-toolbar>\n    <ion-buttons end>\n      <div *ngIf="showCommesse; then crea else aggiungi"></div>\n      <ng-template #crea>\n        <button ion-button icon-end solid (click)="cancel()" color="danger">\n          Annulla\n          <ion-icon name="close"></ion-icon>\n        </button>\n        <button ion-button icon-end solid (click)="save()" color="secondary">\n          Crea\n          <ion-icon name="checkmark-circle-outline"></ion-icon>\n        </button>\n      </ng-template>\n      <ng-template #aggiungi>\n        <button ion-button icon-end solid (click)="addCommesse()" color="secondary">\n          Aggiungi\n          <ion-icon name="add-circle"></ion-icon>\n        </button>\n      </ng-template>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-row fixed class="background-light" padding-bottom>\n    <ion-col col-1></ion-col>\n    <ion-col col-10 ion-item class="background-light">\n      <ion-label floating>Nome Rapportino</ion-label>\n      <ion-input id="nome" type="text" [(ngModel)]="nomeRapportino" (ionChange)="changeName()"></ion-input>\n    </ion-col>\n    <ion-col col-1></ion-col>\n  </ion-row>\n\n  <div *ngIf="showCommesse; then new else old"></div>\n  <ng-template #new>\n    <div *ngIf="listaCommesse?.length > 0; then commesse else nocommesse"></div>\n  </ng-template>\n  <ng-template #old>\n    <div ion-grid no-padding>\n      <div ion-row>\n        <div ion-col col-12 col-md-6 col-lg-4 *ngFor="let riga of listaRigaRapportino">\n          <ion-card>\n            <ion-item text-wrap>\n              <button ion-button icon-only item-right clear (click)="openRigaOption($event, riga)">\n                <ion-icon name="settings"></ion-icon>\n              </button>\n              {{ getNCommessa(riga.idCommessa) }} - {{ getClienteCommessa(riga.idCommessa) }}\n            </ion-item>\n            <!-- <ion-card-content> -->\n              <!-- Data: {{ getDataCommessa(riga) }} -->\n              <ion-item text-wrap item-left>\n                <!-- Data: {{ getDataCommessa(riga) }} -->\n                Data: {{ listaDatiRiga[listaRigaRapportino.indexOf(riga)].data }}\n                <!-- <ion-label floating>Data: </ion-label>\n                <ion-datetime (ionChange)="changeValue(\'data\', riga)" displayFormat="DD/MM/YYYY" [(ngModel)]="listaDatiRiga[listaRigaRapportino.indexOf(riga)].data"></ion-datetime> -->\n                <ion-icon item-right ios="ios-document-outline" md="md-document" (click)="openDocs(riga)"></ion-icon>\n              </ion-item>\n            <!-- </ion-card-content> -->\n          </ion-card>\n        </div>\n      </div>\n    </div>\n    <!-- <ion-list>\n      <ion-item-group *ngFor="let riga of listaRigaRapportino">\n        <ion-item>\n          <ion-label>{{ getNCommessa(riga.idCommessa) }} - {{ getClienteCommessa(riga.idCommessa) }}</ion-label>\n        </ion-item>\n        <ion-item>\n          <ion-label>Data: </ion-label>\n          <ion-datetime (ionChange)="changeValue(\'data\', riga)" displayFormat="DD/MM/YYYY" [(ngModel)]="listaDatiRiga[listaRigaRapportino.indexOf(riga)].data"></ion-datetime>\n        </ion-item>\n        <ion-icon item-center item-end name="more" color="green"></ion-icon>\n      </ion-item-group>\n    </ion-list> -->\n  </ng-template>\n\n  <ng-template #commesse>\n    <h3 padding-left>Elenco Commesse</h3>\n    <p padding-left>Per ogni commessa seleziona il numero di giorni che hai lavorato.</p>\n    <ion-list inset>\n      <ion-item #item *ngFor="let commessa of listaCommesse">\n        <ion-label item-start>{{ commessa.getNumero() }}</ion-label>\n        <button item-end ion-button (click)="remove(commessa)" color="danger">\n          <ion-icon name="remove"></ion-icon>\n        </button>\n        <h4 item-end padding-top padding-bottom>{{ commessa.getCount() }}</h4>\n        <button item-end ion-button (click)="add(commessa)" color="green">\n          <ion-icon name="add"></ion-icon>\n        </button>\n      </ion-item>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfinite())" [enabled]="enabled">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ng-template>\n  <ng-template #nocommesse>\n    <h5 padding-left>Non sono presenti commesse</h5>\n  </ng-template>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/dettagli-rapportino/dettagli-rapportino.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_7__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["PopoverController"], __WEBPACK_IMPORTED_MODULE_0__ionic_native_file__["a" /* File */]])
    ], DettagliRapportinoPage);
    return DettagliRapportinoPage;
}());

//# sourceMappingURL=dettagli-rapportino.js.map

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiepilogoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsContatti__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsCommessa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__class_excel__ = __webpack_require__(575);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__setting_setting__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var RiepilogoPage = /** @class */ (function () {
    function RiepilogoPage(navCtrl, navParams, http, alertCtrl, loadingCtrl, actionSheetCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.righeRapportino = navParams.get("righe");
    }
    RiepilogoPage.prototype.getNCommessa = function (idCommessa) {
        var commessa = __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.filter(function (c) {
            return c.getID() == idCommessa;
        })[0];
        return commessa.getNumero().toString();
    };
    RiepilogoPage.prototype.getCommessa = function (idCommessa) {
        return __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.filter(function (c) {
            return c.getID() == idCommessa;
        })[0];
    };
    RiepilogoPage.prototype.getClienteCommessa = function (idCommessa) {
        var commessa = __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.filter(function (c) {
            return c.getID() == idCommessa;
        })[0];
        var cliente = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.filter(function (c) {
            return c.getID() == commessa.getIdCliente();
        })[0];
        return cliente;
    };
    RiepilogoPage.prototype.getDataCommessa = function (riga) {
        return __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getDateIt(riga.data);
    };
    RiepilogoPage.prototype.createExcel = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].emailToSend.trim() != "") {
            var alert_1 = this.alertCtrl.create({
                title: 'Vuoi creare un riepilogo di questo Rapportino ?',
                buttons: [
                    {
                        text: 'Si',
                        role: 'destructive',
                        handler: function () {
                            _this.excel();
                        }
                    },
                    {
                        text: 'Annulla',
                        role: 'cancel',
                        handler: function () { }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Non hai indicato la mail a cui spedire i riepiloghi, vai nelle impostazioni',
                buttons: [
                    {
                        text: 'VAI',
                        handler: function () {
                            // this.nav.setRoot(SettingPage, {}, {animate: true, direction: 'forward'});
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__setting_setting__["a" /* SettingPage */], {}, { animate: true, direction: 'forward' });
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    RiepilogoPage.prototype.excel = function (righe) {
        if (righe === void 0) { righe = this.righeRapportino; }
        var loading = this.presentLoad();
        var excel = new __WEBPACK_IMPORTED_MODULE_5__class_excel__["a" /* Excel */](righe);
        loading.present();
        var headers = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* Headers */]();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json; charset=UTF-8');
        var requestOptions = new __WEBPACK_IMPORTED_MODULE_8__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var data = {
            rapportino: excel.rapportino,
            riepilogo: excel.riepilogo,
            fattura: excel.fattura,
            nome: __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getRapportino(this.righeRapportino[0].idRapportino).nome,
            email: __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].emailToSend
        };
        var postData = JSON.stringify(data);
        __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].writeLog("POST DATA: " + postData);
        // let api_ip = "http://" + Utils.ipws;
        __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].writeLog("email: " + data.email);
        this.http.post("https://nexrapportino.nexapp.it/apiTest/excel/create.php", postData, requestOptions)
            .subscribe(function (data) {
            __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].writeLog("Excel creato");
            loading.dismiss(true);
            __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].writeLog("Data WS: " + JSON.stringify(data));
        }, function (error) {
            __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].writeLog("ERRORE WS: " + JSON.stringify(error));
            loading.dismiss(false);
        });
        // Utils.writeLog("Excel Header = " + JSON.stringify(excel.nomeColonna));
        // Utils.writeLog("Excel rows = " + JSON.stringify(excel.valueCells));
    };
    RiepilogoPage.prototype.presentLoad = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Attendi...'
        });
        loading.onDidDismiss(function (data) {
            __WEBPACK_IMPORTED_MODULE_6__Utils_Utils__["a" /* Utils */].writeLog("Data dismiss: " + data);
            if (data) {
                _this.presentConfirmAlert(data);
            }
            else {
                _this.presentConfirmAlert(data);
            }
        });
        return loading;
    };
    RiepilogoPage.prototype.presentConfirmAlert = function (data) {
        var text = "C'è stato un errore, riprova";
        if (data) {
            text = "Il riepilogo è stato creato correttamente, riceverai una mail";
        }
        var alert = this.alertCtrl.create({
            //title: 'Attenzione',
            title: text,
            buttons: ['OK']
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Nav"])
    ], RiepilogoPage.prototype, "nav", void 0);
    RiepilogoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
            selector: 'page-riepilogo',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/riepilogo/riepilogo.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Riepilogo</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-end solid color="green" (click)="createExcel()">\n        Estrai Excel\n        <ion-icon ios="ios-download-outline" md="md-download"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid no-padding>\n    <ion-row>\n      <ion-col col-12 col-md-6 col-lg-4 *ngFor="let riga of righeRapportino">\n        <ion-card>\n          <ion-card-title>{{ getNCommessa(riga.idCommessa) }} - {{ getClienteCommessa(riga.idCommessa).getNomeCognome() }}</ion-card-title>\n          <ion-card-content text-wrap>\n            Data: {{ getDataCommessa(riga) }}<br>\n            Totale Ore: {{ riga.getOreViaggioLavoro() }} - Max Ore Rimborsabili: {{ getClienteCommessa(riga.idCommessa).getMaxOre() }}<br>\n            Rimborso Ore: {{ riga.getRimborsoOre(getClienteCommessa(riga.idCommessa)) }} € ({{getClienteCommessa(riga.idCommessa).getTariffaOre()}}€/h)<br>\n            Rimborso km ({{ getClienteCommessa(riga.idCommessa).getTariffaKm() }} €/Km): {{ riga.getTotRimborsoKm(getClienteCommessa(riga.idCommessa).getTariffaKm()) }} €<br>\n            Rimborso Art.15: {{ riga.getRimborsiArt15() }} €<br>\n            Rimborso: {{ riga.getRimborsi() }} €\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/riepilogo/riepilogo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_8__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["ActionSheetController"]])
    ], RiepilogoPage);
    return RiepilogoPage;
}());

//# sourceMappingURL=riepilogo.js.map

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RapportiniPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_file__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__dettagli_rapportino_dettagli_rapportino__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RapportiniPage = /** @class */ (function () {
    function RapportiniPage(navCtrl, navParams, databaseprovider, alertCtrl, toastCtrl, file) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.file = file;
        this.listaRapportini = [];
        this.listaRapportiniUsed = [];
        this.startIndex = 0;
        this.lastIndex = 0;
        this.enabled = true;
        this.showSearch = false;
        this.searchText = "";
        this.oldSearchText = "";
        this.searching = false;
        this.listaRapportiniUsed = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini;
        this.loadRapportini();
        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Totale rapportini: " + __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini.length);
        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Rapportini caricati: " + this.listaRapportini.length);
    }
    RapportiniPage.prototype.nuovo = function () {
        var _this = this;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addRapportino()
                    .then(function (rapportino) {
                    if (rapportino != null) {
                        _this.openRapportino(rapportino, true);
                    }
                });
            }
        });
    };
    RapportiniPage.prototype.loadRapportini = function () {
        if (this.listaRapportiniUsed.length > 0) {
            this.lastIndex = this.listaRapportiniUsed.length;
            if (this.lastIndex > (20 + this.startIndex)) {
                this.lastIndex = 20 + this.startIndex;
            }
            for (var i = this.startIndex; i < this.lastIndex; i++) {
                this.listaRapportini.push(this.listaRapportiniUsed[i]);
            }
        }
        this.startIndex = this.lastIndex;
        if (this.listaRapportini.length == this.listaRapportiniUsed.length) {
            this.enabled = false;
        }
        this.searching = false;
    };
    RapportiniPage.prototype.doInfinite = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                if (_this.listaRapportini.length < _this.listaRapportiniUsed.length) {
                    _this.loadRapportini();
                }
                __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Rapportini caricati visibili: " + _this.listaRapportini.length);
                // this.toTopButtonVisible = true;
                _this.searching = false;
                resolve(_this.listaRapportini);
            }, 700);
        });
    };
    RapportiniPage.prototype.scrollTop = function () {
        this.content.scrollToTop();
    };
    RapportiniPage.prototype.openRapportino = function (rapportino, nuovo) {
        var page = __WEBPACK_IMPORTED_MODULE_6__dettagli_rapportino_dettagli_rapportino__["a" /* DettagliRapportinoPage */];
        // if (nuovo) {
        //   page = CreaRapportinoPage
        // }
        this.navCtrl.push(page, {
            rapportino: rapportino,
            nuovo: nuovo
        });
    };
    RapportiniPage.prototype.openSearch = function () {
        if (!this.showSearch) {
            this.searchText = "";
            this.showSearch = true;
        }
        else {
            this.showSearch = false;
        }
    };
    RapportiniPage.prototype.delete = function (item, rapportino) {
        //item.close()
        var indexGlobal = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini.indexOf(rapportino);
        if (indexGlobal > -1) {
            this.doDelete(indexGlobal, rapportino);
        }
    };
    RapportiniPage.prototype.doDelete = function (index, rapportino) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Attenzione',
            message: 'Sei sicuro di voler eliminare questo rapportino?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () { }
                },
                {
                    text: 'Elimina',
                    handler: function () {
                        _this.deleteRecursive(rapportino);
                        __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].removeListaRapportini(index);
                        _this.searching = true;
                        _this.enabled = true;
                        _this.startIndex = 0;
                        _this.lastIndex = 0;
                        _this.listaRapportini = [];
                        _this.listaRapportiniUsed = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini;
                        if (_this.searchText != "") {
                            _this.listaRapportiniUsed = _this.filterList(_this.searchText);
                        }
                        _this.loadRapportini();
                        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog(rapportino.nome + " Eliminato");
                        _this.databaseprovider.deleteRapportino(rapportino.id);
                    }
                }
            ]
        });
        alert.present();
    };
    RapportiniPage.prototype.deleteRecursive = function (rapportino) {
        var _this = this;
        this.databaseprovider.getRigaRapportino(rapportino.id).then(function (result) {
            var listaRighe = result;
            listaRighe.forEach(function (riga) {
                _this.databaseprovider.getDocs(riga.id).then(function (result) {
                    var listaDocs = result;
                    //elimina docs riga
                    listaDocs.forEach(function (doc) {
                        var nameImg = doc.path.substr(doc.path.lastIndexOf('/') + 1);
                        var pathImg = doc.path.substr(0, doc.path.lastIndexOf('/') + 1);
                        _this.file.removeFile(pathImg, nameImg).then(function (success) {
                            if (success) {
                                _this.databaseprovider.deleteDoc(doc.id).then(function () {
                                    __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Delete doc " + doc.id);
                                });
                            }
                        }, function (err) {
                            __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Error del: " + JSON.stringify(err));
                        });
                    });
                    //elimina riga
                    _this.databaseprovider.deleteRigaRapportino(riga.id).then(function () {
                        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Riga " + riga.id + " eliminata");
                    });
                });
            });
        });
    };
    //search
    RapportiniPage.prototype.resetList = function () {
        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("reset list");
        this.startIndex = 0;
        this.lastIndex = 0;
        this.enabled = true;
        this.listaRapportini = [];
        this.listaRapportiniUsed = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini;
        this.loadRapportini();
    };
    RapportiniPage.prototype.onClearSearchbar = function (ev) {
        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Clear Searchbar");
        this.searching = true;
        this.resetList();
        this.showSearch = false;
    };
    RapportiniPage.prototype.onCancelSearchbar = function (ev) {
        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Cancel Searchbar");
        this.searching = true;
        this.resetList();
        this.showSearch = false;
    };
    RapportiniPage.prototype.changedInput = function (ev) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("text changed");
        this.searching = true;
        this.enabled = true;
        setTimeout(function () {
            _this.searching = false;
            if (_this.searchText != "") {
                var tmpList = _this.filterList(_this.searchText);
                __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Contatti filtrati: " + tmpList.length);
                _this.startIndex = 0;
                _this.lastIndex = 0;
                _this.listaRapportini = [];
                _this.listaRapportiniUsed = tmpList;
                _this.loadRapportini();
                __WEBPACK_IMPORTED_MODULE_5__Utils_Utils__["a" /* Utils */].writeLog("Contatti visibili dopo filtro: " + _this.listaRapportini.length);
            }
            else {
                _this.oldSearchText = "";
                _this.resetList();
            }
        }, 300);
    };
    RapportiniPage.prototype.filterList = function (searchTerm) {
        var listToUse = __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsRapportini__["a" /* UtilsRapportini */].listaRapportini;
        if (searchTerm.length > this.oldSearchText.length) {
            listToUse = this.listaRapportiniUsed;
        }
        this.oldSearchText = searchTerm;
        return listToUse.filter(function (rapportino) {
            return rapportino.nome.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    RapportiniPage.prototype.ionViewDidEnter = function () {
        this.resetList();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Content"]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Content"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Content"]) === "function" && _a || Object)
    ], RapportiniPage.prototype, "content", void 0);
    RapportiniPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
            selector: 'page-rapportini',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/rapportini/rapportini.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Rapportini</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <button ion-item (click)="nuovo()" >\n    <ion-icon name="add-circle" item-start></ion-icon>\n      Nuovo\n    <div class="item-note" item-end></div>\n  </button>\n  <div *ngIf="listaRapportini?.length > 0; then rapportini; else norapportini"></div>\n  <ng-template #rapportini>\n    <ion-list>\n      <ion-item-sliding #item *ngFor="let rapportino of listaRapportini">\n        <button ion-item (click)="openRapportino(rapportino, false)">\n          <ion-icon name="analytics" item-start></ion-icon>\n          {{ rapportino.nome }}\n        </button>\n        <ion-item-options icon-start side="left" (ionSwipe)="delete($event, rapportino)">\n          <button ion-button color="danger" (click)="delete(item, rapportino)" expandable>\n            <ion-icon name="trash"></ion-icon>Elimina\n          </button>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfinite())" [enabled]="enabled">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ng-template>\n  <ion-fab bottom right>\n    <button ion-fab color="primary" (click)="scrollTop()">\n      <ion-icon name="arrow-dropup"></ion-icon>\n    </button>\n  </ion-fab>\n  <ng-template #norapportini>\n    <p>Non sono presenti rapportini</p>\n  </ng-template>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/rapportini/rapportini.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavController"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavParams"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavParams"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["AlertController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["AlertController"]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["ToastController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["ToastController"]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__ionic_native_file__["a" /* File */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__ionic_native_file__["a" /* File */]) === "function" && _g || Object])
    ], RapportiniPage);
    return RapportiniPage;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=rapportini.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DettagliComessaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsCommessa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__commesse_commesse__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dettagli_setting_dettagli_setting__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_select_searchable__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var DettagliComessaPage = /** @class */ (function () {
    function DettagliComessaPage(navCtrl, navParams, platform, databaseprovider, modalCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseprovider = databaseprovider;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.lavoratoEnabled = false;
        this.idToUse = 0;
        this.page = 2;
        //
        this.libero = [];
        this.liberoValue = [];
        this.numero = '';
        this.commessaSelected = navParams.get('commessa');
        this.nuovaCommessa = navParams.get('nuovo');
        this.contattiPrincipali = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociati(0);
        this.totalPages = Math.round(this.contattiPrincipali.length / 20);
        __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Total pages: " + this.totalPages);
        // platform.ready().then(() => {
        if (!this.nuovaCommessa) {
            this.principaleSelected = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.filter(function (c) {
                return c.getID() == _this.commessaSelected.getIdCliente();
            })[0];
            this.associatoSelected = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.filter(function (c) {
                return c.getID() == _this.commessaSelected.getIdLavorato();
            })[0];
            this.numero = this.commessaSelected.getNumero();
        }
        this.libero = this.filterOption();
        for (var i = 0; i < this.libero.length; i++) {
            var v = this.commessaSelected.getLibero()[i] != undefined ? this.commessaSelected.getLibero()[i] : '';
            this.liberoValue.push(v);
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Libero v: " + v);
        }
        __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Campi tot array: " + __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.length);
        __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Libero array: " + this.libero.length);
        // });
        platform.registerBackButtonAction(function () {
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Back pressed");
            _this.backButtonClick();
        });
    }
    DettagliComessaPage.prototype.backButtonClick = function () {
        if (this.principaleSelected == null && this.associatoSelected == null) {
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Principale: " + this.principaleSelected + " associato: " + this.associatoSelected);
            this.presentDelete();
        }
        else {
            if (__WEBPACK_IMPORTED_MODULE_0__Utils_UtilsCommessa__["a" /* UtilsCommessa */].checkChanges(this.commessaSelected, this.principaleSelected, this.associatoSelected, this.numero, this.liberoValue)) {
                this.presentSave();
            }
            else {
                __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Back button -> Nuova commessa ? " + this.nuovaCommessa);
                if (this.nuovaCommessa) {
                    this.presentDelete();
                }
                else {
                    this.navCtrl.pop();
                }
            }
        }
    };
    //Utils cliente e da chi
    DettagliComessaPage.prototype.filterContatti = function (cPrincipali, text) {
        return cPrincipali.filter(function (contatto) {
            return contatto.getNomeCognome().toLowerCase().indexOf(text.toLowerCase()) > -1;
        });
    };
    DettagliComessaPage.prototype.searchContatti = function (event, tipo) {
        var _this = this;
        if (tipo == "selectCliente") {
            this.idToUse = 0;
        }
        else {
            if (this.principaleSelected != null) {
                this.idToUse = this.principaleSelected.getID();
            }
        }
        var text = event.text.trim().toLowerCase();
        event.component.startSearch();
        // Close any running subscription.
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Filtro: " + text);
        if (!text) {
            // Close any running subscription.
            if (this.subscription) {
                this.subscription.unsubscribe();
            }
            event.component.items = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociati(this.idToUse, 1, 20, __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone);
            // Enable and start infinite scroll from the beginning.
            this.page = 2;
            this.totalPages = Math.round(__WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociati(this.idToUse, -1, -1, __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone).length / 20);
            event.component.endSearch();
            event.component.enableInfiniteScroll();
            return;
        }
        // , 1, 20
        this.subscription = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociatiAsync(this.idToUse).subscribe(function (contatti) {
            // Subscription will be closed when unsubscribed manually.
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Subscription status: " + _this.subscription.closed);
            if (_this.subscription.closed) {
                return;
            }
            else {
                event.component.endSearch();
                event.component.enableInfiniteScroll();
            }
            var contattiFilter = _this.filterContatti(contatti, text);
            event.component.items = contattiFilter.slice(0, 20);
            _this.page = 2;
            _this.totalPages = Math.round(contattiFilter.length / 20);
            if (_this.totalPages == 0) {
                _this.totalPages += 1;
            }
            else if (Math.abs((contattiFilter.length - ((_this.totalPages) * 20))) < 10 && Math.abs((contattiFilter.length - ((_this.totalPages) * 20))) > 0) {
                _this.totalPages += 1;
            }
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Total pages: " + _this.totalPages);
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Contatti trovati: " + contattiFilter.length + " per: " + _this.totalPages + " pagine");
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Contatti visibili: " + event.component.items.length);
            event.component.endSearch();
        });
    };
    DettagliComessaPage.prototype.getMoreContatti = function (event) {
        var _this = this;
        var text = (event.text || '').trim().toLowerCase();
        // There're no more contacts - disable infinite scroll.
        if (this.page > this.totalPages) {
            event.component.disableInfiniteScroll();
            return;
        }
        var size = 20;
        if (text && (this.page == this.totalPages)) {
            var list = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociati(this.idToUse);
            if (text) {
                list = this.filterContatti(list, text);
            }
            size = list.length - ((this.page - 1) * size);
        }
        __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Elementi da aggiungere: " + size);
        __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociatiAsync(this.idToUse, this.page, size).subscribe(function (contatti) {
            contatti = event.component.items.concat(contatti);
            // if (text) {
            //   contatti = this.filterContatti(contatti, text);
            // }
            event.component.items = contatti;
            event.component.endInfiniteScroll();
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Pagina n: " + _this.page);
            _this.page++;
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Contatti visibili: " + event.component.items.length);
        });
    };
    DettagliComessaPage.prototype.changeCliente = function (event, tipo) {
        if (tipo == "selectCliente") {
            this.principaleSelected = event.value;
            this.contattiAssociati = __WEBPACK_IMPORTED_MODULE_5__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociati(this.principaleSelected.getID(), -1, -1, __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone);
            this.associatoSelected = null;
            this.lavoratoEnabled = true;
        }
        else {
            this.associatoSelected = event.value;
        }
    };
    //
    DettagliComessaPage.prototype.filterOption = function () {
        return __WEBPACK_IMPORTED_MODULE_3__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.filter(function (campo) {
            if (campo.getIDCampo() == 0) {
                if (campo.getAttivo().toString().toLowerCase() == "true") {
                    return true;
                }
            }
            return false;
        });
    };
    DettagliComessaPage.prototype.scegliOpzione = function (l) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__dettagli_setting_dettagli_setting__["a" /* DettagliSettingPage */], {
            setting: l,
            modal: true
        });
        modal.onDidDismiss(function (value) {
            _this.liberoValue[_this.libero.indexOf(l)] = value;
        });
        modal.present();
    };
    DettagliComessaPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Commessa Selezionata: " + this.commessaSelected.getID() + " " + this.commessaSelected.getNumero() + " " + this.nuovaCommessa);
        this.navbar.backButtonClick = function () {
            _this.backButtonClick();
        };
    };
    DettagliComessaPage.prototype.ionViewWillLeave = function () {
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    };
    DettagliComessaPage.prototype.presentSave = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            //title: 'Attenzione',
            title: 'Sono state apportate delle modifiche',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Salva Modifiche',
                    role: 'destructive',
                    handler: function () {
                        _this.save();
                    }
                },
                {
                    text: 'Continua modifiche',
                    handler: function () { }
                },
                {
                    text: 'Annulla Modifiche',
                    handler: function () {
                        if (_this.nuovaCommessa) {
                            _this.deleteFromDB(_this.commessaSelected.getID());
                        }
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    DettagliComessaPage.prototype.save = function () {
        if (__WEBPACK_IMPORTED_MODULE_0__Utils_UtilsCommessa__["a" /* UtilsCommessa */].checkCommessa(this.principaleSelected, this.associatoSelected, this.numero, this.liberoValue)) {
            if (__WEBPACK_IMPORTED_MODULE_0__Utils_UtilsCommessa__["a" /* UtilsCommessa */].checkChanges(this.commessaSelected, this.principaleSelected, this.associatoSelected, this.numero, this.liberoValue)) {
                this.commessaSelected.setIdCliente(this.principaleSelected.getID());
                this.commessaSelected.setIdLavorato(this.associatoSelected.getID());
                this.commessaSelected.setNumero(this.numero);
                this.liberoValue.forEach(function (l) {
                    __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("libero v: " + (l == null ? 'vuoto' : l));
                });
                this.commessaSelected.setLibero(this.liberoValue);
                if (this.nuovaCommessa) {
                    // UtilsCommessa.addListaCommesse(this.commessaSelected);
                    __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.unshift(this.commessaSelected);
                }
                this.databaseprovider.updateCommessa(this.commessaSelected).then(function () {
                    __WEBPACK_IMPORTED_MODULE_1__commesse_commesse__["a" /* CommessePage */].listChanges = true;
                });
            }
            this.navCtrl.pop();
        }
        else {
            __WEBPACK_IMPORTED_MODULE_8__Utils_Utils__["a" /* Utils */].writeLog("Dati non compilati correttamente");
        }
    };
    DettagliComessaPage.prototype.presentDelete = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            //title: 'Attenzione',
            title: 'Sei sicuro di voler uscire ? Questa commessa non sarà salvata',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Rimani',
                    role: 'destructive',
                    handler: function () { }
                },
                {
                    text: 'Esci',
                    handler: function () {
                        _this.deleteFromDB(_this.commessaSelected.getID());
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    DettagliComessaPage.prototype.deleteFromDB = function (id) {
        this.databaseprovider.deleteCommessa(id).then(function () {
            __WEBPACK_IMPORTED_MODULE_1__commesse_commesse__["a" /* CommessePage */].listChanges = true;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["Navbar"])
    ], DettagliComessaPage.prototype, "navbar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_9_ionic_select_searchable__["SelectSearchableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_9_ionic_select_searchable__["SelectSearchableComponent"])
    ], DettagliComessaPage.prototype, "searchCliente", void 0);
    DettagliComessaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["Component"])({
            selector: 'page-dettagli-comessa',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/dettagli-comessa/dettagli-comessa.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Commessa</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-item>\n    <ion-label>Cliente</ion-label>\n    <select-searchable\n        item-content\n        padding-right\n        [isEnabled]="nuovaCommessa"\n        [(ngModel)]="principaleSelected"\n        [items]="contattiPrincipali"\n        itemValueField="id"\n        itemTextField="nomeCognome"\n        [canSearch]="true"\n        [hasInfiniteScroll]="true"\n        (onSearch)="searchContatti($event, \'selectCliente\')"\n        (onInfiniteScroll)="getMoreContatti($event)"\n        (onChange)="changeCliente($event, \'selectCliente\')">\n    </select-searchable>\n  </ion-item>\n  <ion-item>\n    <ion-label>Da chi ?</ion-label>\n    <select-searchable\n        item-content\n        padding-right\n        [isEnabled]="lavoratoEnabled"\n        [(ngModel)]="associatoSelected"\n        [items]="contattiAssociati"\n        itemValueField="id"\n        itemTextField="nomeCognome"\n        [canSearch]="true"\n        [hasInfiniteScroll]="true"\n        (onSearch)="searchContatti($event, \'selectLavorato\')"\n        (onInfiniteScroll)="getMoreContatti($event)"\n        (onChange)="changeCliente($event, \'selectLavorato\')">\n    </select-searchable>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Numero commessa</ion-label>\n    <ion-input type="text" [(ngModel)]="numero"></ion-input>\n  </ion-item>\n  <ion-item *ngFor="let l of libero">\n    <ion-label fixed>{{ l.getName() }}</ion-label>\n    <ion-input type="text" placeholder="Inserisci oppure" [(ngModel)]="liberoValue[libero.indexOf(l)]"></ion-input>\n    <button ion-button outline item-end (click)="scegliOpzione(l)">Scegli</button>\n  </ion-item>\n  <ion-row>\n    <ion-col>\n      <button ion-button block (click)="save()">SALVA</button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/dettagli-comessa/dettagli-comessa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["AlertController"]])
    ], DettagliComessaPage);
    return DettagliComessaPage;
}());

//# sourceMappingURL=dettagli-comessa.js.map

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_database_database__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, menu, formBuilder, databaseprovider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.databaseprovider = databaseprovider;
        this.Menu = menu;
        this.loginForm = this.formBuilder.group({
            username: ['Nexapp', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required],
            password: ['Nexpwd18!', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]
        });
        // this.username = "Nexapp";
        // this.password = "Nexpwd18!";
    }
    // db
    //
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        this.Menu.enable(false);
        //this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        if (this.loginForm.valid) {
            this.databaseprovider.getUser(this.loginForm.controls['username'].value, this.loginForm.controls['password'].value)
                .then(function (users) {
                __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog("User data: " + users[0].getId() + " " + users[0].getUsername());
                if (users.length == 1) {
                    __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].setUserLogged(users[0]);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], {}, { animate: true, direction: 'forward' });
                    //   //this.navCtrl.setRoot(ContattiPage, {}, {animate: true, direction: 'forward'});
                }
            })
                .catch(function (e) { return __WEBPACK_IMPORTED_MODULE_1__Utils_Utils__["a" /* Utils */].writeLog(e); });
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/login/login.html"*/'<ion-content padding center center-text>\n  <ion-title>NEXRapportino</ion-title>\n  <form (submit)="login()" [formGroup]="loginForm">\n    <!-- gestione username -->\n    <ion-item>\n      <ion-label floating>Username</ion-label>\n      <ion-input [formControl]="loginForm.controls[\'username\']" type="text"></ion-input>\n    </ion-item>\n    <ion-item danger class="no-line error" *ngIf="!loginForm.controls.username.valid && loginForm.controls.username.dirty">\n      Inserisci un Username\n    </ion-item>\n    <!-- fine gestione username -->\n    <!-- gestione password -->\n    <ion-item>\n      <ion-label floating>Password</ion-label>\n      <ion-input [formControl]="loginForm.controls[\'password\']" type="password"></ion-input>\n    </ion-item>\n    <ion-item danger class="no-line error" *ngIf="!loginForm.controls.password.valid && loginForm.controls.password.dirty">\n      Inserisci una Password\n    </ion-item>\n    <!-- fine gestione password -->\n  </form>\n  <ion-row>\n    <ion-col text-center>\n      <button ion-button block [disabled]="!loginForm.valid" (click)="login()">Log In</button>\n    </ion-col>\n  </ion-row>\n  <!-- <button ion-button full outline (click)="goToRegister()"Registrati>Registrati</button> -->\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["MenuController"], __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_5__providers_database_database__["a" /* DatabaseProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContattoDettagliPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__contatti_contatti__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ContattoDettagliPage = /** @class */ (function () {
    function ContattoDettagliPage(navCtrl, navParams, platform, alertCtrl, databaseprovider, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.databaseprovider = databaseprovider;
        this.toastCtrl = toastCtrl;
        this.nuovoContatto = false;
        this.datiOk = false;
        this.contattoSelected = navParams.get('contatto');
        this.nuovoContatto = navParams.get('nuovo');
        __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Costruttore -> nuovo contatto ? " + this.nuovoContatto);
        this.choose = 'dettagli';
        if (this.contattoSelected != null) {
            var cf = void 0;
            var sedelegale = void 0;
            if (this.contattoSelected.getType() == 'Persona') {
                cf = this.contattoSelected.getCF();
                sedelegale = '';
            }
            else {
                cf = '';
                sedelegale = this.contattoSelected.getSedeLegale();
            }
            this.dati = {
                idcompany: this.contattoSelected.getIdCompany(),
                nomeCognome: this.contattoSelected.getNomeCognome(),
                telefono: this.contattoSelected.getTelefono(),
                email: this.contattoSelected.getEmail(),
                piva: this.contattoSelected.getPIva(),
                cf: cf,
                sedelegale: sedelegale,
                sedeoperativa: this.contattoSelected.getSedeOperativa(),
                maxore: this.contattoSelected.getMaxOre(),
                tariffaora: this.contattoSelected.getTariffaOre(),
                tariffagiornata: this.contattoSelected.getTariffaGiornata(),
                tariffakm: this.contattoSelected.getTariffaKm()
            };
            if (this.contattoSelected.getIdCompany() != 0) {
                this.contattoPrincipale = __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].getContattoPrincipale(this.contattoSelected.getIdCompany())[0].getNomeCognome();
            }
        }
        platform.registerBackButtonAction(function () {
            __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Back pressed");
            _this.backButtonClick();
        });
    }
    ContattoDettagliPage_1 = ContattoDettagliPage;
    ContattoDettagliPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Contatto Selezionato: " + this.contattoSelected.getID() + " " + this.contattoSelected.getNomeCognome() + " " + this.contattoSelected.getType());
        this.navbar.backButtonClick = function () {
            _this.backButtonClick();
        };
    };
    ContattoDettagliPage.prototype.ionViewDidEnter = function () {
        if (this.contattoSelected.getType() == 'Azienda') {
            this.listaContattiAssociati = [];
            this.listaContattiAssociati = __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].getListaAssociati(this.contattoSelected.getID());
        }
    };
    ContattoDettagliPage.prototype.ionViewDidLeave = function () {
        if (this.nuovoContatto) {
            __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Leaving view " + this.nuovoContatto);
            this.nuovoContatto = false;
        }
    };
    ContattoDettagliPage.prototype.ionViewWillLeave = function () {
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    };
    ContattoDettagliPage.prototype.backButtonClick = function () {
        __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Back button -> Modifiche ? " + __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].checkChanges(this.dati, this.contattoSelected));
        if (__WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].checkChanges(this.dati, this.contattoSelected)[0]) {
            this.presentSave();
        }
        else {
            __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Back button -> Nuovo contatto  ? " + this.nuovoContatto);
            if (this.nuovoContatto) {
                this.presentDelete();
            }
            else {
                this.navCtrl.pop();
            }
        }
    };
    ContattoDettagliPage.prototype.save = function () {
        var checkDati = __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].checkDati(this.dati);
        __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Dati corretti ? " + checkDati[0]);
        if (checkDati[0]) {
            var checkChanges = __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].checkChanges(this.dati, this.contattoSelected);
            __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Update ? " + checkChanges[0]);
            checkChanges[1].forEach(function (cc) {
                __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Campo mod: " + cc);
            });
            if (__WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].checkChanges(this.dati, this.contattoSelected)[0]) {
                this.contattoSelected.setNomeCognome(this.dati.nomeCognome);
                this.contattoSelected.setTelefono(this.dati.telefono);
                this.contattoSelected.setEmail(this.dati.email);
                this.contattoSelected.setPIva(this.dati.piva);
                this.contattoSelected.setCF(this.dati.cf);
                this.contattoSelected.setSedeLegale(this.dati.sedelegale);
                this.contattoSelected.setSedeOperativa(this.dati.sedeoperativa);
                this.contattoSelected.setMaxOre(parseInt(this.dati.maxore));
                this.contattoSelected.setTariffaOre(parseFloat(this.dati.tariffaora));
                this.contattoSelected.setTariffaGiornata(parseFloat(this.dati.tariffagiornata));
                this.contattoSelected.setTariffaKm(parseFloat(this.dati.tariffakm));
                __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog(this.contattoSelected.writeInfo());
                if (this.nuovoContatto) {
                    //UtilsContatti.addListaContatti(this.contattoSelected);
                    __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.unshift(this.contattoSelected);
                }
                this.databaseprovider.updateContatto(this.contattoSelected)
                    .then(function (res) { __WEBPACK_IMPORTED_MODULE_0__contatti_contatti__["a" /* ContattiPage */].listChanges = true; })
                    .catch(function (err) { __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("E\\ + update contatto: " + err); });
            }
            this.navCtrl.pop();
        }
        else {
            __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].presentToast("Dati non compilati correttamente, " + checkDati[1], this.toastCtrl);
            __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog("Dati non compilati correttamente");
        }
    };
    ContattoDettagliPage.prototype.presentSave = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            //title: 'Attenzione',
            title: 'Sono state apportate delle modifiche',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Salva Modifiche',
                    role: 'destructive',
                    handler: function () {
                        _this.save();
                    }
                },
                {
                    text: 'Continua modifiche',
                    handler: function () { }
                },
                {
                    text: 'Annulla Modifiche',
                    handler: function () {
                        if (_this.nuovoContatto) {
                            _this.deleteFromDB(_this.contattoSelected.getID());
                        }
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    ContattoDettagliPage.prototype.openContatto = function (contatto, nuovo) {
        this.navCtrl.push(ContattoDettagliPage_1, {
            contatto: contatto,
            nuovo: nuovo
        });
    };
    ContattoDettagliPage.prototype.nuovo = function () {
        var _this = this;
        this.nuovoContatto = true;
        this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.databaseprovider.addContatto(_this.contattoSelected.getID())
                    .then(function (contatto) {
                    if (contatto != null) {
                        _this.openContatto(contatto, true);
                    }
                });
            }
        });
    };
    ContattoDettagliPage.prototype.changeType = function (item) {
        if (item.checked) {
            this.contattoSelected.setType("Azienda");
        }
        else {
            this.contattoSelected.setType("Persona");
        }
    };
    ContattoDettagliPage.prototype.presentDelete = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            //title: 'Attenzione',
            title: 'Sei sicuro di voler uscire ? Questo contatto non sarà salvato',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Rimani',
                    role: 'destructive',
                    handler: function () { }
                },
                {
                    text: 'Esci',
                    handler: function () {
                        _this.deleteFromDB(_this.contattoSelected.getID());
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    ContattoDettagliPage.prototype.delete = function (item, contatto) {
        var _this = this;
        //item.close()
        if (contatto.getIdCompany() > 0) {
            var indexGlobal_1 = __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].listaContatti.indexOf(contatto);
            var index_1 = this.listaContattiAssociati.indexOf(contatto);
            if (indexGlobal_1 > -1) {
                var alert_1 = this.alertCtrl.create({
                    title: 'Attenzione',
                    message: 'Sei sicuro di voler eliminare questo contatto?',
                    buttons: [
                        {
                            text: 'No',
                            role: 'cancel',
                            handler: function () { }
                        },
                        {
                            text: 'Elimina',
                            handler: function () {
                                __WEBPACK_IMPORTED_MODULE_2__Utils_UtilsContatti__["a" /* UtilsContatti */].removeListaContatti(indexGlobal_1);
                                _this.listaContattiAssociati.splice(index_1, 1);
                                __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog(contatto.getNomeCognome() + " Eliminato");
                                _this.deleteFromDB(contatto.getID());
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        }
        else {
            __WEBPACK_IMPORTED_MODULE_3__Utils_Utils__["a" /* Utils */].writeLog(contatto.getNomeCognome() + " è una compagnia");
        }
    };
    ContattoDettagliPage.prototype.deleteFromDB = function (id) {
        this.databaseprovider.deleteContatto(id).then(function () {
            __WEBPACK_IMPORTED_MODULE_0__contatti_contatti__["a" /* ContattiPage */].listChanges = true;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["Navbar"])
    ], ContattoDettagliPage.prototype, "navbar", void 0);
    ContattoDettagliPage = ContattoDettagliPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Component"])({
            selector: 'page-contatto-dettagli',template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/contatto-dettagli/contatto-dettagli.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      <div *ngIf="contattoSelected?.getType() == \'Azienda\'; then azienda else persona"></div>\n      <ng-template #persona><ion-icon ios="ios-contact-outline" md="md-contact" item-start></ion-icon></ng-template>\n      <ng-template #azienda><ion-icon name="contacts" color="{{contattoSelected?.getIdCompany() == 0 ? \'primary\' : \'\'}}" item-start></ion-icon></ng-template>\n       {{contattoSelected.getNomeCognome()}}\n    </ion-title>\n  </ion-navbar>\n  <div *ngIf="contattoSelected?.getIdCompany() == 0 && !nuovoContatto; then associati"></div>\n  <ng-template #associati>\n  <ion-toolbar>\n    <ion-segment [(ngModel)]="choose" >\n      <ion-segment-button value="dettagli">\n        <!-- <ion-icon name="list-box"></ion-icon> -->\n        Dettagli\n      </ion-segment-button>\n        <ion-segment-button value="associati">\n          <!-- <ion-icon name="git-compare"></ion-icon> -->\n          Associati\n        </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n  </ng-template>\n</ion-header>\n\n<ion-content>\n  <div [ngSwitch]="choose">\n    <div *ngSwitchCase="\'dettagli\'">\n      <div *ngIf="nuovoContatto && contattoSelected.getIdCompany() != 0">\n      <ion-item>\n        <ion-label>{{contattoSelected.getType()}}</ion-label>\n        <ion-toggle checked (ionChange)="changeType($event)"></ion-toggle> <!-- [(ngModel)]="tipo" -->\n      </ion-item>\n      </div>\n      <ion-item>\n        <ion-label floating>{{contattoSelected.getType() == \'Azienda\' ? \'Ragione Sociale\' : \'Nome e Cognome\'}}</ion-label>\n        <ion-input [(ngModel)]="dati.nomeCognome" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Telefono</ion-label>\n        <ion-input [(ngModel)]="dati.telefono" type="tel"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>E-mail</ion-label>\n        <ion-input [(ngModel)]="dati.email" type="email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Partita iva</ion-label>\n        <ion-input [(ngModel)]="dati.piva" type="text"></ion-input>\n      </ion-item>\n      <div *ngIf="contattoSelected?.getType() == \'Persona\'; then persona"></div>\n      <ng-template #persona>\n      <ion-item>\n        <ion-label floating>Codice Fiscale</ion-label>\n        <ion-input [(ngModel)]="dati.cf" type="text"></ion-input>\n      </ion-item>\n      </ng-template>\n      <div *ngIf="contattoSelected?.getType() == \'Azienda\'; then azienda"></div>\n      <ng-template #azienda>\n      <ion-item>\n        <ion-label floating>Sede Legale</ion-label>\n        <ion-input [(ngModel)]="dati.sedelegale" type="text"></ion-input>\n      </ion-item>\n      </ng-template>\n      <ion-item>\n        <ion-label floating>Sede Operativa</ion-label>\n        <ion-input [(ngModel)]="dati.sedeoperativa" type="text"></ion-input>\n      </ion-item>\n      <!-- <ion-item>\n        <ion-label floating>Max Ore rimborsabili</ion-label>\n        <ion-input [(ngModel)]="dati.maxore" type="text"></ion-input>\n      </ion-item> -->\n      <div *ngIf="contattoSelected.getIdCompany() == 0">\n        <ion-item>\n          <ion-label floatin>Max Ore rimborsabili: {{dati.maxore}}</ion-label>\n          <ion-range min="0" step="1" max="24" snaps="true" [(ngModel)]="dati.maxore">\n            <ion-label range-left>0</ion-label>\n            <ion-label range-right>24</ion-label>\n          </ion-range>\n        </ion-item>\n        <ion-item>\n          <ion-label floating>Tariffa Oraria (€/h)</ion-label>\n          <ion-input [(ngModel)]="dati.tariffaora" type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label floating>Tariffa Giornata (€)</ion-label>\n          <ion-input [(ngModel)]="dati.tariffagiornata" type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label floating>Tariffa KM (€/Km)</ion-label>\n          <ion-input [(ngModel)]="dati.tariffakm" type="number"></ion-input>\n        </ion-item>\n      </div>\n      <ion-row padding-bottom>\n        <ion-col>\n          <button ion-button block (click)="save()">SALVA</button>\n        </ion-col>\n      </ion-row>\n\n      <div *ngIf="contattoSelected.getIdCompany() != 0">\n      <ion-item padding-bottom>\n        <ion-label>Associato a {{contattoPrincipale}}</ion-label>\n      </ion-item>\n      </div>\n    </div>\n    <div *ngSwitchCase="\'associati\'">\n      <button ion-item (click)="nuovo()">\n        <ion-icon name="add-circle" item-start></ion-icon>\n          Nuovo\n        <div class="item-note" item-end></div>\n      </button>\n      \n      <div *ngIf="listaContattiAssociati?.length > 0; then contatti; else nocontatti"></div>\n      <ng-template #contatti>\n        <ion-list>\n          <ion-item-sliding #item *ngFor="let contatto of listaContattiAssociati">\n            <button ion-item (click)="openContatto(contatto)">\n              <div *ngIf="contatto?.getType() == \'Azienda\'; then azienda else persona"></div>\n              <ng-template #persona><ion-icon ios="ios-contact-outline" md="md-contact" item-start></ion-icon></ng-template>\n              <ng-template #azienda><ion-icon name="contacts" item-start></ion-icon></ng-template>\n              {{contatto.getNomeCognome()}}\n            </button>\n            <ion-item-options icon-start side="left" (ionSwipe)="delete($event, contatto)">\n              <button ion-button color="danger" (click)="delete(item, contatto)" expandable>\n                <ion-icon name="trash"></ion-icon>Elimina\n              </button>\n            </ion-item-options> \n          </ion-item-sliding>\n        </ion-list>\n        <!-- <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfinite())" [enabled]="enabled">\n          <ion-infinite-scroll-content></ion-infinite-scroll-content>\n        </ion-infinite-scroll>  -->\n      </ng-template>\n      <ng-template #nocontatti>\n        <p>Non sono presenti contatti associati</p>\n      </ng-template>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/pages/contatto-dettagli/contatto-dettagli.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["ToastController"]])
    ], ContattoDettagliPage);
    return ContattoDettagliPage;
    var ContattoDettagliPage_1;
}());

//# sourceMappingURL=contatto-dettagli.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(353);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_documenti_documenti__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_popover_popover__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_dettagli_rapportino_dettagli_rapportino__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_rapportini_rapportini__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_dettagli_comessa_dettagli_comessa__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_dettagli_setting_dettagli_setting__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(577);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_storage__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_ionic_select_searchable__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_sqlite_porter__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_sqlite__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_contatti_contatti__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_commesse_commesse__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_contatto_dettagli_contatto_dettagli__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_setting_setting__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_call_number__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_screen_orientation__ = __webpack_require__(585);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_riepilogo_riepilogo__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_contatti_contatti__["a" /* ContattiPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_contatto_dettagli_contatto_dettagli__["a" /* ContattoDettagliPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_commesse_commesse__["a" /* CommessePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_dettagli_setting_dettagli_setting__["a" /* DettagliSettingPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_dettagli_comessa_dettagli_comessa__["a" /* DettagliComessaPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_rapportini_rapportini__["a" /* RapportiniPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_dettagli_rapportino_dettagli_rapportino__["a" /* DettagliRapportinoPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_popover_popover__["a" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_riepilogo_riepilogo__["a" /* RiepilogoPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_documenti_documenti__["a" /* DocumentiPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_18_ionic_select_searchable__["SelectSearchableModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_contatti_contatti__["a" /* ContattiPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_contatto_dettagli_contatto_dettagli__["a" /* ContattoDettagliPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_commesse_commesse__["a" /* CommessePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_dettagli_setting_dettagli_setting__["a" /* DettagliSettingPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_dettagli_comessa_dettagli_comessa__["a" /* DettagliComessaPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_rapportini_rapportini__["a" /* RapportiniPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_dettagli_rapportino_dettagli_rapportino__["a" /* DettagliRapportinoPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_popover_popover__["a" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_riepilogo_riepilogo__["a" /* RiepilogoPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_documenti_documenti__["a" /* DocumentiPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_8__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_sqlite_porter__["a" /* SQLitePorter */],
                __WEBPACK_IMPORTED_MODULE_17__providers_database_database__["a" /* DatabaseProvider */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file__["a" /* File */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Documento; });
var Documento = /** @class */ (function () {
    function Documento(_id, _idRiga, _titolo, _path) {
        this.id = _id;
        this.idRiga = _idRiga;
        this.titolo = _titolo;
        this.path = _path;
    }
    return Documento;
}());

//# sourceMappingURL=documento.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RigaRapportino; });
var RigaRapportino = /** @class */ (function () {
    function RigaRapportino(_id, _idRapportino, _idCommessa, _data, _totOViaggio, _totOLavoro, _totKM, _pranzo_art15, _cena_art15, _hotel_art15, _altro_art15, _pranzo, _cena, _hotel, _altro) {
        this.id = _id;
        this.idRapportino = _idRapportino;
        this.idCommessa = _idCommessa;
        this.data = _data;
        this.totOreViaggio = _totOViaggio;
        this.totOreLavoro = _totOLavoro;
        this.totKM = _totKM;
        this.pranzo_art15 = _pranzo_art15;
        this.cena_art15 = _cena_art15;
        this.hotel_art15 = _hotel_art15;
        this.altro_art15 = _altro_art15;
        this.pranzo = _pranzo;
        this.cena = _cena;
        this.hotel = _hotel;
        this.altro = _altro;
    }
    RigaRapportino.prototype.getOreViaggioLavoro = function () { return this.totOreViaggio + this.totOreLavoro; };
    RigaRapportino.prototype.getTotRimborsoKm = function (tariffa) { return this.totKM * tariffa; };
    RigaRapportino.prototype.getVittoArt15 = function () { return this.pranzo_art15 + this.cena_art15; };
    RigaRapportino.prototype.getVitto = function () { return this.pranzo + this.cena; };
    RigaRapportino.prototype.getRimborsiArt15 = function () { return this.pranzo_art15 + this.cena_art15 + this.hotel_art15 + this.altro_art15; };
    RigaRapportino.prototype.getRimborsi = function () { return this.pranzo + this.cena + this.hotel + this.altro; };
    RigaRapportino.prototype.getRimborsoOre = function (cliente) {
        var ore = this.getOreViaggioLavoro();
        if (ore > cliente.getMaxOre()) {
            return cliente.getTariffaGiornata();
        }
        else {
            return ore * cliente.getTariffaOre();
        }
    };
    return RigaRapportino;
}());

//# sourceMappingURL=rigaRapportino.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Commessa; });
var Commessa = /** @class */ (function () {
    function Commessa(_id, _numero, _idCliente, _idLavorato, _libero) {
        this.id = _id;
        this.numero = _numero;
        this.idCliente = _idCliente;
        this.idLavorato = _idLavorato;
        this.libero = _libero;
        this.count = 0;
    }
    Commessa.prototype.getID = function () { return this.id; };
    Commessa.prototype.getNumero = function () { return this.numero; };
    Commessa.prototype.setNumero = function (_numero) { this.numero = _numero; };
    Commessa.prototype.getIdCliente = function () { return this.idCliente; };
    Commessa.prototype.setIdCliente = function (_idCliente) { this.idCliente = _idCliente; };
    Commessa.prototype.getIdLavorato = function () { return this.idLavorato; };
    Commessa.prototype.setIdLavorato = function (_idLavorato) { this.idLavorato = _idLavorato; };
    Commessa.prototype.getLibero = function () { return this.libero; };
    Commessa.prototype.getOneLibero = function (index) { return this.libero[index]; };
    Commessa.prototype.setLibero = function (_libero) { this.libero = _libero; };
    Commessa.prototype.setOneLibero = function (index, _valueLibero) { this.libero[index] = _valueLibero; };
    Commessa.prototype.getCount = function () { return this.count; };
    Commessa.prototype.addCount = function () { this.count++; };
    Commessa.prototype.removeCount = function () { this.count--; };
    Commessa.prototype.restoreCount = function () { this.count = 0; };
    return Commessa;
}());

//# sourceMappingURL=commessa.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CampiSetting; });
var CampiSetting = /** @class */ (function () {
    function CampiSetting(_id, _idCampo, _attivo, _name, _value) {
        this.id = _id;
        this.idCampo = _idCampo;
        this.attivo = _attivo;
        this.name = _name;
        this.value = _value;
    }
    CampiSetting.prototype.getID = function () { return this.id; };
    CampiSetting.prototype.getIDCampo = function () { return this.idCampo; };
    CampiSetting.prototype.getAttivo = function () { return this.attivo; };
    CampiSetting.prototype.setAttivo = function (_attivo) { this.attivo = _attivo; };
    CampiSetting.prototype.getName = function () { return this.name; };
    CampiSetting.prototype.setName = function (_name) { this.name = _name; };
    CampiSetting.prototype.getValue = function () { return this.value; };
    CampiSetting.prototype.setValue = function (_value) { this.value = _value; };
    return CampiSetting;
}());

//# sourceMappingURL=campiSetting.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contatto; });
var Contatto = /** @class */ (function () {
    function Contatto(_id, _idCompany, _type, _nomeCognome, _telefono, _email, _p_iva, _cf, _sede_legale, _sede_operativa, _maxOre, _tariffaOre, _tariffa_giornata, _tariffaKm) {
        this.id = _id;
        this.idCompany = _idCompany;
        this.type = _type;
        this.nomeCognome = _nomeCognome;
        this.telefono = _telefono;
        this.email = _email;
        this.pIva = _p_iva;
        this.cf = _cf;
        this.sedeLegale = _sede_legale;
        this.sedeOperativa = _sede_operativa;
        this.maxOre = _maxOre;
        this.tariffaOre = _tariffaOre;
        this.tariffaGiornata = _tariffa_giornata;
        this.tariffaKm = _tariffaKm;
    }
    Contatto.prototype.getID = function () { return this.id; };
    Contatto.prototype.getIdCompany = function () { return this.idCompany; };
    Contatto.prototype.getType = function () { return this.type; };
    Contatto.prototype.setType = function (_type) { this.type = _type; };
    Contatto.prototype.getNomeCognome = function () { return this.nomeCognome; };
    Contatto.prototype.setNomeCognome = function (_nomeCognome) { this.nomeCognome = _nomeCognome; };
    Contatto.prototype.getTelefono = function () { return this.telefono; };
    Contatto.prototype.setTelefono = function (_telefono) { this.telefono = _telefono; };
    Contatto.prototype.getEmail = function () { return this.email; };
    Contatto.prototype.setEmail = function (_email) { this.email = _email; };
    Contatto.prototype.getPIva = function () { return this.pIva; };
    Contatto.prototype.setPIva = function (_p_iva) { this.pIva = _p_iva; };
    Contatto.prototype.getCF = function () { return this.cf; };
    Contatto.prototype.setCF = function (_cf) { this.cf = _cf; };
    Contatto.prototype.getSedeLegale = function () { return this.sedeLegale; };
    Contatto.prototype.setSedeLegale = function (_sede_legale) { this.sedeLegale = _sede_legale; };
    Contatto.prototype.getSedeOperativa = function () { return this.sedeOperativa; };
    Contatto.prototype.setSedeOperativa = function (_sede_operativa) { this.sedeOperativa = _sede_operativa; };
    Contatto.prototype.getMaxOre = function () { return this.maxOre; };
    Contatto.prototype.setMaxOre = function (_maxOre) { this.maxOre = _maxOre; };
    Contatto.prototype.getTariffaOre = function () { return this.tariffaOre; };
    Contatto.prototype.setTariffaOre = function (_tariffaOre) { this.tariffaOre = _tariffaOre; };
    Contatto.prototype.getTariffaGiornata = function () { return this.tariffaGiornata; };
    Contatto.prototype.setTariffaGiornata = function (_tariffa_giornata) { this.tariffaGiornata = _tariffa_giornata; };
    Contatto.prototype.getTariffaKm = function () { return this.tariffaKm; };
    Contatto.prototype.setTariffaKm = function (_tariffaKm) { this.tariffaKm = _tariffaKm; };
    Contatto.prototype.writeInfo = function () {
        return "ID: " + this.id + ", IDCompany: " + this.idCompany + ", Tipo: " + this.type + ", Nome e Cognome: "
            + this.nomeCognome + ", Telefono: " + this.telefono + ", Email: " + this.email + ", PIVA: " + this.pIva
            + ", CF: " + this.cf + ", Sede Legale: " + this.sedeLegale + ", Sede Operativa: " + this.sedeOperativa
            + ", Max Ore: " + this.maxOre + ", Tariffa Ore: " + this.tariffaOre + ", Tariffa KM: " + this.tariffaKm;
    };
    return Contatto;
}());

//# sourceMappingURL=contatto.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utente; });
var Utente = /** @class */ (function () {
    function Utente(_id, _username) {
        this.id = _id;
        this.username = _username;
    }
    Utente.prototype.getId = function () {
        return this.id;
    };
    Utente.prototype.getUsername = function () {
        return this.username;
    };
    Utente.prototype.setUsername = function (_username) {
        this.username = _username;
    };
    return Utente;
}());

//# sourceMappingURL=utente.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Rapportino; });
var Rapportino = /** @class */ (function () {
    function Rapportino(_id, _nome) {
        this.id = _id;
        this.nome = _nome;
    }
    return Rapportino;
}());

//# sourceMappingURL=rapportino.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsSetting; });
var UtilsSetting = /** @class */ (function () {
    function UtilsSetting() {
    }
    UtilsSetting.setCampi = function (_campi) {
        this.campi = _campi;
    };
    return UtilsSetting;
}());

//# sourceMappingURL=UtilsSetting.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsContatti; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UtilsSetting__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(469);



var UtilsContatti = /** @class */ (function () {
    function UtilsContatti() {
    }
    UtilsContatti.setListaContatti = function (_listaContatti) {
        this.listaContatti = _listaContatti;
    };
    UtilsContatti.addListaContatti = function (contatto) {
        this.listaContatti.push(contatto);
    };
    UtilsContatti.removeListaContatti = function (index) {
        this.listaContatti.splice(index, 1);
    };
    UtilsContatti.checkDati = function (dati) {
        var result = [true, ""];
        if (dati.nomeCognome.trim() == "") {
            result = [false, "Inserire nome e Cognome"];
            return result;
        }
        if (dati.telefono.trim() == "") {
            result = [false, "Inserire Numero Telefono"];
            return result;
        }
        if (dati.email.trim() == "") {
            result = [false, "Inserire E-mail"];
            return result;
        }
        if (parseInt(dati.maxore) <= 0 && dati.idcompany == 0) {
            result = [false, "Inserire Ore maggiori di 0"];
            return result;
        }
        if (parseInt(dati.tariffaora) < 0 && dati.idcompany == 0) {
            result = [false, "Inserire Tariffa oraria maggiore o uguale di 0"];
            return result;
        }
        if (parseInt(dati.tariffagiornata) < 0 && dati.idcompany == 0) {
            result = [false, "Inserire Tariffa Giornata maggiore o uguale di 0"];
            return result;
        }
        if (parseInt(dati.tariffakm) < 0 && dati.idcompany == 0) {
            result = [false, "Inserire Tariffa KM maggiore o uguale di 0"];
            return result;
        }
        return result;
    };
    UtilsContatti.checkChanges = function (dati, contatto) {
        var changes = false;
        var campo = [];
        if (contatto.getNomeCognome() != dati.nomeCognome) {
            changes = true;
            campo.push("Nome Cognome");
        }
        if (contatto.getTelefono() != dati.telefono) {
            changes = true;
            campo.push("Telefono");
        }
        if (contatto.getEmail() != dati.email) {
            changes = true;
            campo.push("Email");
        }
        if (contatto.getPIva() != dati.piva) {
            changes = true;
            campo.push("Piva");
        }
        if (contatto.getCF() != dati.cf) {
            changes = true;
            campo.push("CF");
        }
        if (contatto.getSedeLegale() != dati.sedelegale) {
            changes = true;
            campo.push("Sede legale");
        }
        if (contatto.getSedeOperativa() != dati.sedeoperativa) {
            changes = true;
            campo.push("Sede operativa");
        }
        if (contatto.getMaxOre() != dati.maxore && dati.idcompany == 0) {
            changes = true;
            campo.push("Ore Max");
        }
        if (contatto.getTariffaOre() != dati.tariffaora && dati.idcompany == 0) {
            changes = true;
            campo.push("Tariffa Ore");
        }
        if (contatto.getTariffaGiornata() != dati.tariffagiornata && dati.idcompany == 0) {
            changes = true;
            campo.push("Tariffa Giornata");
        }
        if (contatto.getTariffaKm() != dati.tariffakm && dati.idcompany == 0) {
            changes = true;
            campo.push("Tariffa KM");
        }
        return [changes, campo];
    };
    UtilsContatti.getContattoPrincipale = function (id) {
        return this.listaContatti.filter(function (contatto) {
            return contatto.getID() == id;
        });
    };
    UtilsContatti.getListaAssociati = function (idCompany, page, size, includiPersone) {
        var tmpList;
        var tmp = this.listaContatti.filter(function (contatto) {
            return contatto.getIdCompany() == idCompany;
        });
        tmpList = tmp;
        if (includiPersone != undefined) {
            if (!includiPersone) {
                tmpList = tmp.filter(function (contatto) {
                    return contatto.getType().toLowerCase() == "azienda";
                });
            }
        }
        if (page && size) {
            if (page != -1 && size != -1) {
                tmpList = tmpList.slice((page - 1) * size, ((page - 1) * size) + size);
            }
        }
        return tmpList;
    };
    UtilsContatti.getListaAssociatiAsync = function (idCompany, page, size, timeout) {
        var _this = this;
        if (timeout === void 0) { timeout = 1000; }
        return new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) {
            observer.next(_this.getListaAssociati(idCompany, page, size, __WEBPACK_IMPORTED_MODULE_0__UtilsSetting__["a" /* UtilsSetting */].includiPersone));
            observer.complete();
        }).pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["a" /* delay */])(timeout));
    };
    return UtilsContatti;
}());

//# sourceMappingURL=UtilsContatti.js.map

/***/ }),

/***/ 575:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Excel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__riepilogo__ = __webpack_require__(576);




var Excel = /** @class */ (function () {
    function Excel(_righe) {
        this.valueRiepilogo = [];
        this.countLibero = 0;
        this.rr = _righe;
        this.excelRapportino();
        this.excelRiepilogo();
        this.excelFattura();
        // let rrExcel = this.groupByCliente(_righe);
    }
    Excel.prototype.excelRapportino = function () {
        var _this = this;
        var temp = ["COMMESSA", "CLIENTE", "DATA"];
        var tempLibero = [];
        __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].campi.forEach(function (c) {
            if (c.getIDCampo() == 0 && c.getAttivo().toString().toLowerCase() == "true") {
                _this.countLibero++;
                tempLibero.push(c.getName().toUpperCase());
            }
        });
        var temp2 = ["TOT ORE VIAGGIO", "TOT ORE LAVORO", "TOT ORE VIAGGIO-LAVORO", "TOT RIMBORSO", "TOT KM", "TOT RIMBORSO KM",
            "PRANZO (A15)", "CENA (A15)", "TOT VITTO (A15)", "HOTEL (A15)", "ALTRO (A15)",
            "PRANZO", "CENA", "TOT VITTO", "HOTEL", "ALTRO"];
        var nomeColonna = temp.concat(tempLibero, temp2);
        var valueCells = [];
        for (var i = 0; i < this.rr.length; i++) {
            var riga = this.rr[i];
            var commessa = __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCommessa(riga.idCommessa);
            var cliente = __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCliente(riga.idCommessa);
            var tmpR = [commessa.getNumero(), cliente.getNomeCognome(), __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getDateIt(riga.data)];
            // let tmpRLibero = commessa.getLibero();
            var tmpRLibero = [];
            for (var k = 0; k < this.countLibero; k++) {
                tmpRLibero.push(commessa.getOneLibero(k));
            }
            __WEBPACK_IMPORTED_MODULE_2__Utils_Utils__["a" /* Utils */].writeLog("CONTA LIBERI: " + tmpRLibero.length);
            var tmpR2 = [riga.totOreViaggio.toString(), riga.totOreLavoro.toString(), riga.getOreViaggioLavoro().toString(),
                riga.getRimborsoOre(cliente).toString(), riga.totKM.toString(),
                riga.getTotRimborsoKm(cliente.getTariffaKm()).toString(), riga.pranzo_art15.toString(),
                riga.cena_art15.toString(), riga.getVittoArt15().toString(), riga.hotel_art15.toString(),
                riga.altro_art15.toString(), riga.pranzo.toString(), riga.cena.toString(), riga.getVitto().toString(),
                riga.hotel.toString(), riga.altro.toString()];
            valueCells.push(tmpR.concat(tmpRLibero, tmpR2));
        }
        // Utils.writeLog("Lista Header = " + this.nomeColonna.length);
        this.rapportino = { header: nomeColonna, body: valueCells };
    };
    Excel.prototype.excelRiepilogo = function () {
        var nomeColonna = ["COMMESSA", "CLIENTE", "DATA", "TOT ORE VIAGGIO-LAVORO", "TOT ORE CORRETTE", "RIMBORSI",
            "RIMBORSI (Art 15)"];
        for (var i = 0; i < this.rr.length; i++) {
            var riga = this.rr[i];
            var commessa = __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCommessa(riga.idCommessa);
            var cliente = __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCliente(riga.idCommessa);
            var oreCorrette = riga.getOreViaggioLavoro();
            if (riga.getOreViaggioLavoro() > cliente.getMaxOre()) {
                oreCorrette = cliente.getMaxOre();
            }
            var rimborsi = riga.getTotRimborsoKm(cliente.getTariffaKm()) + riga.getRimborsi();
            var rimborsi_art15 = riga.getRimborsiArt15();
            this.valueRiepilogo.push(new __WEBPACK_IMPORTED_MODULE_3__riepilogo__["a" /* Riepilogo */](commessa.getID(), cliente.getID(), riga.id, __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getDateIt(riga.data), riga.getOreViaggioLavoro(), oreCorrette, rimborsi, rimborsi_art15));
        }
        this.riepilogo = { header: nomeColonna, body: this.getArray(this.valueRiepilogo) };
    };
    Excel.prototype.excelFattura = function () {
        var nomeColonna = ["COMMESSA", "CLIENTE", "SERVIZI SOGG.IVA", "RIMBORSI", "IVA", "RIMBORSI NON IMP.IVA RT. 15"];
        var righe = this.orderByCommessa(Array.from(this.valueRiepilogo));
        var lastCommessa = -1;
        var valueFattura = [];
        for (var i = 0; i < righe.length; i++) {
            var riga = righe[i];
            var commessa = __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCommessa(riga.idCommessa);
            var cliente = __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCliente(riga.idCommessa);
            var rigaRapportino = this.getRiga(riga.idRigaRapportino);
            // console.log("RIGA: " + riga.idCommessa + ", value riep: " + this.valueRiepilogo[0].idCommessa + ", last: " + lastCommessa);
            if (riga === this.valueRiepilogo[0] || riga.idCommessa != lastCommessa) {
                valueFattura.push([commessa.getNumero(), cliente.getNomeCognome(),
                    rigaRapportino.getRimborsoOre(cliente).toString(),
                    rigaRapportino.getRimborsi().toString(), __WEBPACK_IMPORTED_MODULE_2__Utils_Utils__["a" /* Utils */].ivaValue.toString(),
                    rigaRapportino.getRimborsiArt15()]);
                lastCommessa = riga.idCommessa;
            }
            else {
                var last = valueFattura[valueFattura.length - 1];
                last[2] = (parseFloat(last[2]) + rigaRapportino.getRimborsoOre(cliente)).toString();
                last[3] = (parseFloat(last[3]) + rigaRapportino.getRimborsi()).toString();
                last[5] = (parseFloat(last[5]) + rigaRapportino.getRimborsiArt15()).toString();
            }
        }
        this.fattura = { header: nomeColonna, body: valueFattura };
    };
    Excel.prototype.getArray = function (riepilogo) {
        var result = [];
        riepilogo.forEach(function (r) {
            result.push([__WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCommessa(r.idCommessa).getNumero(), __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].getCliente(r.idCliente).getNomeCognome(),
                r.data, r.totOreViaggioLavoro.toString(), r.totOreCorrette.toString(), r.rimborsi.toString(),
                r.rimborsi_art15.toString()]);
        });
        return result;
    };
    Excel.prototype.orderByCommessa = function (rr) {
        return rr.sort(function (a, b) {
            if (a[0] < b[0])
                return -1;
            else if (a[0] > b[0])
                return 1;
            else
                return 0;
        });
    };
    Excel.prototype.getRiga = function (idRiga) {
        return this.rr.filter(function (r) {
            return r.id == idRiga;
        })[0];
    };
    return Excel;
}());

//# sourceMappingURL=excel.js.map

/***/ }),

/***/ 576:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Riepilogo; });
var Riepilogo = /** @class */ (function () {
    function Riepilogo(_idCommessa, _idCliente, _idRigaRapportino, _data, _totOre, _totOreC, _rimborsi, _rimborsi_art15) {
        this.idCommessa = _idCommessa;
        this.idCliente = _idCliente;
        this.idRigaRapportino = _idRigaRapportino;
        this.data = _data;
        this.totOreViaggioLavoro = _totOre;
        this.totOreCorrette = _totOreC;
        this.rimborsi = _rimborsi;
        this.rimborsi_art15 = _rimborsi_art15;
    }
    return Riepilogo;
}());

//# sourceMappingURL=riepilogo.js.map

/***/ }),

/***/ 577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_setting_setting__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_database_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__Utils_UtilsContatti__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_contatti_contatti__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_commesse_commesse__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__Utils_UtilsCommessa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_rapportini_rapportini__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_storage__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_http__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, databaseprovider, storage, http) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.databaseprovider = databaseprovider;
        this.storage = storage;
        this.http = http;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */];
        this.users = [];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */] },
            { title: 'Contatti', component: __WEBPACK_IMPORTED_MODULE_12__pages_contatti_contatti__["a" /* ContattiPage */] },
            { title: 'Commesse', component: __WEBPACK_IMPORTED_MODULE_13__pages_commesse_commesse__["a" /* CommessePage */] },
            { title: 'Rapportini', component: __WEBPACK_IMPORTED_MODULE_15__pages_rapportini_rapportini__["a" /* RapportiniPage */] },
            { title: 'Impostazioni', component: __WEBPACK_IMPORTED_MODULE_2__pages_setting_setting__["a" /* SettingPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
                if (rdy) {
                    __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog('Database Pronto');
                    _this.databaseprovider.getContatti()
                        .then(function (listaContatti) {
                        __WEBPACK_IMPORTED_MODULE_11__Utils_UtilsContatti__["a" /* UtilsContatti */].setListaContatti(listaContatti);
                    });
                }
            });
            _this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
                if (rdy) {
                    _this.databaseprovider.getCampiSettings()
                        .then(function (campiSetting) {
                        __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].setCampi(campiSetting);
                    });
                }
            });
            _this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
                if (rdy) {
                    _this.databaseprovider.getCommesse()
                        .then(function (commesse) {
                        __WEBPACK_IMPORTED_MODULE_14__Utils_UtilsCommessa__["a" /* UtilsCommessa */].setListaCommesse(commesse);
                    });
                }
            });
            _this.databaseprovider.getDatabaseState().subscribe(function (rdy) {
                if (rdy) {
                    _this.databaseprovider.getRapportini()
                        .then(function (rapportini) {
                        __WEBPACK_IMPORTED_MODULE_0__Utils_UtilsRapportini__["a" /* UtilsRapportini */].setListaRapportini(rapportini);
                    });
                }
            });
            _this.storage.get('includi_persone').then(function (value) {
                __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone = value;
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].writeLog("Includi Persone: " + __WEBPACK_IMPORTED_MODULE_1__Utils_UtilsSetting__["a" /* UtilsSetting */].includiPersone);
            });
            _this.getIva();
            _this.getEmailToSend();
            _this.getIPWS();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component, {}, { animate: true, direction: 'forward' });
    };
    MyApp.prototype.logout = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */], {}, { animate: true, direction: 'back' });
    };
    MyApp.prototype.getIva = function () {
        __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].ivaValue = 22.00;
    };
    MyApp.prototype.getEmailToSend = function () {
        this.storage.get('email_to_send').then(function (val) {
            if (val != '') {
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].emailToSend = val;
            }
            else {
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].emailToSend = '';
            }
        });
    };
    MyApp.prototype.getIPWS = function () {
        this.storage.get('ipws').then(function (val) {
            if (val != '') {
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].ipws = val;
            }
            else {
                __WEBPACK_IMPORTED_MODULE_10__Utils_Utils__["a" /* Utils */].ipws = '';
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({template:/*ion-inline-start:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/app/app.html"*/'<ion-menu [content]="content"> \n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n      <button menuClose ion-item (click)="logout()">Log Out</button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled=“false” swipeEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/NexApp/Documents/IonicProjects/nexRapportino/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_9__providers_database_database__["a" /* DatabaseProvider */], __WEBPACK_IMPORTED_MODULE_16__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_17__angular_http__["b" /* Http */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsRapportini; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UtilsCommessa__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__UtilsContatti__ = __webpack_require__(52);


var UtilsRapportini = /** @class */ (function () {
    function UtilsRapportini() {
    }
    UtilsRapportini.setListaRapportini = function (_listaRapportini) {
        this.listaRapportini = _listaRapportini;
    };
    UtilsRapportini.addListaRapportini = function (contatto) {
        this.listaRapportini.push(contatto);
    };
    UtilsRapportini.removeListaRapportini = function (index) {
        this.listaRapportini.splice(index, 1);
    };
    UtilsRapportini.getDateString = function (date) {
        var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        var dateS = new Date(date).toLocaleDateString('it-IT', options).toString();
        var aDateS = dateS.split("/");
        return aDateS[2] + "-" + aDateS[1] + "-" + aDateS[0];
    };
    UtilsRapportini.getDateIt = function (date) {
        var options = { year: 'numeric', month: '2-digit', day: '2-digit' };
        return new Date(date).toLocaleDateString('it-IT', options).toString();
    };
    UtilsRapportini.getDataCommessa = function (riga) {
        return UtilsRapportini.getDateIt(riga.data);
    };
    UtilsRapportini.getDateTimeISOString = function (date) {
        var tzoffset = new Date().getTimezoneOffset() * 60000;
        return new Date(date.getTime() - tzoffset).toISOString();
    };
    UtilsRapportini.sortByDate = function (listaRigaRapportino) {
        return listaRigaRapportino.sort(function (a, b) {
            if (new Date(a.data) > new Date(b.data))
                return 1;
            if (new Date(a.data) < new Date(b.data))
                return -1;
            return 0;
            // return new Date(a.data) - new Date(b.data);
        });
    };
    UtilsRapportini.getCommessa = function (idCommessa) {
        return __WEBPACK_IMPORTED_MODULE_0__UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.filter(function (c) {
            return c.getID() == idCommessa;
        })[0];
    };
    UtilsRapportini.getCliente = function (idCommessa) {
        var commessa = __WEBPACK_IMPORTED_MODULE_0__UtilsCommessa__["a" /* UtilsCommessa */].listaCommesse.filter(function (c) {
            return c.getID() == idCommessa;
        })[0];
        return __WEBPACK_IMPORTED_MODULE_1__UtilsContatti__["a" /* UtilsContatti */].listaContatti.filter(function (c) {
            return c.getID() == commessa.getIdCliente();
        })[0];
    };
    UtilsRapportini.getRapportino = function (idRapportino) {
        return UtilsRapportini.listaRapportini.filter(function (r) {
            return r.id == idRapportino;
        })[0];
    };
    return UtilsRapportini;
}());

//# sourceMappingURL=UtilsRapportini.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsCommessa; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(15);

var UtilsCommessa = /** @class */ (function () {
    function UtilsCommessa() {
    }
    UtilsCommessa.setListaCommesse = function (_listaCommesse) {
        this.listaCommesse = _listaCommesse;
    };
    UtilsCommessa.addListaCommesse = function (commessa) {
        this.listaCommesse.push(commessa);
    };
    UtilsCommessa.removeListaCommesse = function (index) {
        this.listaCommesse.splice(index, 1);
    };
    UtilsCommessa.checkCommessa = function (p, a, n, libero) {
        if (p == null) {
            return [false, "Scegliere Cliente"];
        }
        if (a == null) {
            return [false, "Scegliere Da chi hai lavorato"];
        }
        if (n == '') {
            return [false, "Inserire numero Commessa"];
        }
        // libero.forEach(value => {
        //     if (value.trim() == '') {
        //     return [false, "Compila tutti i campi"];
        //     }
        // })
        return [true, ""];
    };
    UtilsCommessa.checkChanges = function (commessa, p, a, n, libero) {
        var result = false;
        if (p != null) {
            if (p.getID() != commessa.getIdCliente()) {
                result = true;
                __WEBPACK_IMPORTED_MODULE_0__Utils__["a" /* Utils */].writeLog("Principale");
            }
        }
        if (a != null) {
            if (a.getID() != commessa.getIdLavorato()) {
                result = true;
                __WEBPACK_IMPORTED_MODULE_0__Utils__["a" /* Utils */].writeLog("Associato");
            }
        }
        if (n != commessa.getNumero()) {
            result = true;
            __WEBPACK_IMPORTED_MODULE_0__Utils__["a" /* Utils */].writeLog("Numero");
        }
        // if (libero.length != commessa.getLibero().length) {
        //     result = true;
        //     Utils.writeLog("Libero Length " + libero.length + " " + commessa.getLibero().length);
        // } else {
        for (var i = 0; i < libero.length; i++) {
            if (libero[i] != commessa.getLibero()[i]) {
                result = true;
                __WEBPACK_IMPORTED_MODULE_0__Utils__["a" /* Utils */].writeLog("Libero " + i + " value");
                return result;
            }
        }
        // }
        return result;
    };
    return UtilsCommessa;
}());

//# sourceMappingURL=UtilsCommessa.js.map

/***/ })

},[331]);
//# sourceMappingURL=main.js.map